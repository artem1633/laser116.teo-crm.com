<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cashbox}}`.
 */
class m201008_155742_create_cashbox_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cashbox}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cashbox}}');
    }
}

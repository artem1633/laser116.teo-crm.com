<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%welding_status}}`.
 */
class m201008_153250_create_welding_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%welding_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'color' => $this->string()->comment('Цвет')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%welding_status}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding postavshik_id to table `available`.
 */
class m180614_024725_add_postavshik_id_column_to_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('available', 'postavshik_id', $this->integer());
        $this->addColumn('resource', 'postavshik_id', $this->integer());

        $this->createIndex('idx-available-postavshik_id', 'available', 'postavshik_id', false);
        $this->addForeignKey("fk-available-postavshik_id", "available", "postavshik_id", "suppliers", "id");

        $this->createIndex('idx-resource-postavshik_id', 'resource', 'postavshik_id', false);
        $this->addForeignKey("fk-resource-postavshik_id", "resource", "postavshik_id", "suppliers", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-available-postavshik_id','available');
        $this->dropIndex('idx-available-postavshik_id','available');

        $this->dropForeignKey('fk-resource-postavshik_id','resource');
        $this->dropIndex('idx-resource-postavshik_id','resource');

        $this->dropColumn('available', 'postavshik_id');
        $this->dropColumn('resource', 'postavshik_id');
    }
}

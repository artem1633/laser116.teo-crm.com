<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order_part}}`.
 */
class m201021_143520_add_additional_works_columns_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'additional_word_price1', $this->float()->comment('Стоимость дополнительных работ 1'));
        $this->addColumn('order_part', 'additional_word_price2', $this->float()->comment('Стоимость дополнительных работ 2'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_part', 'additional_word_price1');
        $this->dropColumn('order_part', 'additional_word_price2');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `goods`.
 */
class m180606_041120_create_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('goods', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->insert('goods',array(
            'name' => 'Без названия',
        ));

        $this->insert('goods',array(
            'name' => 'Носки',
        ));

        $this->insert('goods',array(
            'name' => 'Сопутствующие товары',
        ));

        $this->insert('goods',array(
            'name' => 'Швейная фурнитура',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('goods');
    }
}

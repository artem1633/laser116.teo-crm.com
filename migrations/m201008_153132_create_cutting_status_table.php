<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cutting_status}}`.
 */
class m201008_153132_create_cutting_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cutting_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'color' => $this->string()->comment('Цвет')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cutting_status}}');
    }
}

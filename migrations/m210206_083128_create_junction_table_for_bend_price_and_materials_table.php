<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bend_price_materials}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%bend_price}}`
 * - `{{%materials}}`
 */
class m210206_083128_create_junction_table_for_bend_price_and_materials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bend_price_materials}}', [
            'id' => $this->primaryKey(),
            'bend_price_id' => $this->integer(),
            'materials_id' => $this->integer(),
        ]);

        // creates index for column `bend_price_id`
        $this->createIndex(
            '{{%idx-bend_price_materials-bend_price_id}}',
            '{{%bend_price_materials}}',
            'bend_price_id'
        );

        // add foreign key for table `{{%bend_price}}`
        $this->addForeignKey(
            '{{%fk-bend_price_materials-bend_price_id}}',
            '{{%bend_price_materials}}',
            'bend_price_id',
            '{{%bend_price}}',
            'id',
            'CASCADE'
        );

        // creates index for column `materials_id`
        $this->createIndex(
            '{{%idx-bend_price_materials-materials_id}}',
            '{{%bend_price_materials}}',
            'materials_id'
        );

        // add foreign key for table `{{%materials}}`
        $this->addForeignKey(
            '{{%fk-bend_price_materials-materials_id}}',
            '{{%bend_price_materials}}',
            'materials_id',
            '{{%materials}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%bend_price}}`
        $this->dropForeignKey(
            '{{%fk-bend_price_materials-bend_price_id}}',
            '{{%bend_price_materials}}'
        );

        // drops index for column `bend_price_id`
        $this->dropIndex(
            '{{%idx-bend_price_materials-bend_price_id}}',
            '{{%bend_price_materials}}'
        );

        // drops foreign key for table `{{%materials}}`
        $this->dropForeignKey(
            '{{%fk-bend_price_materials-materials_id}}',
            '{{%bend_price_materials}}'
        );

        // drops index for column `materials_id`
        $this->dropIndex(
            '{{%idx-bend_price_materials-materials_id}}',
            '{{%bend_price_materials}}'
        );

        $this->dropTable('{{%bend_price_materials}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding number to table `resource`.
 */
class m180615_170124_add_number_column_to_resource_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('resource', 'number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('resource', 'number');
    }
}

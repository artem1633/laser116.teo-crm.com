<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%payments}}`.
 */
class m201020_113418_add_customer_id_column_to_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments', 'customer_id', $this->integer()->after('id')->comment('Контрагент'));

        $this->createIndex(
            'idx-payments-customer_id',
            'payments',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-payments-customer_id',
            'payments',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-payments-customer_id',
            'payments'
        );

        $this->dropIndex(
            'idx-payments-customer_id',
            'payments'
        );

        $this->dropColumn('payments', 'customer_id');
    }
}

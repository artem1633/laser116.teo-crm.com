<?php

use yii\db\Migration;

/**
 * Handles the creation of table `available`.
 */
class m180609_122400_create_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('available', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->comment('Названия товара'),
            'creator_id' => $this->integer()->comment('Создатель'),
            'storage_id' => $this->integer()->comment('Склад'),
            'status_id' => $this->integer()->comment('Статус'),
            'count' => $this->integer()->comment('Количество'),
            'price' => $this->float()->comment('Цена'),
            'price_shop' => $this->float()->comment('Цена закупа'),
            'good_id' => $this->integer()->comment('Категория  товара'),
            'comment' => $this->text()->comment('Комментария'),
        ]);

        $this->createIndex('idx-available-product_id', 'available', 'product_id', false);
        $this->addForeignKey("fk-available-product_id", "available", "product_id", "materials", "id");

        $this->createIndex('idx-available-creator_id', 'available', 'creator_id', false);
        $this->addForeignKey("fk-available-creator_id", "available", "creator_id", "user", "id");

        $this->createIndex('idx-available-storage_id', 'available', 'storage_id', false);
        $this->addForeignKey("fk-available-storage_id", "available", "storage_id", "storage", "id");

        $this->createIndex('idx-available-status_id', 'available', 'status_id', false);
        $this->addForeignKey("fk-available-status_id", "available", "status_id", "product_status", "id");

        $this->createIndex('idx-available-good_id', 'available', 'good_id', false);
        $this->addForeignKey("fk-available-good_id", "available", "good_id", "goods", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-available-product_id','available');
        $this->dropIndex('idx-available-product_id','available');  

        $this->dropForeignKey('fk-available-creator_id','available');
        $this->dropIndex('idx-available-creator_id','available');  

        $this->dropForeignKey('fk-available-storage_id','available');
        $this->dropIndex('idx-available-storage_id','available');  

        $this->dropForeignKey('fk-available-status_id','available');
        $this->dropIndex('idx-available-status_id','available');

        $this->dropForeignKey('fk-available-good_id','available');
        $this->dropIndex('idx-available-good_id','available');  

        $this->dropTable('available');
    }
}

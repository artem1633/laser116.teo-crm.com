<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m201010_011721_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->comment('Ключ'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);
        $this->insert('settings', [
            'key' => 'sms',
            'value' => '',
            'label' => 'Смс шлюз',
        ]);

        $this->insert('settings', [
            'key' => 'hours',
            'value' => '',
            'label' => 'Часов смена',
        ]);

        $this->insert('settings', [
            'key' => 'paint_price',
            'value' => '',
            'label' => 'Цена покраски',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%score}}`.
 */
class m210711_061026_add_created_by_column_to_score_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('score', 'created_by', $this->integer()->comment('Кто создал'));

        $this->createIndex(
            'idx-score-created_by',
            'score',
            'created_by'
        );

        $this->addForeignKey(
            'fk-score-created_by',
            'score',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-score-created_by',
            'score'
        );

        $this->dropIndex(
            'idx-score-created_by',
            'score'
        );

        $this->dropColumn('score', 'created_by');
    }
}

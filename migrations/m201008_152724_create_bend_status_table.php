<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bend_status}}`.
 */
class m201008_152724_create_bend_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bend_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'color' => $this->string()->comment('Цвет')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bend_status}}');
    }
}

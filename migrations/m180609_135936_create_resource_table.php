<?php

use yii\db\Migration;

/**
 * Handles the creation of table `resource`.
 */
class m180609_135936_create_resource_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('resource', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->comment('Названия товара'),
            'creator_id' => $this->integer()->comment('Создатель'),
            'storage_id' => $this->integer()->comment('Склад'),
            'status_id' => $this->integer()->comment('Статус'),
            'count' => $this->integer()->comment('Количество'),
            'price' => $this->float()->comment('Цена'),
            'price_shop' => $this->float()->comment('Цена закупа'),
            'good_id' => $this->integer()->comment('Категория  товара'),
            'comment' => $this->text()->comment('Комментария'),
        ]);

        $this->createIndex('idx-resource-product_id', 'resource', 'product_id', false);
        $this->addForeignKey("fk-resource-product_id", "resource", "product_id", "product", "id");

        $this->createIndex('idx-resource-creator_id', 'resource', 'creator_id', false);
        $this->addForeignKey("fk-resource-creator_id", "resource", "creator_id", "user", "id");

        $this->createIndex('idx-resource-storage_id', 'resource', 'storage_id', false);
        $this->addForeignKey("fk-resource-storage_id", "resource", "storage_id", "storage", "id");

        $this->createIndex('idx-resource-status_id', 'resource', 'status_id', false);
        $this->addForeignKey("fk-resource-status_id", "resource", "status_id", "product_status", "id");

        $this->createIndex('idx-resource-good_id', 'resource', 'good_id', false);
        $this->addForeignKey("fk-resource-good_id", "resource", "good_id", "goods", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-resource-product_id','resource');
        $this->dropIndex('idx-resource-product_id','resource');  

        $this->dropForeignKey('fk-resource-creator_id','resource');
        $this->dropIndex('idx-resource-creator_id','resource');  

        $this->dropForeignKey('fk-resource-storage_id','resource');
        $this->dropIndex('idx-resource-storage_id','resource');

        $this->dropForeignKey('fk-resource-status_id','resource');
        $this->dropIndex('idx-resource-status_id','resource');  

        $this->dropForeignKey('fk-resource-good_id','resource');
        $this->dropIndex('idx-resource-good_id','resource');  

        $this->dropTable('resource');
    }
}

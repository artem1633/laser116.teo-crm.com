<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m210516_104846_add_new_min_columns_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'min_cut_price', $this->double()->comment('Минимальная цена резки'));
        $this->addColumn('order', 'min_bend_price', $this->double()->comment('Минимальная гибки резки'));
        $this->addColumn('order', 'min_welding_price', $this->double()->comment('Минимальная сварки резки'));
        $this->addColumn('order', 'min_painting_price', $this->double()->comment('Минимальная покраски резки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'min_cut_price');
        $this->dropColumn('order', 'min_bend_price');
        $this->dropColumn('order', 'min_welding_price');
        $this->dropColumn('order', 'min_painting_price');
    }
}

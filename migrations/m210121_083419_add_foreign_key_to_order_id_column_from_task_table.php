<?php

use yii\db\Migration;

/**
 * Class m210121_083419_add_foreign_key_to_order_id_column_from_task_table
 */
class m210121_083419_add_foreign_key_to_order_id_column_from_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-task-order_id',
            'task',
            'order_id'
        );

        $this->addForeignKey(
            'fk-task-order_id',
            'task',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-task-order_id',
            'task'
        );

        $this->dropIndex(
            'idx-task-order_id',
            'task'
        );
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m210719_101817_add_broken_user_id_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'is_broken', $this->boolean()->defaultValue(false)->comment('Брак'));
        $this->addColumn('order', 'date_broken', $this->date()->comment('Дата брака'));
        $this->addColumn('order', 'broken_user_id', $this->integer()->comment('Менеджер, который допустил брак'));

        $this->createIndex(
            'idx-order-broken_user_id',
            'order',
            'broken_user_id'
        );

        $this->addForeignKey(
            'fk-order-broken_user_id',
            'order',
            'broken_user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-broken_user_id',
            'order'
        );

        $this->dropIndex(
            'idx-order-broken_user_id',
            'order'
        );

        $this->dropColumn('order', 'broken_user_id');
        $this->dropColumn('order', 'date_broken');
        $this->dropColumn('order', 'is_broken');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order_part}}`.
 */
class m210129_074214_add_mine_column_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'mine', $this->boolean()->defaultValue(false)->comment('Наш метариал'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_part', 'mine');
    }
}

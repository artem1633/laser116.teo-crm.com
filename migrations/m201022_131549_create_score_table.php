<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%score}}`.
 */
class m201022_131549_create_score_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%score}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'amount' => $this->float()->comment('Сумма'),
            'status' => $this->integer()->defaultValue(false)->comment('Оплачен/не оплачен'),
            'inner_score' => $this->string()->comment('Внутренней счет'),
            'customer_id' => $this->integer()->comment('Контрагент'),
            'one_s' => $this->string()->comment('Связь с 1С'),
            'file' => $this->string()->comment('Файл'),
            'created_at' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-score-customer_id',
            'score',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-score-customer_id',
            'score',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-score-order_id',
            'score',
            'order_id'
        );

        $this->addForeignKey(
            'fk-score-order_id',
            'score',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-score-customer_id',
            'score'
        );

        $this->dropIndex(
            'idx-score-customer_id',
            'score'
        );

        $this->dropForeignKey(
            'fk-score-order_id',
            'score'
        );

        $this->dropIndex(
            'idx-score-order_id',
            'score'
        );

        $this->dropTable('{{%score}}');
    }
}

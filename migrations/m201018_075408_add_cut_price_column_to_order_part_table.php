<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order_part}}`.
 */
class m201018_075408_add_cut_price_column_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'cut_price', $this->float()->after('count')->comment('Цена за резку'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_part', 'cut_price');
    }
}

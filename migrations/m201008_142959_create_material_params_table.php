<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%material_params}}`.
 */
class m201008_142959_create_material_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%material_params}}', [
            'id' => $this->primaryKey(),
            'thickness' => $this->float()->comment('Толщина'),
            'width' => $this->integer()->comment('Ширина'),
            'length' => $this->integer()->comment('Длина'),
            'price' => $this->float()->comment('Цена'),
            'remainder' => $this->integer()->comment('Минимальный остаток'),
            'cut_price' => $this->float()->comment('Цена резки'),
            'inset_price' => $this->float()->comment('Цена Врезки'),
            'cut_speed' => $this->float()->comment('Скорость реза'),
            'inset_time' => $this->float()->comment('Время врезки'),
            'materials_id' => $this->integer()->comment('Материал')
        ]);
        $this->createIndex(
            'idx-material_params-materials_id',
            'material_params',
            'materials_id'
        );
        $this->addForeignKey(
            'fk-material_params-materials_id',
            'material_params',
            'materials_id',
            'materials',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-material_params-materials_id',
            'material_params'
        );
        $this->dropIndex(
            'idx-material_params-materials_id',
            'material_params'
        );
        $this->dropTable('{{%material_params}}');
    }
}


<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pattern_sms}}`.
 */
class m201008_153450_create_pattern_sms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pattern_sms}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'text' => $this->text()->comment('Текст')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pattern_sms}}');
    }
}

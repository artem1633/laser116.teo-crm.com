<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_status}}`.
 */
class m201008_155909_create_order_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'color' => $this->string()->comment('Цвет'),
            'pattern_sms_id' => $this->integer()->comment('Шаблон СМС'),
            'pattern_email_id' => $this->integer()->comment('Шаблон email')
        ]);
        $this->createIndex(
            'idx-order_status-pattern_sms_id',
            'order_status',
            'pattern_sms_id'
        );
        $this->addForeignKey(
            'fk-order_status-pattern_sms_id',
            'order_status',
            'pattern_sms_id',
            'pattern_sms',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-order_status-pattern_email_id',
            'order_status',
            'pattern_email_id'
        );
        $this->addForeignKey(
            'fk-order_status-pattern_email_id',
            'order_status',
            'pattern_email_id',
            'pattern_email',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order_status-pattern_email_id',
            'order_status'
        );
        $this->dropIndex(
            'idx-order_status-pattern_email_id',
            'order_status'
        );
        $this->dropForeignKey(
            'fk-order_status-pattern_sms_id',
            'order_status'
        );
        $this->dropIndex(
            'idx-order_status-pattern_sms_id',
            'order_status'
        );
        $this->dropTable('{{%order_status}}');
    }
}

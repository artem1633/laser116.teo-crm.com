<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%provider_contact}}`.
 */
class m201008_155402_create_provider_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%provider_contact}}', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->comment('ФИО контактного лица'),
            'position' => $this->string()->comment('Должность'),
            'mobile' => $this->string()->comment('Мобильный телефон'),
            'phone' => $this->string()->comment('Служебный телефон'),
            'email' => $this->string()->comment('email'),
            'provider_id' => $this->integer()->comment('Контрагент')
        ]);
        $this->createIndex(
            'idx-provider_contact-provider_id',
            'provider_contact',
            'provider_id'
        );
        $this->addForeignKey(
            'fk-provider_contact-provider_id',
            'provider_contact',
            'provider_id',
            'provider',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-provider_contact-provider_id',
            'provider_contact'
        );
        $this->dropIndex(
            'idx-provider_contact-provider_id',
            'provider_contact'
        );
        $this->dropTable('{{%provider_contact}}');
    }
}

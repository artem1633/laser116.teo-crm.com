<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%available}}`.
 */
class m201026_082137_add_provider_id_column_to_available_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('available', 'provider_id', $this->integer()->comment('Поставщик'));

        $this->createIndex(
            'idx-available-provider_id',
            'available',
            'provider_id'
        );

        $this->addForeignKey(
            'fk-available-provider_id',
            'available',
            'provider_id',
            'provider',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-available-provider_id',
            'available'
        );

        $this->dropIndex(
            'idx-available-provider_id',
            'available'
        );

        $this->dropColumn('available', 'provider_id');
    }
}

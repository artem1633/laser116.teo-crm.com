<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage`.
 */
class m180609_105727_create_storage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('storage', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'user_id' => $this->integer(),
            'atelier_id' => $this->integer(),
            'is_main' => $this->integer()->defaultValue(0),
        ]);

        $this->createIndex('idx-storage-user_id', 'storage', 'user_id', false);
        $this->addForeignKey("fk-storage-user_id", "storage", "user_id", "user", "id");

        $this->createIndex('idx-storage-atelier_id', 'storage', 'atelier_id', false);
        $this->addForeignKey("fk-storage-atelier_id", "storage", "atelier_id", "atelier", "id");

        $this->insert('storage',array(
            'name' => 'Главный склад',
            'user_id' => null,
            'atelier_id' => null,
            'is_main' => 1,
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-storage-user_id','storage');
        $this->dropIndex('idx-storage-user_id','storage');  

        $this->dropForeignKey('fk-storage-atelier_id','storage');
        $this->dropIndex('idx-storage-atelier_id','storage');  

        $this->dropTable('storage');
    }
}

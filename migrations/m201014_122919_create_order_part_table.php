<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_part}}`.
 */
class m201014_122919_create_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_part}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'path_file' => $this->text()->comment('Путь к файлу дизайна'),
            'count' => $this->integer()->comment('Количество'),
            'cut_price_without_material' => $this->float()->comment('Цена резки без учета материала. Расчет резки'),
            'cut_price_for_one_detail' => $this->float()->comment('Цена резки за одну деталь. Расчет резки'),
            'cut_thickness_metal' => $this->float(2)->comment('Толщина материала. Расчет резки'),
            'cut_discount' => $this->text()->comment('Выбор скидки. Расчет резки'),
            'cut_material_id' => $this->integer()->comment('Материал. Расчет резки'),
            'cut_num' => $this->integer()->comment('Количество. Расчет резки'),
            'cut_length_each' => $this->float()->comment('Длинна каждого. Расчет резки'),
            'cut_time' => $this->float()->comment('Время резки. Расчет резки'),
            'cut_incut' => $this->integer()->comment('Кол-во врезок. Расчет резки'),
            'incut_price_per_meter' => $this->float()->comment('Цена за метр врезки'),
            'incut_total_price' => $this->float()->comment('Общая цена врезки'),
            'welding_price' => $this->float()->comment('Цена сварки'),
            'bend_num' => $this->integer()->comment('Кол-во гибов'),
            'detail_area' => $this->float()->comment('Площадь детали'),
            'digitization_price' => $this->float()->comment('Цена оцифровки'),
            'material_length' => $this->float()->comment('Длина материала'),
            'material_width' => $this->float()->comment('Ширина материала'),
            'material_owner' => $this->smallInteger()->comment('Владелец материала'),
            'material_area' => $this->float()->comment('Площадь материала'),
            'material_area_sum' => $this->float()->comment('Сумма площади материала'),
            'material_ratio' => $this->float()->comment('Коофициент'),
            'material_sheet_price' => $this->float()->comment('Цена одного листа материала'),
            'material_sheet_size' => $this->text()->comment('Размеры листа материла'),
            'material_weight' => $this->float()->comment('Вес листа материала'),
            'material_price' => $this->float()->comment('Цена материала'),
            'total_price' => $this->float()->comment('Общая сумма по позиции'),
            'created_at' => $this->dateTime()->defaultValue(null),
            'updated_at' => $this->dateTime()->defaultValue(null),
        ]);

        $this->addForeignKey(
            'fk-order_part-order_id',
            '{{%order_part}}',
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order_part-cut_material_id',
            '{{%order_part}}',
            'cut_material_id',
            '{{%materials}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_part}}');
    }
}

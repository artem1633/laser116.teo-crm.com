<?php

use yii\db\Migration;

/**
 * Class m201022_132743_add_score_id_to_payments_table
 */
class m201022_132743_add_score_id_to_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments', 'score_id', $this->integer()->comment('Счет'));

        $this->createIndex(
            'idx-payments-score_id',
            'payments',
            'score_id'
        );

        $this->addForeignKey(
            'fk-payments-score_id',
            'payments',
            'score_id',
            'score',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-payments-score_id',
            'payments'
        );

        $this->dropIndex(
            'idx-payments-score_id',
            'payments'
        );

        $this->dropColumn('payments', 'score_id');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bend_price_paint}}`.
 */
class m201012_102328_create_bend_price_paint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bend_price_paint}}', [
            'id' => $this->primaryKey(),
            'count' => $this->integer()->comment('Количество'),
            'price' => $this->integer()->comment('Цена')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bend_price_paint}}');
    }
}

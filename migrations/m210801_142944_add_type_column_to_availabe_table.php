<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%availabe}}`.
 */
class m210801_142944_add_type_column_to_availabe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('available', 'type', $this->integer()->defaultValue(1)->comment('Тип'));
        $this->addColumn('resource', 'type', $this->integer()->defaultValue(1)->comment('Тип'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resource', 'type');
        $this->dropColumn('available', 'type');
    }
}

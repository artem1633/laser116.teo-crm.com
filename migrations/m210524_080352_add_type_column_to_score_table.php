<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%score}}`.
 */
class m210524_080352_add_type_column_to_score_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('score', 'type', $this->integer()->after('order_id')->comment('Тип'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('score', 'type');
    }
}

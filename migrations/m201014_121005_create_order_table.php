<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m201014_121005_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string()->comment('Номер'),
            'customer_id' => $this->integer()->comment('Заказчик'),
            'manager_id' => $this->integer()->comment('Менеджер'),
            'status_id' => $this->integer()->comment('Статус заказа'),
            'cutting_status_id' => $this->integer()->comment('Статус резки'),
            'bend_status_id' => $this->integer()->comment('Статус гибки'),
            'welding_status_id' => $this->integer()->comment('Статус сварки'),
            'paint_status_id' => $this->integer()->comment('Статус покраски'),
            'num_set' => $this->integer()->comment('Кол-во комплектов'),
            'all_count' => $this->integer()->comment('Общее кол-во'),
            'customer_contact_id' => $this->integer()->comment('Контактное лицо'),
            'price' => $this->float()->comment('Сумма заказа'),
            'total_time' => $this->float()->comment('Кол-во времени, час'),
            'total_shift' => $this->float()->comment('Кол-во смен'),
            'pay_status' => $this->integer()->comment('Статус оплаты'),
            'pay_no_tax' => $this->smallInteger()->comment('Оплата без НДС'),
            'planned_date' => $this->date()->comment('Планируемая дата'),
            'show_discount' => $this->smallInteger()->comment('Показывать скидку'),
            'price_painting' => $this->float()->comment('Цена покраски'),
            'price_bend' => $this->float()->comment('Цена гиба'),
            'cutting_discount' => $this->float()->comment('Скидка на резку'),
            'numbering' => $this->integer()->comment('Оцифровка'),
            'price_delivery_metal' => $this->float()->comment('Цена доставки металла'),
            'round_to' => $this->float()->comment('Точность расчета(округление до)'),
            'comment' => $this->text()->comment('Комментарий'),
            'planing_date' => $this->date()->comment('Планируемая дата'),
            'created_at' => $this->dateTime()->defaultValue(null),
            'updated_at' => $this->dateTime()->defaultValue(null),
        ]);

        $this->addForeignKey(
            'fk-order-customer_id',
            '{{%order}}',
            'customer_id',
            '{{%customer}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-manager_id',
            '{{%order}}',
            'manager_id',
            '{{%user}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-status_id',
            '{{%order}}',
            'status_id',
            '{{%order_status}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->createIndex(
            'idx-order-cutting_status_id',
            'order',
            'cutting_status_id'
        );

        $this->addForeignKey(
            'fk-order-cutting_status_id',
            'order',
            'cutting_status_id',
            'cutting_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order-bend_status_id',
            'order',
            'bend_status_id'
        );

        $this->addForeignKey(
            'fk-order-bend_status_id',
            'order',
            'bend_status_id',
            'bend_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order-welding_status_id',
            'order',
            'welding_status_id'
        );

        $this->addForeignKey(
            'fk-order-welding_status_id',
            'order',
            'welding_status_id',
            'welding_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order-paint_status_id',
            'order',
            'paint_status_id'
        );

        $this->addForeignKey(
            'fk-order-paint_status_id',
            'order',
            'paint_status_id',
            'paint_status',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-order-customer_contact_id',
            'order',
            'customer_contact_id'
        );

        $this->addForeignKey(
            'fk-order-customer_contact_id',
            'order',
            'customer_contact_id',
            'customer_contact',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order}}');
    }
}

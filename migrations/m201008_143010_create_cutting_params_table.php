<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cutting_params}}`.
 */
class m201008_143010_create_cutting_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cutting_params}}', [
            'id' => $this->primaryKey(),
            'cutting_id' => $this->integer()->comment('Резка'),
            'thickness' => $this->float()->comment('Толщина'),
            'price' => $this->float()->comment('Цена')
        ]);
        $this->createIndex(
            'idx-cutting_params-cutting_id',
            'cutting_params',
            'cutting_id'
        );
        $this->addForeignKey(
            'fk-cutting_params-cutting_id',
            'cutting_params',
            'cutting_id',
            'cutting',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-cutting_params-cutting_id',
            'cutting_params'
        );
        $this->dropIndex(
            'idx-cutting_params-cutting_id',
            'cutting_params'
        );
        $this->dropTable('{{%cutting_params}}');
    }
}

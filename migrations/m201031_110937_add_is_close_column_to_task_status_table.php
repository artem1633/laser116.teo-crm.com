<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%task_status}}`.
 */
class m201031_110937_add_is_close_column_to_task_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_status', 'is_close', $this->boolean()->comment('Завершающий'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task_status', 'is_close');
    }
}

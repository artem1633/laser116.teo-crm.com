<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%resource}}`.
 */
class m201026_085338_add_provider_id_column_to_resource_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resource', 'provider_id', $this->integer()->comment('Поставщик'));

        $this->createIndex(
            'idx-resource-provider_id',
            'resource',
            'provider_id'
        );

        $this->addForeignKey(
            'fk-resource-provider_id',
            'resource',
            'provider_id',
            'provider',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-resource-provider_id',
            'resource'
        );

        $this->dropIndex(
            'idx-resource-provider_id',
            'resource'
        );

        $this->dropColumn('resource', 'provider_id');
    }
}

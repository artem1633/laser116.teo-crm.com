<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%materials}}`.
 */
class m201020_161404_add_additional_coefficient_column_to_materials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('materials', 'additional_coefficient', $this->boolean()->comment('Дополнительный коэффициент'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('materials', 'additional_coefficient');
    }
}

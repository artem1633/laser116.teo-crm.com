<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pattern_document}}`.
 */
class m201008_153848_create_pattern_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pattern_document}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'text' => $this->text()->comment('Текст')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pattern_document}}');
    }
}

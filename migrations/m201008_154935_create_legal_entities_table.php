<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%legal_entities}}`.
 */
class m201008_154935_create_legal_entities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%legal_entities}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'address' => $this->string()->comment('Адрес'),
            'director' => $this->string()->comment('Директор'),
            'post_address' => $this->string()->comment('Почтовый адрес'),
            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),
            'r_s' => $this->string()->comment('Р/С'),
            'k_s' => $this->string()->comment('К/С'),
            'bic' => $this->string()->comment('БИК')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%legal_entities}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%paint_status}}`.
 */
class m201008_153206_create_paint_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%paint_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'color' => $this->string()->comment('Цвет')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%paint_status}}');
    }
}

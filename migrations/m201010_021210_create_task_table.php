<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%task}}`.
 */
class m201010_021210_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'name' => $this->string()->comment('Описание задачи'),
            'executor_id' => $this->integer()->comment('Исполнитель'),
            'deadline' => $this->date()->comment('Срок выполнения'),
            'priority' => $this->integer()->comment('Приоритет'),
            'comment' => $this->text()->comment('Комментарии'),
            'task_status_id' => $this->integer()->comment('Статус задачи'),
            'created_at' => $this->dateTime()->comment('Дата создания'),
            'updated_at' => $this->dateTime()->comment('Дата изменения'),
            'created_by' => $this->integer()->comment('Кто создал')
        ]);
        $this->createIndex(
            'idx-task-created_by',
            'task',
            'created_by'
        );
        $this->addForeignKey(
            'fk-task-created_by',
            'task',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-task-executor_id',
            'task',
            'executor_id'
        );
        $this->addForeignKey(
            'fk-task-executor_id',
            'task',
            'executor_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-task-task_status_id',
            'task',
            'task_status_id'
        );
        $this->addForeignKey(
            'fk-task-task_status_id',
            'task',
            'task_status_id',
            'task_status',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-task-task_status_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-task_status_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-executor_id',
            'task'
        );
        $this->dropIndex(
            'idx-task-executor_id',
            'task'
        );
        $this->dropForeignKey(
            'fk-task-created_by',
            'task'
        );
        $this->dropIndex(
            'idx-task-created_by',
            'task'
        );

        $this->dropTable('{{%task}}');
    }
}

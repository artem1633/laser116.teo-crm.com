<?php

use yii\db\Migration;

/**
 * Class m201026_090745_alter_product_id_column_for_resource_table
 */
class m201026_090745_alter_product_id_column_for_resource_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-resource-product_id','resource');

        $this->addForeignKey("fk-resource-product_id", "resource", "product_id", "materials", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bend_price}}`.
 */
class m201008_151955_create_bend_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bend_price}}', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer()->comment('Материал'),
            'count' => $this->integer()->comment('Количество'),
            'price' => $this->integer()->comment('Цена')
        ]);

        $this->createIndex(
            'idx-bend_price-material_id',
            'bend_price',
            'material_id'
        );

        $this->addForeignKey(
            'fk-bend_price-material_id',
            'bend_price',
            'material_id',
            'materials',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bend_price}}');

        $this->dropForeignKey(
            'fk-bend_price-material_id',
            'bend_price'
        );

        $this->dropIndex(
            'idx-bend_price-material_id',
            'bend_price'
        );
    }
}

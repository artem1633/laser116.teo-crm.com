<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m201016_111320_add_new_columns_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'name', $this->string()->after('id')->comment('Имя'));
        $this->addColumn('order', 'duration', $this->integer()->comment('Срок изготовления'));
        $this->addColumn('order', 'payment_date', $this->date()->comment('Имя'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'name');
        $this->dropColumn('order', 'duration');
        $this->dropColumn('order', 'payment_date');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order}}`.
 */
class m210123_070503_add_material_price_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'material_price', $this->float()->comment('Цена материала'));
        $this->addColumn('order', 'cut_price', $this->float()->comment('Цена за резку'));
        $this->addColumn('order', 'total_bend_price', $this->float()->comment('Сумма по гибке'));
        $this->addColumn('order', 'total_welding_price', $this->float()->comment('Сумма по сварке'));
        $this->addColumn('order', 'total_paint_price', $this->float()->comment('Сумма по покраске'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'material_price');
        $this->dropColumn('order', 'cut_price');
        $this->dropColumn('order', 'total_bend_price');
        $this->dropColumn('order', 'total_welding_price');
        $this->dropColumn('order', 'total_paint_price');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%commercial_offer}}`.
 */
class m210719_131757_create_commercial_offer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%commercial_offer}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'customer_id' => $this->integer()->comment('Контрагент'),
            'path' => $this->string()->comment('Файл'),
            'sum' => $this->double()->comment('Сумма'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-commercial_offer-order_id',
            'commercial_offer',
            'order_id'
        );

        $this->addForeignKey(
            'fk-commercial_offer-order_id',
            'commercial_offer',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-commercial_offer-customer_id',
            'commercial_offer',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-commercial_offer-customer_id',
            'commercial_offer',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-commercial_offer-customer_id',
            'commercial_offer'
        );

        $this->dropIndex(
            'idx-commercial_offer-customer_id',
            'commercial_offer'
        );

        $this->dropForeignKey(
            'fk-commercial_offer-order_id',
            'commercial_offer'
        );

        $this->dropIndex(
            'idx-commercial_offer-order_id',
            'commercial_offer'
        );

        $this->dropTable('{{%commercial_offer}}');
    }
}

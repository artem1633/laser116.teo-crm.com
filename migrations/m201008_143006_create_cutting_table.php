<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cutting}}`.
 */
class m201008_143006_create_cutting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cutting}}', [
            'id' => $this->primaryKey(),
            'materials_id' => $this->integer()->comment('Материал'),
        ]);
        $this->createIndex(
            'idx-cutting-materials_id',
            'cutting',
            'materials_id'
        );
        $this->addForeignKey(
            'fk-cutting-materials_id',
            'cutting',
            'materials_id',
            'materials',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-cutting-materials_id',
            'cutting'
        );
        $this->dropIndex(
            'idx-cutting-materials_id',
            'cutting'
        );
        $this->dropTable('{{%cutting}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%resource}}`.
 */
class m201026_085353_add_category_id_column_to_resource_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resource', 'category_id', $this->integer()->comment('Категория'));

        $this->createIndex(
            'idx-resource-category_id',
            'resource',
            'category_id'
        );

        $this->addForeignKey(
            'fk-resource-category_id',
            'resource',
            'category_id',
            'material_params',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-resource-category_id',
            'resource'
        );

        $this->dropIndex(
            'idx-resource-category_id',
            'resource'
        );

        $this->dropColumn('resource', 'category_id');
    }
}

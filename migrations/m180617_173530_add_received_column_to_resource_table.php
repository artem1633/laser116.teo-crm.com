<?php

use yii\db\Migration;

/**
 * Handles adding received to table `resource`.
 */
class m180617_173530_add_received_column_to_resource_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('available', 'received', $this->boolean()->defaultValue(0));
        $this->addColumn('resource', 'received', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('available', 'received');
        $this->dropColumn('resource', 'received');
    }
}

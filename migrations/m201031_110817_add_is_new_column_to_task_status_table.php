<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%task_status}}`.
 */
class m201031_110817_add_is_new_column_to_task_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_status', 'is_new', $this->boolean()->comment('Новый'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task_status', 'is_new');
    }
}

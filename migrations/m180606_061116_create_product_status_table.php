<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_status`.
 */
class m180606_061116_create_product_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('product_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
        
        $this->insert('product_status',array(
            'name' => 'На складе',
        ));

        $this->insert('product_status',array(
            'name' => 'Поступили на склад',
        ));

        $this->insert('product_status',array(
            'name' => 'Необходимо заказать',
        ));

        $this->insert('product_status',array(
            'name' => 'Ожидание',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('product_status');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pattern_email}}`.
 */
class m201008_153605_create_pattern_email_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pattern_email}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'text' => $this->text()->comment('Текст')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pattern_email}}');
    }
}

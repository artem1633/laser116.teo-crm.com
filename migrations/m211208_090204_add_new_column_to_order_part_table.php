<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order_part}}`.
 */
class m211208_090204_add_new_column_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'material_weight_painted', $this->double()->comment('Вес покрашенных деталей'));
        $this->addColumn('order_part', 'total_painted', $this->double()->comment('Всего покрашенных деталей'));

        $this->addColumn('order_part', 'material_weight_cutted', $this->double()->comment('Вес нарезанных деталей'));
        $this->addColumn('order_part', 'total_cutted', $this->double()->comment('Всего нарезано деталей'));
        $this->addColumn('order_part', 'users_cutted', $this->text()->comment('Пользователи нарезали'));

        $this->addColumn('order_part', 'material_weight_bended', $this->double()->comment('Вес согнутых деталей'));
        $this->addColumn('order_part', 'total_bended', $this->double()->comment('Всего согнутых деталей'));
        $this->addColumn('order_part', 'users_bended', $this->text()->comment('Пользователи согнули'));

        $this->addColumn('order_part', 'material_weight_welded', $this->double()->comment('Вес сваренных деталей'));
        $this->addColumn('order_part', 'total_welded', $this->double()->comment('Всего сваренных деталей'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_part', 'material_weight_painted');
        $this->dropColumn('order_part', 'total_painted');

        $this->dropColumn('order_part', 'material_weight_cutted');
        $this->dropColumn('order_part', 'total_cutted');
        $this->dropColumn('order_part', 'users_cutted');

        $this->dropColumn('order_part', 'material_weight_bended');
        $this->dropColumn('order_part', 'total_bended');
        $this->dropColumn('order_part', 'users_bended');

        $this->dropColumn('order_part', 'material_weight_welded');
        $this->dropColumn('order_part', 'total_welded');
    }
}

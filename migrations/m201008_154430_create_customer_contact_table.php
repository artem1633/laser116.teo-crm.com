<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customer_contact}}`.
 */
class m201008_154430_create_customer_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%customer_contact}}', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->comment('ФИО контактного лица'),
            'position' => $this->string()->comment('Должность'),
            'mobile' => $this->string()->comment('Мобильный телефон'),
            'phone' => $this->string()->comment('Служебный телефон'),
            'email' => $this->string()->comment('email'),
            'customer_id' => $this->integer()->comment('Контрагент')
        ]);
        $this->createIndex(
            'idx-customer_contact-customer_id',
            'customer_contact',
            'customer_id'
        );
        $this->addForeignKey(
            'fk-customer_contact-customer_id',
            'customer_contact',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-customer_contact-customer_id',
            'customer_contact'
        );
        $this->dropIndex(
            'idx-customer_contact-customer_id',
            'customer_contact'
        );
        $this->dropTable('{{%customer_contact}}');
    }
}

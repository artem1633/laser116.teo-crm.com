<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%provider}}`.
 */
class m201020_074208_add_new_columns_to_provider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data = [
            'official_name' => $this->string()->after('name')->comment('Оф. наименование'),
            'address' => $this->string()->comment('Адрес'),
            'post_address' => $this->string()->comment('Почтовый адрес'),
            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),
            'r_s' => $this->string()->comment('Р/С'),
            'k_s' => $this->string()->comment('К/С'),
            'bic' => $this->string()->comment('БИК'),
        ];

        foreach ($data as $name => $value){
            $this->addColumn('provider', $name, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $data = [
            'official_name' => $this->string()->after('name')->comment('Оф. наименование'),
            'address' => $this->string()->comment('Адрес'),
            'post_address' => $this->string()->comment('Почтовый адрес'),
            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),
            'r_s' => $this->string()->comment('Р/С'),
            'k_s' => $this->string()->comment('К/С'),
            'bic' => $this->string()->comment('БИК')
        ];

        foreach ($data as $name => $value){
            $this->dropColumn('provider', $name);
        }
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%customer}}`.
 */
class m210122_104002_add_manager_id_column_to_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('customer', 'manager_id', $this->integer()->comment('Менеджер'));

        $this->createIndex(
            'idx-customer-manager_id',
            'customer',
            'manager_id'
        );

        $this->addForeignKey(
            'fk-customer-manager_id',
            'customer',
            'manager_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-customer-manager_id',
            'customer'
        );

        $this->dropIndex(
            'idx-customer-manager_id',
            'customer'
        );

        $this->dropColumn('customer', 'manager_id');
    }
}

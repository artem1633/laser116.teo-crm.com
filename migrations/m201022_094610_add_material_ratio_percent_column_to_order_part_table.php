<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order_part}}`.
 */
class m201022_094610_add_material_ratio_percent_column_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'material_ratio_percent', $this->float()->comment('Коэф (%)'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_part', 'material_ratio_percent');
    }
}

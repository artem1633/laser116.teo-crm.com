<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payments}}`.
 */
class m201010_021219_create_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payments}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'cashbox_id' => $this->integer()->comment('Касса'),
            'name' => $this->string()->comment('Название'),
            'sum' => $this->string()->comment('Сумма'),
            'type' => $this->integer()->comment('Тип'),
            'comment' => $this->text()->comment('Комментарий'),
            'created_at' => $this->dateTime()->comment('Дата создания'),
            'created_by' => $this->integer()->comment('Кто создал')
        ]);
        $this->createIndex(
            'idx-payments-created_by',
            'payments',
            'created_by'
        );
        $this->addForeignKey(
            'fk-payments-created_by',
            'payments',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->createIndex(
            'idx-payments-cashbox_id',
            'payments',
            'cashbox_id'
        );
        $this->addForeignKey(
            'fk-payments-cashbox_id',
            'payments',
            'cashbox_id',
            'cashbox',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-payments-cashbox_id',
            'payments'
        );
        $this->dropIndex(
            'idx-payments-cashbox_id',
            'payments'
        );
        $this->dropForeignKey(
            'fk-payments-created_by',
            'payments'
        );
        $this->dropIndex(
            'idx-payments-created_by',
            'payments'
        );
        $this->dropTable('{{%payments}}');
    }
}

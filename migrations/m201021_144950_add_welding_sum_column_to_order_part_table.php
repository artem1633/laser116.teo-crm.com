<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%order_part}}`.
 */
class m201021_144950_add_welding_sum_column_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'welding_sum', $this->float()->comment('Сумма сварки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_part', 'welding_sum');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%available}}`.
 */
class m201026_083443_add_category_id_column_to_available_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('available', 'category_id', $this->integer()->comment('Категория'));

        $this->createIndex(
            'idx-available-category_id',
            'available',
            'category_id'
        );

        $this->addForeignKey(
            'fk-available-category_id',
            'available',
            'category_id',
            'material_params',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-available-category_id',
            'available'
        );

        $this->dropIndex(
            'idx-available-category_id',
            'available'
        );

        $this->dropColumn('available', 'category_id');
    }
}

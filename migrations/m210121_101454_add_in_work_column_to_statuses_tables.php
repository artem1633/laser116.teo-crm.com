<?php

use yii\db\Migration;

/**
 * Class m210121_101454_add_in_work_column_to_statuses_tables
 */
class m210121_101454_add_in_work_column_to_statuses_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bend_status', 'in_work', $this->boolean()->defaultValue(false)->comment('В работе'));
        $this->addColumn('cutting_status', 'in_work', $this->boolean()->defaultValue(false)->comment('В работе'));
        $this->addColumn('paint_status', 'in_work', $this->boolean()->defaultValue(false)->comment('В работе'));
        $this->addColumn('welding_status', 'in_work', $this->boolean()->defaultValue(false)->comment('В работе'));
        $this->addColumn('order_status', 'in_work', $this->boolean()->defaultValue(false)->comment('В работе'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bend_status', 'in_work');
        $this->dropColumn('cutting_status', 'in_work');
        $this->dropColumn('paint_status', 'in_work');
        $this->dropColumn('welding_status', 'in_work');
        $this->dropColumn('order_status', 'in_work');
    }
}

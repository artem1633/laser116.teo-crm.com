<?php

use yii\db\Migration;

/**
 * Handles adding number to table `available`.
 */
class m180614_040441_add_number_column_to_available_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('available', 'number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('available', 'number');
    }
}

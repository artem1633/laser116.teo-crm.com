<?php

namespace app\assets\pages;

use yii\web\AssetBundle;

/**
 * Class OrderViewPageAsset
 * @package app\assets\pages
 */
class OrderViewPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/order-view.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset'
    ];
}
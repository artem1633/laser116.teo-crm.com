<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaintStatus */

?>
<div class="paint-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

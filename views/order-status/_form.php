<?php

use app\models\PatternEmail;
use app\models\PatternSms;
use kartik\color\ColorInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
        'options' => ['placeholder' => 'Выберите цвет ...'],
    ]); ?>


    <?= $form->field($model, 'pattern_sms_id')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(PatternSms::find()->orderBy('name asc')->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'выберите ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>


    <?= $form->field($model, 'pattern_email_id')->widget(Select2::classname(), [
        'data' =>  ArrayHelper::map(PatternEmail::find()->orderBy('name asc')->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'выберите ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'in_work')->checkbox() ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

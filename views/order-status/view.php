<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderStatus */
?>
<div class="order-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
            'pattern_sms_id',
            'pattern_email_id:email',
        ],
    ]) ?>

</div>

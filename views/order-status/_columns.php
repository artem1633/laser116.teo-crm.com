<?php

use app\models\PatternEmail;
use app\models\PatternSms;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'attribute' => 'color',
        'value' => function ($model, $key, $index, $widget) {
            return "<span class='badge'  style='background-color: {$model->color}'> </span>  ";
        },
        'width' => '',
        'filterType' => GridView::FILTER_COLOR,
        'filterWidgetOptions' => [
            'showDefaultPalette' => true,
        ],
        'vAlign' => 'middle',
        'format' => 'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pattern_sms_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(PatternSms::find()->where(['id' => $data->pattern_sms_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(PatternSms::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
        ],
        'width' => '10%',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pattern_email_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(PatternEmail::find()->where(['id' => $data->pattern_sms_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(PatternEmail::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
        ],
        'width' => '10%',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{update}{delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },

//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];   
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CuttingStatus */
?>
<div class="cutting-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

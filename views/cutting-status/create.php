<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CuttingStatus */

?>
<div class="cutting-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

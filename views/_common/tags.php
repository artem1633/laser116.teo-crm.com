<div class="row">
    <div class="col-md-12">
        <h4>Тэги</h4>
    </div>
</div>
<div class="row">
    <div class="<?= ($size == 'big' ? 'col-md-3' : 'col-md-6') ?>">
        <p><b>{order.name}</b> - Имя заказа</p>
        <p><b>{order.number}</b> - Номер</p>
        <p><b>{order.customer.name}</b> - Наименование заказчика</p>
        <p><b>{order.customer.official_name}</b> - Официальное наименование заказчика</p>
        <p><b>{order.customer.address}</b> - Адрес заказчика</p>
        <p><b>{order.customer.post_address}</b> - Почтовый адрес заказчика</p>
        <p><b>{order.customer.director}</b> - Директор заказчика</p>
        <p><b>{order.customer.r_s}</b> - Р/C клиента</p>
        <p><b>{order.customer.k_s}</b> - К/C клиента</p>
        <p><b>{order.customer.inn}</b> - ИНН заказчика</p>
        <p><b>{order.customer.kpp}</b> - КПП заказчика</p>
        <p><b>{order.customer.bic}</b> - БИК банка заказчика</p>
        <p><b>{order.manager.name}</b> - Наименование менеджера</p>
        <p><b>{order.status.name}</b> - Статус заказа</p>
        <p><b>{order.cuttingStatus.name}</b> - Статус резки</p>
        <p><b>{order.bendStatus.name}</b> - Статус гибки</p>
        <p><b>{order.weldingStatus.name}</b> - Статус сварки</p>
        <p><b>{order.paintStatus.name}</b> - Статус покраски</p>
        <p><b>{order.num_set}</b> - Кол-во комплектов</p>
    </div>
    <div class="<?= ($size == 'big' ? 'col-md-3' : 'col-md-6') ?>">
        <p><b>{order.all_count}</b> - Общее кол-во</p>
        <p><b>{order.customerContact.name}</b> - Контактное лицо</p>
        <p><b>{order.price}</b> - Сумма заказа</p>
        <p><b>{order.total_time}</b> - Кол-во времени, час</p>
        <p><b>{order.total_shift}</b> - Кол-во смен</p>
        <p><b>{order.pay_status}</b> - Статус оплаты</p>
        <p><b>{order.pay_no_tax}</b> - Оплата без НДС</p>
        <p><b>{order.planned_date}</b> - Планируемая дата</p>
        <p><b>{order.show_discount}</b> - Показывать скидку</p>
        <p><b>{order.price_painting}</b> - Цена покраски</p>
        <p><b>{order.price_bend}</b> - Цена гиба</p>
        <p><b>{order.cutting_discount}</b> - Скидка на резку</p>
        <p><b>{order.numbering}</b> - Оцифровка</p>
        <p><b>{order.price_delivery_metal}</b> - Цена доставки металла</p>
        <p><b>{order.round_to}</b> - Точность расчета(округление до)</p>
        <p><b>{order.comment}</b> - Комментарий</p>
        <p><b>{order.planing_date}</b> - Планируемая дата</p>
        <p><b>{order.created_at}</b> - Дата заказа</p>
        <p><b>{order.duration}</b> - Срок изготовления, дни</p>
        <p><b>{order.payment_date}</b> - Дата оплаты</p>
    </div>
</div>
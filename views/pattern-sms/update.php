<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatternSms */
?>
<div class="pattern-sms-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

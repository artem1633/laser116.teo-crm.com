<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PatternSms */
?>
<div class="pattern-sms-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'text:ntext',
        ],
    ]) ?>

</div>

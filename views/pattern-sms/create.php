<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PatternSms */

?>
<div class="pattern-sms-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

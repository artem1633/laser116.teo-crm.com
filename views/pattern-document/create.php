<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PatternDocument */

?>
<div class="pattern-document-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatternDocument */
?>
<div class="pattern-document-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

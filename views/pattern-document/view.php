<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PatternDocument */
?>
<div class="pattern-document-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'text:ntext',
        ],
    ]) ?>

</div>

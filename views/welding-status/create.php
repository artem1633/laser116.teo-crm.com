<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WeldingStatus */

?>
<div class="welding-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

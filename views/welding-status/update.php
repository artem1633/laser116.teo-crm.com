<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WeldingStatus */
?>
<div class="welding-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

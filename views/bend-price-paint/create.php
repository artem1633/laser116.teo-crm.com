<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BendPrice */

?>
<div class="bend-price-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

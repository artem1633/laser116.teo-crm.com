<?php
use app\models\PaintStatus;
use app\models\PatternDocument;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use app\models\CustomerContact;
use app\models\OrderStatus;
use app\models\CuttingStatus;
use app\models\BendStatus;
use app\models\WeldingStatus;

/* @var $this yii\web\View */
/* @var $model \app\models\forms\DocumentForm */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'templateId')->widget(Select2::class, array(
                'data' => ArrayHelper::map(PatternDocument::find()->all(), 'id', 'name'),
                'pluginEvents' => [
                    'change' => 'function(){
                        var value = $(this).val();
                        
                        $.get("/pattern-document/view-ajax?id="+value, function(response){
                            $("#documentform-name").val(response.name);
                            CKEDITOR.instances["documentform-content"].setData(response.text);
                        });
                    }'
                ],
                'options' => ['allowClear' => true, 'placeholder' => 'Выберите'],
            )) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'content')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]); ?>
        </div>
    </div>

    <div class="hidden">
        <?= $form->field($model, 'type') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
use app\models\BendStatus;
use app\models\Customer;
use app\models\CuttingStatus;
use app\models\Order;
use app\models\OrderStatus;
use app\models\PaintStatus;
use app\models\User;
use app\models\WeldingStatus;
use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'attribute' => 'created_at',
        'format' => ['date', 'php:Y-m-d'],
        'width' => '2%',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'number',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'value' => 'customer.name',
        'filter' => ArrayHelper::map(Customer::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'label' => 'Маршрут заказа',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'label' => 'Заказ',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_id',
        'value' => 'status.name',
        'content' => function($model){
            return Editable::widget([
                'model'=>$model,
                'name' => 'status_id',
                'asPopover' => true,
                'value' => \yii\helpers\ArrayHelper::getValue($model, 'status.name'),
                'size'=>'md',
                'inputType' => Editable::INPUT_SELECT2,
                'formOptions' => [
                    'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'status.name'],
                ],
                'options' => ['class' => 'form-control', 'data' => \yii\helpers\ArrayHelper::map(OrderStatus::find()->all(), 'id', 'name')],
                'pluginOptions' => [
                    'placeholder' => 'Выберите',
                    'allowClear' => true,
                ],
            ]);
        },
        'filter' => ArrayHelper::map(OrderStatus::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'label' => 'Часы',
        'content' => function($model){
            return 1;
        },
    ],
    [
        'label' => 'Смены',
        'content' => function($model){
            return 1;
        },
    ],
    [
        'attribute' => 'bend_num',
    ],
    [
        'label' => 'Метров сварки',
    ],
    [
        'label' => 'Кв. м. покр.',
    ],
    [
        'attribute' => 'planned_date',
        'label' => 'Дата готовности и планируемая дата',
    ],
    [
        'attribute' => 'comment',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'manager_id',
        'value' => 'manager.name',
        'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
    [
        'attribute' => 'material_price',
    ],
    [
        'attribute' => 'cut_price',
    ],
    [
        'attribute' => 'total_bend_price',
    ],
    [
        'attribute' => 'total_welding_price',
    ],
    [
        'attribute' => 'total_paint_price',
    ],
    [
        'attribute' => 'duration',
    ],
    [
        'attribute' => 'numbering',
    ],
    [
        'attribute' => 'pay_status',
        'value' => function($model){
            return ArrayHelper::getValue(Order::paymentStatusLabels(), $model->pay_status);
        }
    ],
    [
        'attribute' => 'payment_date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cutting_status_id',
        'value' => 'cuttingStatus.name',
        'content' => function($model){
            return Editable::widget([
                'model'=>$model,
                'name' => 'cutting_status_id',
                'asPopover' => true,
                'value' => \yii\helpers\ArrayHelper::getValue($model, 'cuttingStatus.name'),
                'size'=>'md',
                'inputType' => Editable::INPUT_SELECT2,
                'formOptions' => [
                    'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'cuttingStatus.name'],
                ],
                'options' => ['class' => 'form-control', 'data' => \yii\helpers\ArrayHelper::map(CuttingStatus::find()->all(), 'id', 'name')],
                'pluginOptions' => [
                    'placeholder' => 'Выберите',
                    'allowClear' => true,
                ],
            ]);
        },
        'filter' => ArrayHelper::map(CuttingStatus::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => '', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => false,
                'tokenSeparators' => [','],
            ],
        ],
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'bend_status_id',
         'value' => 'bendStatus.name',
         'content' => function($model){
             return Editable::widget([
                 'model'=>$model,
                 'name' => 'bend_status_id',
                 'asPopover' => true,
                 'value' => \yii\helpers\ArrayHelper::getValue($model, 'bendStatus.name'),
                 'size'=>'md',
                 'inputType' => Editable::INPUT_SELECT2,
                 'formOptions' => [
                     'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'bendStatus.name'],
                 ],
                 'options' => ['class' => 'form-control', 'data' => \yii\helpers\ArrayHelper::map(BendStatus::find()->all(), 'id', 'name')],
                 'pluginOptions' => [
                     'placeholder' => 'Выберите',
                     'allowClear' => true,
                 ],
             ]);
         },
         'filter' => ArrayHelper::map(BendStatus::find()->asArray()->all(), 'id', 'name'),
         'filterType' => GridView::FILTER_SELECT2,
         'filterWidgetOptions' => [
             'options' => ['prompt' => '', 'multiple' => true],
             'pluginOptions' => [
                 'allowClear' => true,
                 'tags' => false,
                 'tokenSeparators' => [','],
             ],
         ],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'welding_status_id',
         'value' => 'weldingStatus.name',
         'content' => function($model){
             return Editable::widget([
                 'model'=>$model,
                 'name' => 'welding_status_id',
                 'asPopover' => true,
                 'value' => \yii\helpers\ArrayHelper::getValue($model, 'weldingStatus.name'),
                 'size'=>'md',
                 'inputType' => Editable::INPUT_SELECT2,
                 'formOptions' => [
                     'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'weldingStatus.name'],
                 ],
                 'options' => ['class' => 'form-control', 'data' => \yii\helpers\ArrayHelper::map(WeldingStatus::find()->all(), 'id', 'name')],
                 'pluginOptions' => [
                     'placeholder' => 'Выберите',
                     'allowClear' => true,
                 ],
             ]);
         },
         'filter' => ArrayHelper::map(WeldingStatus::find()->asArray()->all(), 'id', 'name'),
         'filterType' => GridView::FILTER_SELECT2,
         'filterWidgetOptions' => [
             'options' => ['prompt' => '', 'multiple' => true],
             'pluginOptions' => [
                 'allowClear' => true,
                 'tags' => false,
                 'tokenSeparators' => [','],
             ],
         ],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'paint_status_id',
         'value' => 'paintStatus.name',
         'content' => function($model){
             return Editable::widget([
                 'model'=>$model,
                 'name' => 'paint_status_id',
                 'asPopover' => true,
                 'value' => \yii\helpers\ArrayHelper::getValue($model, 'paintStatus.name'),
                 'size'=>'md',
                 'inputType' => Editable::INPUT_SELECT2,
                 'formOptions' => [
                     'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'paintStatus.name'],
                 ],
                 'options' => ['class' => 'form-control', 'data' => \yii\helpers\ArrayHelper::map(PaintStatus::find()->all(), 'id', 'name'),
                     ],
                 'pluginOptions' => [
                     'placeholder' => 'Выберите',
                     'allowClear' => true,
                 ],
             ]);
         },
         'filter' => ArrayHelper::map(PaintStatus::find()->asArray()->all(), 'id', 'name'),
         'filterType' => GridView::FILTER_SELECT2,
         'filterWidgetOptions' => [
             'options' => ['prompt' => '', 'multiple' => true],
             'pluginOptions' => [
                 'allowClear' => true,
                 'tags' => false,
                 'tokenSeparators' => [','],
             ],
         ],
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'num_set',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'all_count',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'customer_contact_id',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'price',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'total_time',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'total_shift',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'pay_status',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'pay_no_tax',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'planned_date',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'show_discount',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'price_painting',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'price_bend',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'cutting_discount',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'numbering',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'price_delivery_metal',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'round_to',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'comment',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'planing_date',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
//        'template' => '{}',
        'template' => '{get-word} {add-task} {question} {copy} {view} {update} {delete}',
        'buttons' => [
            'get-word' => function($url, $model){
                return \yii\helpers\Html::a('<i class="fa fa-file-word-o"></i>', ['order/get-word', 'id' => $model->id], ['data-pjax' => 0, 'title'=>'Скачать КП',
                    'data-toggle'=>'tooltip',
                ]);
            },
            'question' => function($url, $model){
                return \yii\helpers\Html::a('<i class="fa fa-question"></i>', ['task/question', 'orderId' => $model->id, 'reloadPjaxReload' => '#crud-datatable-pjax'], ['role' => 'modal-remote', 'title'=>'Задать вопрос',
                    'data-toggle'=>'tooltip',
                ]);
            },
            'add-task' => function($url, $model){
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span>', ['task/create', 'orderId' => $model->id, 'reloadPjaxReload' => '#crud-datatable-pjax'], ['role' => 'modal-remote', 'title'=>'Add task',
                    'data-toggle'=>'tooltip',
                ]);
            },
            'copy' => function($url, $model){
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['role' => 'modal-remote', 'title'=>'Copy',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Are you sure?',
                    'data-confirm-message'=>'Are you sure want to copy this item']);
            }
        ],
        'viewOptions'=>['title'=>'View','data-toggle'=>'tooltip', 'data-pjax' => 0, 'class' => 'view-href'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   
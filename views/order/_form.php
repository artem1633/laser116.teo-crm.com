<?php
use app\models\PaintStatus;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use app\models\CustomerContact;
use app\models\OrderStatus;
use app\models\CuttingStatus;
use app\models\BendStatus;
use app\models\WeldingStatus;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row hidden">
        <div class="col-md-4">
            <?= $form->field($model, 'planned_date')->input('date') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'duration')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'payment_date')->input('date') ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'customer_id')->widget(\kartik\select2\Select2::class, [
//                'data' => ArrayHelper::map(Customer::find()->all(), 'id', 'name'),
                'data' => ArrayHelper::map(Customer::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Выберите'],
                'pluginEvents' => [
                    "change" => "function() {
                        var customerId = $('#order-customer_id').val();
                        
                        $.get('/customer/get-contact-list?customer_id='+customerId, function(response){
                            $('#order-customer_contact_id').html(response);
                        });
                    }",
//                    'minimumInputLength' => 3,
//                    'language' => [
//                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
//                    ],
//                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
//                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
                'pluginOptions' => [
                    'ajax' => [
                        'url'=>\yii\helpers\Url::to(['/order/customers']),
                        'dataType'=>'json',
                        'delay'=>250,
                        'cache'=>true,
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'customer_contact_id')->widget(\kartik\select2\Select2::class, [
                'data' => $model->customer_id != null ? ArrayHelper::map(CustomerContact::find()->where(['customer_id' => $model->customer_id])->all(), 'id', 'fio') : [],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'manager_id')->widget(\kartik\select2\Select2::class, array(
                'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
            )) ?>
        </div>
    </div>

    <div class="row hidden">
        <div class="col-md-2">
            <?= $form->field($model, 'num_set')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'numbering')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'price_bend')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'files')->textarea() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textInput() ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

use app\models\BendStatus;
use app\models\CuttingStatus;
use app\models\Order;
use app\models\OrderStatus;
use app\models\PaintStatus;
use app\models\WeldingStatus;
use kartik\editable\Editable;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

\johnitvn\ajaxcrud\CrudAsset::register($this);

\app\assets\pages\OrderViewPageAsset::register($this);





?>

<style>

    #main-detail-view th, #main-detail-view td {
        font-size: 11px !important;
        padding: 5px 10px !important;
    }

    .panel-footer {
        padding: 0 !important;
    }

    #main-detail-view .form-control {
        height: 28px;
        padding: 3px 6px;
    }

    #main-detail-view .form-group input[type=checkbox] {
        margin-top: 27px;
    }

</style>

<div class="order-view">

    <?php if(Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN || Yii::$app->user->identity->role == \app\models\User::ROLE_MANAGER): ?>
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Информация</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <?= Html::a('<span class="glyphicon glyphicon-file"></span>', ['order/document', 'id' => $model->id], ['role' => 'modal-remote', 'class' => 'btn btn-success btn-xs']) ?>
                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['order/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-container-info-container'], ['role' => 'modal-remote', 'class' => 'btn btn-warning btn-xs']) ?>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?= DetailView::widget([
                            'id' => 'main-detail-view',
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'number',
                                [
                                    'attribute' => 'customer_id',
                                    'value' => function($model){
                                        return ArrayHelper::getValue($model, 'customer.name');
                                    },
                                ],
                                [
                                    'attribute' => 'customer_contact_id',
                                    'value' => function($model){
                                        return ArrayHelper::getValue($model, 'customerContact.fio');
                                    },
                                ],
                                [
                                    'attribute' => 'manager_id',
                                    'value' => function($model){
                                        return ArrayHelper::getValue($model, 'manager.name');
                                    },
                                ],
                                [
                                    'attribute' => 'price',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return '<span data-total="total_total_price">'.$model->price.'</span>';
                                    }
                                ],
//                        'num_set',
//                        'all_count',
//                        'total_time',
//                        'total_shift',
//                        'pay_no_tax',ph
                                'planned_date',
//                        'show_discount',
//                        'price_painting',
//                        'price_bend',
//                        'cutting_discount',
//                        'numbering',
//                        'price_delivery_metal',
//                        'round_to',
                                'comment:ntext',
                                [
                                    'attribute' => 'planing_date',
                                    'label' => 'Срок изготовления',
                                    'value' => function($model){
                                        return "8-17 дней";
                                    },
                                ],
                                'created_at',
                                [
                                    'label' => 'Брак',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return $this->render('_broken_form', ['model' => $model]);
                                    }
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Данные для расчета</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?= $this->render('_form_count', [
                            'model' => $model,
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Минимальные цена на услуги</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" style="display:none;">
                        <?= DetailView::widget([
                            'id' => 'main-detail-view',
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'min_cut_price',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'min_cut_price',
                                            'asPopover' => true,
                                            'value' => ArrayHelper::getValue($model, 'min_cut_price'),
                                            'size'=>'md',
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'min_cut_price'],
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'min_bend_price',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'min_bend_price',
                                            'asPopover' => true,
                                            'value' => ArrayHelper::getValue($model, 'min_bend_price'),
                                            'size'=>'md',
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'min_bend_price'],
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'min_welding_price',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'min_welding_price',
                                            'asPopover' => true,
                                            'value' => ArrayHelper::getValue($model, 'min_welding_price'),
                                            'size'=>'md',
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'min_welding_price'],
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'min_painting_price',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'min_painting_price',
                                            'asPopover' => true,
                                            'value' => ArrayHelper::getValue($model, 'min_painting_price'),
                                            'size'=>'md',
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'min_painting_price'],
                                            ],
                                        ]);
                                    },
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Счета</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?= $this->render('@app/views/score/index', [
                            'searchModel' => $scoreSearchModel,
                            'dataProvider' => $scoreDataProvider,
                            'order' => $model,
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Статусы заказа</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body" style="display:none;">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                [
                                    'attribute' => 'status_id',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'status_id',
                                            'asPopover' => true,
                                            'value' => ArrayHelper::getValue($model, 'status.name'),
                                            'size'=>'md',
                                            'inputType' => Editable::INPUT_SELECT2,
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'status.name'],
                                            ],
                                            'options' => array('class' => 'form-control', 'data' => ArrayHelper::map(OrderStatus::find()->all(), 'id', 'name')),
                                            'pluginOptions' => [
                                                'placeholder' => 'Выберите',
                                                'allowClear' => true,
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'cutting_status_id',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'cutting_status_id',
                                            'asPopover' => true,
                                            'value' => \yii\helpers\ArrayHelper::getValue($model, 'cuttingStatus.name'),
                                            'size'=>'md',
                                            'inputType' => Editable::INPUT_SELECT2,
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'cuttingStatus.name'],
                                            ],
                                            'options' => ['class' => 'form-control', 'data' => ArrayHelper::map(CuttingStatus::find()->all(), 'id', 'name')],
                                            'pluginOptions' => [
                                                'placeholder' => 'Выберите',
                                                'allowClear' => true,
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'bend_status_id',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'bend_status_id',
                                            'asPopover' => true,
                                            'value' => \yii\helpers\ArrayHelper::getValue($model, 'bendStatus.name'),
                                            'size'=>'md',
                                            'inputType' => Editable::INPUT_SELECT2,
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'bendStatus.name'],
                                            ],
                                            'options' => ['class' => 'form-control', 'data' => ArrayHelper::map(BendStatus::find()->all(), 'id', 'name')],
                                            'pluginOptions' => [
                                                'placeholder' => 'Выберите',
                                                'allowClear' => true,
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'welding_status_id',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'welding_status_id',
                                            'asPopover' => true,
                                            'value' => \yii\helpers\ArrayHelper::getValue($model, 'weldingStatus.name'),
                                            'size'=>'md',
                                            'inputType' => Editable::INPUT_SELECT2,
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'weldingStatus.name'],
                                            ],
                                            'options' => ['class' => 'form-control', 'data' => ArrayHelper::map(WeldingStatus::find()->all(), 'id', 'name')],
                                            'pluginOptions' => [
                                                'placeholder' => 'Выберите',
                                                'allowClear' => true,
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'paint_status_id',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Editable::widget([
                                            'model'=>$model,
                                            'name' => 'paint_status_id',
                                            'asPopover' => true,
                                            'value' => \yii\helpers\ArrayHelper::getValue($model, 'paintStatus.name'),
                                            'size'=>'md',
                                            'inputType' => Editable::INPUT_SELECT2,
                                            'formOptions' => [
                                                'action' => ['order/editable-change', 'id' => $model->id, 'output' => 'paintStatus.name'],
                                            ],
                                            'options' => ['class' => 'form-control', 'data' => ArrayHelper::map(PaintStatus::find()->all(), 'id', 'name'),
                                            ],
                                            'pluginOptions' => [
                                                'placeholder' => 'Выберите',
                                                'allowClear' => true,
                                            ],
                                        ]);
                                    },
                                ],
                                [
                                    'attribute' => 'pay_status',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return ArrayHelper::getValue(Order::paymentStatusLabels(), $model->pay_status);
                                    }
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Задачи</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <?php
                    $panelStyle = 'display: none;';

                    foreach ($taskDataProvider->models as $task){
                        $taskStatus = $task->taskStatus;

                        if($taskStatus){
                            if($taskStatus->is_close == false){
                                $panelStyle = null;
                            }
                        } else {
                            $panelStyle = null;
                        }
                    }

                    ?>
                    <div class="panel-body" style="<?= $panelStyle ?>">
                        <?= $this->render('_task_index', [
                            'searchModel' => $taskSearchModel,
                            'dataProvider' => $taskDataProvider,
                            'order' => $model,
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-3">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Коммерческие предложения</h4>
                        <div class="panel-heading-btn" style="margin-top: -20px;">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?= $this->render('@app/views/commercial-offer/index', [
                            'searchModel' => $offerSearchModel,
                            'dataProvider' => $offerDataProvider,
                            'order' => $model,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Детали</h4>
                </div>
                <div class="panel-body back-fone">
                    <?= $this->render('@app/views/order-part/index', [
                        'searchModel' => $partSearchModel,
                        'dataProvider' => $partDataProvider
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg', 'tabindex' => false],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS
$('#common-data-form input').change(function(){
    $(this).submit();
});

$('#common-data-form').on('beforeSubmit', function () {
    var form = $(this);
    // отправляем данные на сервер
    $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serializeArray()
        }
    )
    .done(function(data) {
       if(data.success) {
          // данные сохранены
        } else {
          // сервер вернул ошибку и не сохранил наши данные
        }
    })
    .fail(function () {
         // не удалось выполнить запрос к серверу
    })

    return false; // отменяем отправку данных формы
})
JS;

$this->registerJs($script);

?>

<?php

$script = <<< JS
$('#ajaxCrudModal').on('hidden.bs.modal', function (e) {
  $('#ajaxCrudModal').addClass('modal-slg');
})
JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>

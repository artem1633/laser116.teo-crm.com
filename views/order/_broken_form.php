<?php
use app\models\PaintStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use app\models\CustomerContact;
use app\models\OrderStatus;
use app\models\CuttingStatus;
use app\models\BendStatus;
use app\models\WeldingStatus;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="order-form">

    <?php $form = ActiveForm::begin(['id' => 'broken-form', 'action' => ['order/update', 'id' => $model->id]]); ?>

        <div class="row">
            <div class="col-md-1">
                <?= $form->field($model, 'is_broken')->checkbox([], false)->label(false) ?>
            </div>
            <div id="broken-fields">
                <div class="col-md-7">
                    <?= $form->field($model, 'date_broken')->input('date', ['disabled' => !$model->is_broken]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'broken_user_id')->dropDownList(ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'), ['prompt' => 'Выберите', 'disabled' => !$model->is_broken])->label('Менеджер') ?>
                </div>
            </div>
        </div>


    <?php ActiveForm::end(); ?>

</div>


<?php

$script = <<< JS

$('#broken-form input[type=checkbox]').change(function(){
    if($(this).is(':checked')){
        $('#broken-fields input, #broken-fields select').attr('disabled', false);
    } else {
        $('#broken-fields input, #broken-fields select').attr('disabled', true);
    }
});

$('#broken-form input, #broken-form select').change(function(){
    $('#broken-form').submit();
});

JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>
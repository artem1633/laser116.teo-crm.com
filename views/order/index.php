<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>


    <style>
        table th, table td {
            padding: 3px !important;
            font-size: 11px !important;
        }
    </style>

<div class="order-index">
    <div id="ajaxCrudDatatable">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Заказы</h4>
            </div>
            <div class="panel-body">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'containerOptions' => ['style' => 'overflow: auto;'],
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_columns.php'),
                    'rowOptions' => function($model){
                        return ['style' => 'cursor: pointer;'];
                    },
                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                            ['role'=>'modal-remote','title'=> 'Добавить задачу','class'=>'btn btn-success']).'&nbsp;'.
                        Html::a('<i class="fa fa-repeat"></i>', [''],
                            ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after'=>BulkButtonWidget::widget([
                                'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                    ["bulk-delete"] ,
                                    [
                                        "class"=>"btn btn-danger btn-xs",
                                        'role'=>'modal-remote-bulk',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                    ]),
                            ]).
                            '<div class="clearfix"></div>',
                    ]
                ])?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg', 'tabindex' => false],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS
$('[data-key]').click(function(e){
    if($(e.target).is('td')){
        var id = $(this).data('key');
        window.location = '/order/view?id='+id;
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
<?php

use app\components\helpers\ColorManager;
use app\models\TaskStatus;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $order \app\models\Order */

$this->title = 'Задачи';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
$statuses = TaskStatus::find()->all();
?>
<style>
    <?php foreach($statuses as $status): ?>
    .table tbody tr.status-<?=$status->id?> td {
        background-color: <?=$status->color?>;
        border-color: <?=ColorManager::darkenColor($status->color, 1.07)?>
    }
    <?php endforeach; ?>
</style>
<div class="panel panel-inverse position-index">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'containerOptions' => ['style' => 'overflow-y: auto;'],
                'pjax'=>true,
                'rowOptions' => function($data){
                    if($data->task_status_id){
                        return ['class' => 'status-'.$data->task_status_id];
                    }
                },
                'columns' =>  [
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => '20px',
                    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
                    // [
                    // 'class'=>'\kartik\grid\DataColumn',
                    // 'attribute'=>'id',
                    // ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'order_id',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'name',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'task_status_id',
                        'content' => function($data){
                            $status = ArrayHelper::getColumn(TaskStatus::find()->where(['id' => $data->task_status_id])->all(), 'name');
                            return implode('',$status);

                        },
                        'filter' => ArrayHelper::map(TaskStatus::find()->asArray()->all(), 'id', 'name'),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['prompt' => ''],
                            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
                        ],
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'created_at',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'updated_at',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'created_by',
                        'content' => function($data){
                            $status = ArrayHelper::getColumn(User::find()->where(['id' => $data->created_by])->all(), 'name');
                            return implode('',$status);

                        },
                        'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'options' => ['prompt' => ''],
                            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'dropdown' => false,
                        'vAlign'=>'middle',
                        'urlCreator' => function($action, $model, $key, $index) {
                            return Url::to(["task/{$action}",'id'=>$key, 'orderId' => $model->order_id]);
                        },
                        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
                        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
                        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-toggle'=>'tooltip',
                            'data-confirm-title'=>'Удаление',
                            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
                    ],

                ],
                'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['task/create', 'orderId' => $order->id],
                        ['role'=>'modal-remote','title'=> 'Добавить задачу','class'=>'btn btn-success',
                                'onclick' => '$("#ajaxCrudModal").removeClass("modal-slg");'
                            ]).'&nbsp;'.
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
                'panelFooterTemplate' => null,
                'striped' => true,
                'condensed' => true,
                'responsiveWrap' => false,
//                'responsive' => true,
                'panel' => [
                    'options' => ['style' => 'margin-bottom: 0; padding-bottom: 0;'],
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
                ]
            ])?>
        </div>
</div>
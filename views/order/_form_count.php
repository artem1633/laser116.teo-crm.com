<?php
use app\models\PaintStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use app\models\CustomerContact;
use app\models\OrderStatus;
use app\models\CuttingStatus;
use app\models\BendStatus;
use app\models\WeldingStatus;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */

$model->all_count = \app\models\OrderPart::find()->where(['order_id' => $model->id])->count();

?>

<div class="order-form">

    <?php $form = ActiveForm::begin(['action' => ['order/update-count', 'id' => $model->id]]) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'num_set')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'all_count')->textInput(['readonly' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'show_discount')->dropDownList([
                1 => 'Да',
                0 => 'Нет',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'price_bend', [
                'options' => ['class' => 'input-group'],
                'inputOptions' => ['class' => 'form-control', 'style' => 'display: table-cell'],
                'labelOptions' => ['class' => 'control-label', 'style' => 'display: table-row; white-space: nowrap;'],
                'template' => '{label}{input}  <div class="input-group-btn">
    <button class="btn btn-info" type="button" onclick="$(this).parent().parent().find(\'input\').val(20);"><i class="fa fa-refresh"></i></button>
  </div>',
            ])->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'cutting_discount', [
                'options' => ['class' => 'input-group'],
                'inputOptions' => ['class' => 'form-control', 'style' => 'display: table-cell'],
                'labelOptions' => ['class' => 'control-label', 'style' => 'display: table-row; white-space: nowrap;'],
                'template' => '{label}{input}  <div class="input-group-btn">
    <button class="btn btn-info" type="button" onclick="$(this).parent().parent().find(\'input\').val(0);"><i class="fa fa-refresh"></i></button>
  </div>',
            ])->textInput() ?>
        </div>
        <div class="col-md-4">
            <?php
                $setting = \app\models\Settings::findByKey('paint_price');

                $defaultValue = null;

                if($setting){
                    $defaultValue = $setting->value;
                }

                if($setting && $model->price_painting == null){
                    $model->price_painting = $setting->value;
                }
            ?>
            <?= $form->field($model, 'price_painting', [
                'options' => ['class' => 'input-group'],
                'inputOptions' => ['class' => 'form-control', 'style' => 'display: table-cell'],
                'labelOptions' => ['class' => 'control-label', 'style' => 'display: table-row; white-space: nowrap;'],
                'template' => '{label}{input}  <div class="input-group-btn">
    <button class="btn btn-info" type="button" onclick="$(this).parent().parent().find(\'input\').val(\''.$defaultValue.'\');"><i class="fa fa-refresh"></i></button>
  </div>',
            ])->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4" style="margin-top: 15px;">
            <?= $form->field($model, 'numbering')->textInput() ?>
        </div>
        <div class="col-md-4" style="margin-top: 5px;">
            <?= $form->field($model, 'price_delivery_metal', [
                'options' => ['class' => 'input-group'],
                'inputOptions' => ['class' => 'form-control', 'style' => 'display: table-cell'],
                'labelOptions' => ['class' => 'control-label', 'style' => 'display: table-row; font-size: 11px;'],
                'template' => '{label}{input}  <div class="input-group-btn">
    <button class="btn btn-info" type="button" onclick="$(this).parent().parent().find(\'input\').val(8);"><i class="fa fa-refresh"></i></button>
  </div>',
            ])->textInput() ?>
        </div>
        <div class="col-md-4" style="margin-top: 15px;">
            <?= $form->field($model, 'round_to', [
                'options' => ['class' => 'input-group'],
                'labelOptions' => ['class' => 'control-label', 'style' => 'font-size: 11px;'],
            ])->dropDownList([
                4 => 100,
                3 => 10,
                2 => 1,
                1 => 0.1,

            ])->label('Точность расчета') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'pay_no_tax')->dropDownList([
                1 => 'Да',
                0 => 'Нет',
            ]) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

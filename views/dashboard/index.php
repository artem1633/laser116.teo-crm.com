<?php

/** @var $this \yii\web\View */
/** @var $cashiers \app\models\Cashbox[] */

use kartik\form\ActiveForm;
use yii\helpers\Html;

?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <?= Html::a('Сегодня', ['#'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Вчера', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Неделя', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Пред. неделя', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Месяц', ['#'], ['class' => 'btn btn-default']) ?>
                <?= Html::a('Пред. месяц', ['#'], ['class' => 'btn btn-default']) ?>
            </div>
            <div class="col-md-2">
                <?php $form = ActiveForm::begin(['method' => 'GET']); ?>
                    <?= \kartik\daterange\DateRangePicker::widget([
                        'name' => 'period',
                        'options' => [
                            'autocomplete' => 'off',
                            'class' => 'form-control',
                            'placeholder' => 'Выберите даты'
                        ],
                    ]) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Обороты денежных средств</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <?php foreach ($cashiers as $cashier): ?>
                            <th scope="col"><?= $cashier['name'] ?></th>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row"><b>Приход</b></th>
                        <?php foreach ($cashiers as $cashier): ?>
                            <td><b><?= $cashier['debit'] ? '<span class="text-success">'.Yii::$app->formatter->asCurrency($cashier['debit'], 'rub').'</span>' : 0 ?></b></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <th scope="row"><b>Расход</b></th>
                        <?php foreach ($cashiers as $cashier): ?>
                            <td><b><?= $cashier['credit'] ? '<span class="text-warning">'.Yii::$app->formatter->asCurrency($cashier['credit'], 'rub').'</span>' : 0 ?></b></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <th scope="row"><b>Остаток</b></th>
                        <?php foreach ($cashiers as $cashier): ?>
                            <td><b><?= $cashier['balance'] ?  Yii::$app->formatter->asCurrency($cashier['balance'], 'rub') : 0?></b></td>
                        <?php endforeach; ?>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="row">
            <?php if(Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN): ?>
                <div class="col-md-6">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Показатели по менеджерам</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <canvas id="charContent" width="400" height="150%"></canvas>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-md-6">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Задолжность</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Контрагент</th>
                                <th scope="col">Задолжность</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($creditors as $creditorId => $value): ?>
                                <tr>
                                    <td><?php

                                        $creditor = \app\models\Customer::findOne($creditorId);

                                        if($creditor){
                                            echo $creditor->name;
                                        }

                                        ?></td>
                                    <td><?= Yii::$app->formatter->asCurrency($value, 'rub') ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td>Всего</td>
                                <td><?= Yii::$app->formatter->asCurrency(array_sum($creditors), 'rub') ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php if(Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Показатели компании</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <canvas id="charContent2" width="400" height="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>
    <?php if(Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN): ?>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">Диаграмма по менеджерам</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <canvas id="charContent3" width="400" height="350%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Показатели по оцифровке</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-sm btn-icon btn-circle btn-default" data-original-title="" title="" data-init="true" style="margin-top: -38px;"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <canvas id="charContent4" width="400" height="350%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>

<script>
    <?php if(Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN): ?>
    var ctx = document.getElementById('charContent').getContext('2d');
    var charContent = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?=json_encode(array_values($ordersLabels))?>,
            datasets: <?=json_encode($ordersDataset)?>
        }
    });
    var ctx = document.getElementById('charContent2').getContext('2d');
    var charContent = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?= json_encode(array_values($payedLabels)) ?>,
            datasets: [{
                label: 'Прибыль',
                data: <?=json_encode($payedOrderData)?>,
                backgroundColor: "rgba(23, 191, 205,0.6)"
            }, {
                label: 'Сумма заказов',
                data: <?=json_encode($payedData)?>,
                backgroundColor: "rgba(245, 156, 26,0.6)"
            }]
        }
    });
    var ctx = document.getElementById('charContent3').getContext('2d');
    var charContent = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?=json_encode(array_values($scoreLabels))?>,
            datasets: <?=json_encode($scoreDataset)?>
        }
    });
    var ctx = document.getElementById('charContent4').getContext('2d');
    var charContent = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?=json_encode(array_values($numberingLabels))?>,
            datasets: <?=json_encode($numberingDataset)?>
        }
    });
    <?php endif; ?>
</script>
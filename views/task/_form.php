<?php

use app\models\Order;
use app\models\TaskStatus;
use app\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($createFromOrder == false): ?>
        <?= $form->field($model, 'order_id')->widget(Select2::class, array(
            'data' => ArrayHelper::map(Order::find()->all(), 'id', 'name'),
            'options' => [
                'placeholder' => 'Выберите'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        )) ?>

    <?php endif; ?>

    <?= $form->field($model, 'task_status_id')->widget(Select2::class, array(
        'data' => ArrayHelper::map(TaskStatus::find()->orderBy('name asc')->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Выберите'
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    )) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if($createFromOrder == false): ?>
        <?= $form->field($model, 'executor_id')->widget(Select2::class, array(
            'data' => ArrayHelper::map(User::find()->orderBy('name asc')->all(), 'id', 'name'),
            'options' => [
                'placeholder' => 'Выберите'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        )) ?>
    <?php endif; ?>

    <?= $form->field($model, 'deadline')->input('date') ?>

    <?= $form->field($model, 'priority')->dropDownList([
            1 => 'Высокий',
            2 => 'Средний',
            3 => 'Низкий'
    ]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

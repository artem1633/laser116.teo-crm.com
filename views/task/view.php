<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
?>
<div class="task-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'name',
            'executor_id',
            'deadline',
            'priority',
            'comment:ntext',
            'created_at',
            'updated_at',
            'created_by',
        ],
    ]) ?>

</div>

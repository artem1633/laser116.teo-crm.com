<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Task */

?>
<div class="task-create">
    <?= $this->render('_form', [
        'model' => $model,
        'createFromOrder' => $createFromOrder,
    ]) ?>
</div>

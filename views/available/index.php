<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use kartik\tabs\TabsX;

$this->title = '';
CrudAsset::register($this);

$waiting_visible = false;
$insource_visible = false;
$application_visible = false;
$part_visible = true;

if(isset($post['Available']['active_window'])) { $insource_visible = true; $part_visible = false; }
if(isset($post['Resource']['active_window'])) { $waiting_visible = true; $part_visible = false; }
if(isset($post['Application']['active_window'])) { $application_visible = true; $part_visible = false; }

?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Склад</h4>
    </div>
    <div class="panel-body">
        <?=
        TabsX::widget([
            'items' => [
                [
                    'label' => 'Остатки',
                    'options' => ['id' => 'tab1'],
                    'content' => $remainResource,
                    'active' => $part_visible,
                ],
                [
                    'label' => 'Ожидает',
                    'options' => ['id' => 'tab2'],
                    'content' => $waitingResource,
                    'active' => $waiting_visible,
                ],
                [
                    'label' => 'Оприходовано',
                    'options' => ['id' => 'tab3'],
                    'content' => $contentInResourceWaid,
                    'active' => $insource_visible,
                ],
                [
                    'label' => 'Списание материала',
                    'options' => ['id' => 'tab4'],
                    'content' => $writeoffContent,
                    'active' => false,
                ],
                /*[
                    'label' => 'Заявки',
                    'options' => ['id' => 'tab4'],
                    'content' => $application,
                    'active' => $application_visible,
                ],*/

            ],
        ]);
        ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

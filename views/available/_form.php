<?php
use app\models\MaterialParams;
use app\models\Provider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Users;

$role = Yii::$app->user->identity->role;
if($role == \app\models\User::ROLE_ADMIN) $disabled = false;
else $disabled = true;
?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title"></h4>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-5">
                <?= $form->field($model, 'product_id')->widget(kartik\select2\Select2::classname(), [
//                        'data' => $model->getProductList(),
                    'data' => ArrayHelper::map(\app\models\Materials::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите товара'],
                    'pluginEvents' => [
                        "change" => "function() 
                            {
                                $.get('set-price',
                                {
                                    'id':$(this).val()
                                },
                                    function(data) { $('#price').val(data); }     
                                );
                                
                                $.get('/materials/get-params?id='+$(this).val(), function(response){
                                    $('#available-category_id').html(response);
                                }); 
                            }",
                    ],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'category_id')->dropDownList([], [/*'prompt' => 'Выберите категорию'*/]) ?>
            </div>

            <div class="hidden">
                <?= $form->field($model, 'storage_name')->textInput(['disabled' => 'true']) ?>
            </div>
        </div>

        <?php if($model->type == 1): ?>
            <div class="row">


                <div class="col-md-5">
                    <?= $form->field($model, 'provider_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => ArrayHelper::map(Provider::find()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Выберите поставщика'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
            </div>
            <?php if($model->type == 1): ?>
                <div class="col-md-3">
                    <?= $form->field($model, 'price_shop')->textInput(['type' => 'number']) ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="row">
            <div class="col-md-9">
                <?= $form->field($model, 'comment')->textarea(['rows' => 5]) ?>
            </div>
        </div>

        <div style="display: none;">
            <?= $form->field($model, 'status_id')->dropDownList($model->getStatusList(), [/*'prompt' => 'Выберите статуса'*/]) ?>
            <?= $form->field($model, 'creator_id')->textInput() ?>
            <?= $form->field($model, 'number')->textInput() ?>
        </div>

        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить в документ' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a('Закрыть документ', ['index'],['data-pjax'=>0, 'class'=>'btn btn-warning']);?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
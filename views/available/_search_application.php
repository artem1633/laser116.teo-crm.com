<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use kartik\field\FieldRange;
use kartik\form\ActiveForm;
use kartik\touchspin\TouchSpin;
use app\models\Application;

?>
<div class="available-search">

    <?php $form = ActiveForm::begin(); ?>
         <div class="row">
            <div class="col-md-5" >
                <?= $form->field($model, 'search_product')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => Application::getProductList(),
                        'options' => [
                            'placeholder' => 'Выберите товара',
                            'value' => $post['Application']['search_product'],
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])
                ?>

            </div>
            <div class="col-md-5">
                 <?= $form->field($model, 'search_atelier')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => Application::getAtelierList(),
                        'options' => [
                            'placeholder' => 'Выберите магазина',
                            'value' => $post['Application']['search_atelier'],
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])
                ?>
            </div>    
            <div style="display: none;">
                <?= $form->field($model, 'active_window')->textInput(['value' => 1]) ?>
            </div> 
            <div class="col-md-2" >
                <div class="form-group" style="margin-top: 25px;">
                    <?= Html::submitButton('Поиск' , ['style'=>' ','class' =>  'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>

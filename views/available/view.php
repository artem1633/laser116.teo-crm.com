<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Available */
$this->title = '';
?>
<div class="box box-solid box-primary">    
        <div class="box-header">
            <div class="btn-group pull-right">
                <?= Html::a('Назад', ['/available/index'],['data-pjax'=>0, 'class'=>'btn btn-warning btn-xs']);?>
            </div>
            <h3 class="box-title">Просмотр</h3>
        </div>
        <div class="box box-default">   
            <div class="box-body">
                <div class="col-md-12" style="font-size: 10px;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <th>Товар</th>
                                <th>Поставщик</th>
                                <th>Склад</th>
                                <th>Категория товар</th>
                                <th>Статус</th>
                                <th>Кол-во</th>
                                <th>Цена</th>
                                <th>Цена закупа</th>
                            </tr>
                            <?php 
                            foreach ($result as $value) {
                            ?>
                                <tr>
                                    <td><?=$value->product->name?></td>
                                    <td><?=$value->postavshik->name?></td>
                                    <td><?=$value->storage->name?></td>
                                    <td><?=$value->good->name?></td>
                                    <td><?=$value->status->name?></td>
                                    <td><?=$value->count?></td>
                                    <td><?=$value->price?></td>
                                    <td><?=$value->price_shop?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>

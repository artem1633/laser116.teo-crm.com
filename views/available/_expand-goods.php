<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\AvailableSearch;

$session = Yii::$app->session;
$result = [];
foreach ($session['provider']->getModels() as $model) {
    if($model->number == $number) $result [] = $model->id;
}

$provider1 = AvailableSearch::searchById($result);
?>

<?=GridView::widget([
    'id'=>'crud-datatable',
    'dataProvider' => $provider1,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'product_id',
            'value' => 'product.name',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'category_id',
            'value' => 'category.thickness',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'provider_id',
            'value' => 'provider.name',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'count',
        ],
//        [
//            'class'=>'\kartik\grid\DataColumn',
//            'attribute'=>'price',
//        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'price_shop',
        ],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>

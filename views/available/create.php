<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model app\models\Available */
$this->title = 'Поступление на склад:';
?>
<div class="available-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Документы</h4>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'product_id',
                    'value'=>'product.name',
                ],
//                [
//                    'attribute' => 'storage_id',
//                    'value'=>'storage.name',
//                ],
                [
                    'attribute' => 'category_id',
                    'value'=>'category.thickness',
                ],
                [
                    'attribute' => 'provider_id',
                    'value'=>'provider.name',
                ],
                'count',
//                'price',
                'price_shop',
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}'
                ],
            ],
        ]); ?>
    </div>
</div>
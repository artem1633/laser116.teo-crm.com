<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Storage */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="storage-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
	    <div class="col-md-9">
		    <?= $form->field($tovar, 'storage_id')->dropDownList(
                $tovar->getOtherStorageList(),
                [
                    //'prompt' => 'Выберите Мастера',
                ]
                ) ?> 
		</div>	
		<div class="col-md-3">
	    	<?= $form->field($tovar, 'move_count')->textInput([]) ?>			
		</div>	
        <div style="display: none;">
	    	<?= $form->field($tovar, 'product_id')->textInput(['value' => $tovar->product->name, 'maxlength' => true, 'disabled' => true]) ?>
	    </div>		
	</div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

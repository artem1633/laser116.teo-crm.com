<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use kartik\field\FieldRange;
use kartik\form\ActiveForm;
use kartik\touchspin\TouchSpin;
use app\models\Resource;

?>
<div class="available-search">

    <?php $form = ActiveForm::begin(); ?>
         <div class="row">
            <div class="col-md-5" > 
                <label>Товары</label>
                <?= \kartik\select2\Select2::widget([
                        'name' => 'tovars',
                        'data' => Resource::getProductList(),
                        'value' => $post['tovars'],
                        'options' => [
                            'placeholder' => 'Выберите товара',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])
                ?>

            </div>
            <div class="col-md-5">
                <label>Склады</label>
                 <?= \kartik\select2\Select2::widget([
                        'name' => 'storages',
                        'data' => Resource::getStorageList(),
                        'value' => $post['storages'],
                        'options' => [
                            'placeholder' => 'Выберите склада',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])
                ?>
            </div>     
            <div class="col-md-2" >
                <div class="form-group" style="margin-top: 25px;">
                    <?= Html::submitButton('Поиск' , ['style'=>' ','class' =>  'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>

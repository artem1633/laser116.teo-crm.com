<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\Resource;
use app\models\ResourceSearch;

CrudAsset::register($this);

$numbers = [];
$models_id = [];
$result = [];
foreach ($dataProvider->getModels() as $model) 
{
	$numbers [] = $model->number;
	$models_id [] = $model->id;
}
$numbers = array_unique($numbers);

foreach ($numbers as $number) {
	foreach ($dataProvider->getModels() as $model) 
	{
		if($model->number == $number){ $result [] = $model->id; break;}
	}
}

$provider = ResourceSearch::searchById($result);
$session = Yii::$app->session;
$session['provider_resource'] = $dataProvider;

?>
    <div class="box box-default">  
        <div class="box-body">
            <?= $this->render('_search_resource', ['model' => new Resource(), 'post'=>$post]); ?> 
       </div>
    </div>
    
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'waiting',
            'dataProvider' => $provider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($model, $key, $index, $grid) {
			    return ['data-id' => $model->id, 'data-toggle'=>'tooltip'];
			},
            'pjax'=>true,
            'columns' => [
			    [
			        'class' => 'kartik\grid\SerialColumn',
			        'width' => '30px',
			    ],
			    [
			        'class'=>'kartik\grid\ExpandRowColumn', 
			        'width'=>'50px',
			        'value'=>function ($model, $key, $index, $column) {
			            return GridView::ROW_COLLAPSED;
			        },
			        'detail'=>function ($model, $key, $index, $column) {
			            return \Yii::$app->controller->renderPartial('_expand-resource', ['number'=>$model->number]);
			        },
			        'headerOptions'=>['class'=>'kartik-sheet-style'],
			        'expandOneOnly'=>true
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'number',
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'creator_id',
			        'content' => function($data){
			        	return $data->creator->name;
			        }
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'date_cr',
			    ],
			    [
			        'class'    => 'kartik\grid\ActionColumn',
			        'template' => '{leadEnter} {leadView} {leadDelete}',
			        'buttons'  => [

			            'leadView' => function ($url, $model) {
			                $url = Url::to(['/available/create', 'id' => $model->number, 'storage_id' => 1, 'status' => 3]);
			                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title'=>'', 'data-toggle'=>'tooltip', 'data-pjax' => 0]);
			            },
			            'leadMove' => function ($url, $model) {
				                $url = Url::to(['/available/move-storage-waiting', 'id' => $model->id, 'type' => 'waiting', 'forceReload' => '#waiting-pjax']);
				                return Html::a('<span class="glyphicon glyphicon-log-in"></span>', $url, ['role'=>'modal-remote','title'=>'Поступило', 'data-toggle'=>'tooltip']);
			            },
			            'leadEnter' => function ($url, $model) {
			            	if($model->received == 0){
				                $url = Url::to(['/available/move-storage-waiting', 'id' => $model->id, 'type' => 'waiting', 'forceReload' => '#waiting-pjax'], ['role'=>'modal-remote',]);
				                return Html::a('<span class="glyphicon glyphicon-log-in"></span>', $url, [
				                	'role'=>'modal-remote','title'=>'Поступило', 
				                	'data-confirm'=>false, 'data-method'=>false,
					                'data-request-method'=>'post',
					                'data-toggle'=>'tooltip',
					                'data-confirm-title'=>'Подтвердите действие',
					                'data-confirm-message'=>'Вы уверены переместить товаров?',
				                ]);
				            }
			            },
			            'leadUpdate' => function ($url, $model) {
			                $url = Url::to(['/resource/update', 'id' => $model->id]);
			                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
			            },
			            'leadDelete' => function ($url, $model) {
			            	if($model->received == 0){
				                $url = Url::to(['/resource/delete-resource', 'id' => $model->id]);
				                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
				                    'role'=>'modal-remote','title'=>'', 
				                          'data-confirm'=>false, 'data-method'=>false,
				                          'data-request-method'=>'post',
				                          'data-toggle'=>'tooltip',
				                          'data-confirm-title'=>'Подтвердите действие',
				                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
				                ]);
				            }
			            },
			        ]
			    ]
			],
			'toolbar'=> [
                [
                	'content'=> Html::a('Создать', ['/available/create', 'status' => 3], ['data-pjax'=>'0','title'=> 'Создать','class'=>'btn btn-primary']) . '{export}' 
            	],
            ],  
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
        ])?>
    </div>
<?php
$this->registerJs("
	$('#waiting-container tbody td').css('cursor', 'pointer');
    $('#waiting-container tr td').on('dblclick', function(e){
	    var id = $(this).closest('tr').data('id');
	    location.href = '" . Url::to(['/resource/view']) . "?id=' +id;
	});

");
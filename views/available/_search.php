<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;

?>
<div class="available-search">
    <?php $form = ActiveForm::begin(); ?>
         <div class="row">
            <div class="col-md-5" > 
                 <?= $form->field($model, 'tovar')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getProductList(),
                    'options' => [
                        'placeholder' => 'Выберите товара',
                        'value' => ArrayHelper::getValue($post, 'Available.tovar'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-5">
            </div>            
            <div style="display: none;">
                <?= $form->field($model, 'active_window')->textInput(['value' => 1]) ?>
            </div>
            <div class="col-md-2" >
                <div class="form-group" style="margin-top: 25px;">
                    <?= Html::submitButton('Поиск' , ['style'=>' ','class' =>  'btn btn-primary']) ?>
                </div>
            </div>

        </div>
    <?php ActiveForm::end(); ?>
</div>

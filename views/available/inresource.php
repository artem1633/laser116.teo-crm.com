﻿<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Available;
use app\models\AvailableSearch;
use yii\helpers\ArrayHelper;

CrudAsset::register($this);

$numbers = [];
$models_id = [];
$result = [];
foreach ($dataProvider->getModels() as $model) 
{
	$numbers [] = $model->number;
	$models_id [] = $model->id;
}
$numbers = array_unique($numbers);

foreach ($numbers as $number) {
	$available = Available::find()->where(['number' => $number])->filterWhere(['like', 'product_id', ArrayHelper::getValue($post, 'Available.tovar')])->andFilterWhere(['like', 'storage_id', ArrayHelper::getValue($post, 'Available.storages')])->one();
	if($available != null) $result [] = $available->id;
}

$provider = AvailableSearch::searchById($result);
$session = Yii::$app->session;
$session['provider'] = $dataProvider;

?>
	<div class="box box-default">  
        <div class="box-body">
            <?= $this->render('_search', ['model' => new \app\models\Available(), 'post'=>$post]); ?> 
       </div>
    </div>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id'=>'available',
            'dataProvider' => $provider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($model, $key, $index, $grid) {
			    return ['data-id' => $model->id];
			},
            'pjax'=>true,
            'columns' => [
			    [
			        'class' => 'kartik\grid\SerialColumn',
			        'width' => '30px',
			    ],
			    [
			        'class'=>'kartik\grid\ExpandRowColumn', 
			        'width'=>'50px',
			        'value'=>function ($model, $key, $index, $column) {
			            return GridView::ROW_COLLAPSED;
			        },
			        'detail'=>function ($model, $key, $index, $column) {

			            return \Yii::$app->controller->renderPartial('_expand-goods', ['number'=>$model->number, /*'provider' => $provider*/]);
			        },
			        'headerOptions'=>['class'=>'kartik-sheet-style'],
			        'expandOneOnly'=>true
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'number',
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'creator_id',
			        'content' => function($data){
			        	return $data->creator->name;
			        }
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'date_cr',
			    ],
			    [
			        'class'    => 'kartik\grid\ActionColumn',
			        'template' => '{leadUpdate}{leadView}{leadDelete}',
			        'buttons'  => [

			            'leadView' => function ($url, $model) {
			                $url = Url::to(['/available/view-available', 'id' => $model->id]);
			                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
			            },
			            'leadMove' => function ($url, $model) {
			                $url = Url::to(['/available/move-storage', 'id' => $model->id, 'type' => 'available', 'forceReload' => '#available-pjax']);
			                return Html::a('<span class="glyphicon glyphicon-share"></span>', $url, ['role'=>'modal-remote','title'=>'Перемещения', 'data-toggle'=>'tooltip']);
			            },
			            'leadUpdate' => function ($url, $model) {
			                $url = Url::to(['/available/update', 'id' => $model->id]);
			                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
			            },
			            'leadDelete' => function ($url, $model) {
			                $url = Url::to(['/available/delete-available', 'id' => $model->id]);
			                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
			                    'role'=>'modal-remote','title'=>'', 
			                          'data-confirm'=>false, 'data-method'=>false,
			                          'data-request-method'=>'post',
			                          'data-toggle'=>'tooltip',
			                          'data-confirm-title'=>'Подтвердите действие',
			                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
			                ]);
			            },
			        ]
			    ]
			],
			'toolbar'=> [
                [
                	'content'=> Html::a('Создать', ['/available/create', 'status' => 1], ['data-pjax'=>'0','title'=> 'Создать','class'=>'btn btn-primary']) . '{export}' 
            	],
            ],  
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
        ])?>
    </div>
<?php
$this->registerJs("
	$('#available-container  tbody td').css('cursor', 'pointer');

    $('#available-container tr td').on('dblclick', function(e){
	    var id = $(this).closest('tr').data('id');
	    location.href = '" . Url::to(['/available/view']) . "?id=' +id;
	});

");
?>
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

?>
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'remain',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
			    [
			        'class' => 'kartik\grid\SerialColumn',
			        'width' => '30px',
			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'product_id',
			        'value' => 'product.name',
			    ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'category_id',
                    'value' => 'category.thickness',
                ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'creator_id',
                    'value' => 'creator.name',
			    ],
//			    [
//			        'class'=>'\kartik\grid\DataColumn',
//			        'attribute'=>'postavshik_id',
//                    'value' => 'postavshik.name',
//			    ],
			    [
			        'class'=>'\kartik\grid\DataColumn',
			        'attribute'=>'count',
			    ],
//			    [
//			        'class'=>'\kartik\grid\DataColumn',
//			        'attribute'=>'price',
//			    ],
//			    [
//			        'class'=>'\kartik\grid\DataColumn',
//			        'attribute'=>'price_shop',
//			    ],
			    [
			        'class'    => 'kartik\grid\ActionColumn',
			        'template' => '{leadDelete}',
			        'buttons'  => [

			            'leadView' => function ($url, $model) {
			                $url = Url::to(['/resource/view', 'id' => $model->id]);
			                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
			            },
			            'leadMove' => function ($url, $model) {
			                $url = Url::to(['/available/move-storage', 'id' => $model->id, 'type' => 'resource', 'forceReload' => '#remain-pjax']);
			                return Html::a('<span class="glyphicon glyphicon-share"></span>', $url, ['role'=>'modal-remote','title'=>'Перемещения', 'data-toggle'=>'tooltip']);
			            },
			            'leadUpdate' => function ($url, $model) {
			                $url = Url::to(['/resource/update', 'id' => $model->id]);
			                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
			            },
			            'leadDelete' => function ($url, $model) {
			                $url = Url::to(['/resource/delete', 'id' => $model->id]);
			                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
			                    'role'=>'modal-remote','title'=>'', 
			                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
			                          'data-request-method'=>'post',
			                          'data-toggle'=>'tooltip',
			                          'data-confirm-title'=>'Подтвердите действие',
			                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
			                ]);
			            },
			        ]
			    ]
			],
			'toolbar'=> [
                [
                	'{export}' 
            	],
            ],  
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
        ])?>
    </div>

<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CommercialOffer */
?>
<div class="commercial-offer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'customer_id',
            'path',
            'sum',
            'created_at',
        ],
    ]) ?>

</div>

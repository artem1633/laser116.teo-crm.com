<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CommercialOffer */

?>
<div class="commercial-offer-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

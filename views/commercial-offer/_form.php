<?php
use app\models\Customer;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CommercialOffer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commercial-offer-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'sum')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'customer_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(Customer::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput() ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>

<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommercialOfferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $order \app\models\Order */

?>

            <?=GridView::widget([
                'id'=>'crud-commercial-offer-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_columns.php'),
    'containerOptions' => ['style' => 'overflow-y: auto;'],
                'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['commercial-offer/create', 'orderId' => $order->id, 'containerPjaxReload' => '#crud-commercial-offer-datatable-pjax'],
                        ['role'=>'modal-remote','title'=> 'Добавить предложение','class'=>'btn btn-success']),
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                'headingOptions' => ['style' => 'display: none;'],
                'after'=>BulkButtonWidget::widget([
                'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                ["commercial-offer/bulk-delete"] ,
                [
                "class"=>"btn btn-danger btn-xs",
                'role'=>'modal-remote-bulk',
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
                'data-confirm-title'=>'Вы уверены?',
                'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                ]),
                ]).
                '<div class="clearfix"></div>',
                ]
            ])?>
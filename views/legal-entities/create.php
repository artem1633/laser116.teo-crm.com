<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LegalEntities */

?>
<div class="legal-entities-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

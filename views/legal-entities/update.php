<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LegalEntities */
?>
<div class="legal-entities-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cutting */
?>
<div class="cutting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

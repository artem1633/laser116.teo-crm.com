<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cutting */

?>
<div class="cutting-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

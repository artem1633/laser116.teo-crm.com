<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cutting */
?>
<div class="cutting-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'materials_id',
        ],
    ]) ?>

</div>

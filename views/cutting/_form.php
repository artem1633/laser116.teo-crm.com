<?php

use app\models\Materials;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cutting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cutting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'materials_id')->dropDownList(ArrayHelper::map(Materials::find()->orderBy('name asc')->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'params')->widget(MultipleInput::className(), [

        'id' => 'my_id',
        'min' => 0,
        'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden'
                ]
            ],
            [
                'name' => 'thickness',
                'title' => 'Толщина',
            ],
            [
                'name' => 'price',
                'enableError' => true,
                'title' => 'Цена',
            ],

        ],
    ]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

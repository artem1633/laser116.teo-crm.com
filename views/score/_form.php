<?php
use app\models\Customer;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Score */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="score-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'amount')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'inner_score')->textInput(['maxlength' => true]) ?>
        </div>
    </div>




    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'customer_id')->widget(\kartik\select2\Select2::class, [
                'data' => ArrayHelper::map(Customer::find()->all(), 'id', 'name'),
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'one_s')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fileload')->fileInput() ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

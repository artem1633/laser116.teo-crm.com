<?php
use app\models\Score;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $order \app\models\Order */

CrudAsset::register($this);

?>
<div class="score-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-score-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'containerOptions' => ['style' => 'overflow-y: auto;'],
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'rowOptions' => function($model){
                return ['style' => 'cursor: pointer;'];
            },
            'panelBeforeTemplate' =>    Html::a('Счет <i class="fa fa-plus"></i>', ['score/create', 'containerPjaxReload' => '#crud-score-datatable-pjax', 'order_id' => $order->id, 'type' => Score::TYPE_SCORE],
                    ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-success',  'onclick' => '$("#ajaxCrudModal").removeClass("modal-slg");']).' '.Html::a('Реализация <i class="fa fa-plus"></i>', ['score/create', 'containerPjaxReload' => '#crud-score-datatable-pjax', 'order_id' => $order->id, 'type' => Score::TYPE_REALIZATION],
                    ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-info',  'onclick' => '$("#ajaxCrudModal").removeClass("modal-slg");']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'options' => ['style' => 'margin-bottom: 0; padding-bottom: 0;'],
                'headingOptions' => ['style' => 'display: none;'],
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                            ["bulk-delete"] ,
                            [
                                "class"=>"btn btn-danger btn-xs",
                                'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверены?',
                                'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
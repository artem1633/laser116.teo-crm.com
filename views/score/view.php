<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Score */
?>
<div class="score-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'amount',
            'status',
            'inner_score',
            'customer_id',
            'one_s',
            'file',
            'created_at',
        ],
    ]) ?>

</div>

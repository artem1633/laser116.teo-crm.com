<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Score */
?>
<div class="score-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

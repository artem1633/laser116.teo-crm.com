<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatternEmail */
?>
<div class="pattern-email-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

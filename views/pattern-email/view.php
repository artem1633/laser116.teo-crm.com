<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PatternEmail */
?>
<div class="pattern-email-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'text:ntext',
        ],
    ]) ?>

</div>

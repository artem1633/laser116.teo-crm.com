<?php
use app\components\helpers\NumberHelper;
use app\models\MaterialParams;
use app\models\OrderPart;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderPartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Parts';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$this->registerCssFile('web/css/order-view.css');

?>
    <div class="order-part-index">
        <div id="ajaxCrudDatatable">
            <?php Pjax::begin(['id' => 'pjax-order-type-container']) ?>
            <div id="crud-datatable-container" class="table-responsive kv-grid-container my-table-style">
                <table class="kv-grid-table table table-bordered table-striped table-condensed">
                    <thead>
                    <tr>
                        <th class="kv-all-select kv-align-center kv-align-middle skip-export kv-merged-header" style="width:20px;" rowspan="3" data-col-seq="0">
                            <input type="checkbox" class="select-on-check-all" name="selection_all" value="1">
                        </th>

                        <th rowspan="2"><a href="#">Файл</a></th>

                        <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>
                            <th rowspan="2"><a href="#">Цена за резку</a></th>
                            <th rowspan="2"><a href="#">Цена резки за одну деталь</a></th>
                            <th rowspan="2"><a href="#">Цена покраски</a></th>
                            <th rowspan="2"><a href="#">Цена сварки</a></th>
                        <?php endif; ?>
                            <?php 
                                if(Yii::$app->user->identity->role == 5 || Yii::$app->user->identity->role == User::ROLE_CUTTING || Yii::$app->user->identity->role == User::ROLE_BEND || Yii::$app->user->identity->role == User::ROLE_WELDING){
                                    $colspan = 3;
                                } else {
                                    $colspan = 7;
                                }
                             ?>
                            <th colspan="<?= $colspan ?>"><a href="#">Расчет резки</a></th>
                        <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>
                            <th colspan="2"><a href="#">Расчет врезок</a></th>
                            <th rowspan="2"><a href="#">Цена сварки</a></th>
                            <th rowspan="2"><a href="#">Количество гибов</a></th>
                            <th rowspan="2"><a href="#">Стоимость гибки</a></th>
                            <th rowspan="2"><a href="#">P детали (покраска)</a></th>
                            <th rowspan="2"><a href="#">Цена оцифровки</a></th>
                            <th rowspan="2"><a href="#">Цена доп. работ 1</a></th>
                            <th rowspan="2"><a href="#">Цена доп. работ 2</a></th>
                            <th colspan="9"><a href="#">Расчет материала</a></th>
                        <?php endif; ?>

                        <th rowspan="2"><a href="#">Вес</a></th>
                        <?php if(Yii::$app->user->identity->role == 5): ?>
                            <th rowspan="2"><a href="#">Вес покрашенных деталей</a></th>
                            <th rowspan="2"><a href="#">Всего покрашенных деталей</a></th>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->role == User::ROLE_CUTTING): ?>
                            <th rowspan="2"><a href="#">Вес нарезанных деталей</a></th>
                            <th rowspan="2"><a href="#">Всего нарезано деталей</a></th>
                            <?php foreach(User::find()->where(['role' => User::ROLE_CUTTING])->all() as $user): ?>
                                <th rowspan="2"><a href="#">Нарезал <?= $user->name ?></a></th>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->role == User::ROLE_BEND): ?>
                            <th rowspan="2"><a href="#">Вес согнутых деталей</a></th>
                            <th rowspan="2"><a href="#">Всего согнутых деталей</a></th>
                            <?php foreach(User::find()->where(['role' => User::ROLE_BEND])->all() as $user): ?>
                                <th rowspan="2"><a href="#">Согнул <?= $user->name ?></a></th>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->identity->role == User::ROLE_WELDING): ?>
                            <th rowspan="2"><a href="#">Вес согнутых деталей</a></th>
                            <th rowspan="2"><a href="#">Всего согнутых деталей</a></th>
                        <?php endif; ?>

                         <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>

                        <th rowspan="2"><a href="#">Общая сумма</a></th>
                        
                        <th class="kv-align-center kv-align-middle skip-export kv-merged-header" rowspan="3">Действия
                        </th>
                            <?php endif; ?>
                    </tr>
                    <tr>
                        <th><a href="#">Материал</a></th>
                        <th><a href="#">S материала</a></th>
                        <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>
                                <th><a href="#">Скидка</a></th>
                        <?php endif; ?>
                        <th><a href="#">Кол-во</a></th>
                        <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>
                        <th><a href="#">L кажд, м</a></th>
                        <th><a href="#">Время резки</a></th>
                        <th><a href="#">Врезки</a></th>
                        <th><a href="#">Цена за метр</a></th>
                        <th><a href="#">Цена врезки</a></th>
                        <th><a href="#">Длина</a></th>
                        <th><a href="#">Ширина</a></th>
                        <th><a href="#">Чей материал</a></th>
                        <th><a href="#">Площадь детали</a></th>
                        <th><a href="#">Сумма площади</a></th>
                        <th><a href="#">Коэф.</a></th>
                        <th><a href="#">Коэф (%)</a></th>
                        <th><a href="#">Цена за лист</a></th>
                        <th><a href="#">Размеры листа</a></th>
                        <?php endif; ?>
                    </tr>
                    <!--Поиск-->
                    </thead>
                    <tbody>
                    <?php foreach ($dataProvider->models as $model): ?>
                        <?php
                        $materialParams = [];
                        if($model->cut_material_id != null){
                            $materialParams = ArrayHelper::map(MaterialParams::find()->where(['materials_id' => $model->cut_material_id])->all(), 'id', 'thickness');
                        }
                        ?>
                        <tr>
                            <td><input type="checkbox" class="kv-row-checkbox"></td>
                            <td style="width: 10%;"><?= Html::activeInput('string', $model, 'path_file', ['id' => "orderpart-cut_num-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>

                                <td data-calc="cut_price-<?=$model->id?>"><?= $model->cut_price ?></td>
                                <td data-calc="cut_price_for_one_detail-<?=$model->id?>"><?= round($model->cut_price_for_one_detail, 2) ?>₽</td>
                                <td data-calc="paint_price-<?=$model->id?>"><?= $model->paint_price ?></td>
                                <td data-calc="welding_sum-<?=$model->id?>"><?= $model->welding_sum ?></td>

                            <?php endif; ?>

                            <td><?= Html::activeDropDownList($model, 'cut_material_id', ArrayHelper::map(\app\models\Materials::find()->all(), 'id', 'name'), ['id' => "orderpart-cut_material_id-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm', 'prompt' => 'Выберите']) ?></td>
                            <td><?= Html::activeDropDownList($model, 'cut_thickness_metal', $materialParams, ['id' => "orderpart-cut_thickness_metal-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm', 'prompt' => 'Выберите']) ?></td>
                            <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>
                                <td><a href="#"><?= 0 ?></a></td>
                            <?php endif; ?>
                            <td><?= Html::activeInput('string', $model, 'cut_num', ['id' => "orderpart-cut_num-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>

                            <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>

                            <td><?= Html::activeInput('string', $model, 'cut_length_each', ['id' => "orderpart-cut_length_each-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td data-calc="cut_time-<?=$model->id?>"><?= $model->cut_time ?></td>
                            <td><?= Html::activeInput('string', $model, 'cut_incut', ['id' => "orderpart-cut_incut-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td data-calc="incut_price_per_meter-<?=$model->id?>"><?= $model->incut_price_per_meter ?>₽</td>
                            <td data-calc="incut_total_price-<?=$model->id?>"><?= $model->incut_total_price ?>₽</td>
                            <td><?= Html::activeInput('string', $model, 'welding_price', ['id' => "orderpart-welding_price-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeInput('string', $model, 'bend_num', ['id' => "orderpart-bend_num-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td data-calc="bend_price-<?=$model->id?>"><?=$model->bend_price?></td>
                            <td><?= Html::activeInput('string', $model, 'detail_area', ['id' => "orderpart-detail_area-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeInput('string', $model, 'digitization_price', ['id' => "orderpart-digitization_price-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeInput('string', $model, 'additional_word_price1', ['id' => "orderpart-additional_word_price1-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeInput('string', $model, 'additional_word_price2', ['id' => "orderpart-additional_word_price2-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeInput('string', $model, 'material_length', ['id' => "orderpart-material_length-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeInput('string', $model, 'material_width', ['id' => "orderpart-material_width-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeDropDownList($model, 'mine', [0 => 'Заказчика', 1 => 'Наш'], ['id' => "orderpart-mine-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td><?= Html::activeInput('string', $model, 'material_area', ['id' => "orderpart-material_area-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td data-calc="material_area_sum-<?=$model->id?>"><?=$model->material_area_sum?></td>
                            <td data-calc="material_ratio-<?=$model->id?>"><?=$model->material_ratio?></td>
                            <td data-calc="material_ratio_percent-<?=$model->id?>"><?=$model->material_ratio_percent?></td>
                            <td><?= Html::activeInput('string', $model, 'material_sheet_price', ['id' => "orderpart-material_sheet_price-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <td data-calc="material_sheet_size-<?=$model->id?>"><?=$model->material_sheet_size?></td>
                            <?php endif; ?>
                            <td data-calc="material_weight-<?=$model->id?>"><?=$model->material_weight?></td>
                            <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>
                            <td data-calc="total_price-<?=$model->id?>"><?=$model->total_price?></td>
                            <td class="skip-export kv-align-center kv-align-middle crud-datatable">
                                <?= Html::a('<i class="fa fa-refresh"></i>', '#', ['class' => 'btn btn-warning btn-xs', 'onclick' => 'event.preventDefault(); $(this).parent().parent().find("input[name=\"OrderPart[material_sheet_price]\"]").first().trigger("change");', 'data-pjax' => 0]) ?>
                                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['order-part/delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-xs',
                                    'title'=>'Удалить',
                                    'role' => 'modal-remote',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-toggle'=>'tooltip',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                ]) ?>
                            </td>
                            <?php endif; ?>
                            <?php if(Yii::$app->user->identity->role == 5): ?>
                                <td><?= Html::activeInput('string', $model, 'material_weight_painted', ['id' => "orderpart-material_weight_painted-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                                <td><?= Html::activeInput('string', $model, 'total_painted', ['id' => "orderpart-total_painted-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <?php endif; ?>
                            <?php if(Yii::$app->user->identity->role == User::ROLE_CUTTING): ?>
                                <td><?= Html::activeInput('string', $model, 'material_weight_cutted', ['id' => "orderpart-material_weight_cutted-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                                <td><?= Html::activeInput('string', $model, 'total_cutted', ['id' => "orderpart-total_cutted-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                                <?php foreach(User::find()->where(['role' => User::ROLE_CUTTING])->all() as $user): ?>
                                    <?php
                                        $usersData = json_decode($model->users_cutted, true);
                                        $value = isset($usersData[$user->id]) ? $usersData[$user->id] : null;
                                    ?>
                                    <td><?= Html::activeInput('string', $model, "users_cutted[$user->id]", ['id' => "orderpart-users_cutted[$user->id]-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm', 'value' => $value]) ?></td>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if(Yii::$app->user->identity->role == User::ROLE_BEND): ?>
                                <td><?= Html::activeInput('string', $model, 'material_weight_bended', ['id' => "orderpart-material_weight_bended-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                                <td><?= Html::activeInput('string', $model, 'total_bended', ['id' => "orderpart-total_bended-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                                <?php foreach(User::find()->where(['role' => User::ROLE_BEND])->all() as $user): ?>
                                    <?php
                                        $usersData = json_decode($model->users_bended, true);
                                        $value = isset($usersData[$user->id]) ? $usersData[$user->id] : null;
                                    ?>
                                    <td><?= Html::activeInput('string', $model, "users_bended[$user->id]", ['id' => "orderpart-users_bended[$user->id]-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm', 'value' => $value]) ?></td>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if(Yii::$app->user->identity->role == User::ROLE_WELDING): ?>
                                <td><?= Html::activeInput('string', $model, 'material_weight_welded', ['id' => "orderpart-material_weight_welded-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                                <td><?= Html::activeInput('string', $model, 'total_welded', ['id' => "orderpart-total_welded-{$model->id}", 'data-entity-id' => $model->id, 'class' => 'form-control form-sm']) ?></td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                    <?php if(Yii::$app->user->identity->role != 5 && Yii::$app->user->identity->role != User::ROLE_CUTTING && Yii::$app->user->identity->role != User::ROLE_BEND && Yii::$app->user->identity->role != User::ROLE_WELDING): ?>
                    <tr class="warning">
                        <?php
                        $uniqueParts = OrderPart::find()->where(['order_id' => $model->order_id])->groupBy(['cut_material_id', 'cut_thickness_metal'])->all();



                        $totalCutNum = OrderPart::find()->where(['order_id' => $model->order_id])->sum('cut_num');
                        $totalCutLengthEach = 0;
                        $totalCutIncut = 0;
                        $totalWeldingPrice = 0;
                        $totalBendNum = 0;
                        $totalDetailArea = 0;
                        $totalDigitizationPrice = 0;
                        $totalMaterialPrice = 0;
                        $totalTotalPrice = 0;
                        foreach ($dataProvider->models as $model)
                        {
                            $totalCutLengthEach += $model->cut_num * $model->cut_length_each;
                            $totalCutIncut += $model->cut_num * $model->cut_incut;
                            $totalWeldingPrice += $model->cut_num * $model->welding_price;
                            $totalBendNum += $model->cut_num * $model->bend_num;
                            $totalDetailArea += $model->cut_num * $model->detail_area;
                            $totalDigitizationPrice += $model->digitization_price;
                            $totalMaterialPrice += $model->material_price;
                            $totalTotalPrice += $model->total_price;
                        }
                        $totalCutLengthEach = $totalCutLengthEach * $model->order->num_set;
                        $totalCutIncut = $totalCutIncut * $model->order->num_set;
                        $totalWeldingPrice = $totalWeldingPrice * $model->order->num_set;
                        $totalBendNum = $totalBendNum * $model->order->num_set;
                        $totalDetailArea = $totalDetailArea * $model->order->num_set * 2.05;
                        $totalDigitizationPrice = $totalDigitizationPrice * $model->order->num_set;
                        ?>
                        <td></td>
                        <td></td>
                        <td>Резка без учета материала</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td data-total="total_cut_num"><?=$totalCutNum * $model->order->num_set?></td>
                        <td data-total="total_cut_length_each"><?=$totalCutLengthEach?></td>
                        <td></td>
                        <td data-total="total_cut_incut"><?=$totalCutIncut?></td>
                        <td></td>
                        <td></td>
                        <td data-total="total_welding_price"><?=$totalWeldingPrice?></td>
                        <td data-total="total_bend_num"><?=$totalBendNum?></td>
                        <td data-total="total_detail_area"><?=$totalDetailArea?></td>
                        <td data-total="total_digitization_price"><?=$totalDigitizationPrice?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td data-total="total_total_price"><?=$totalTotalPrice?></td>
                        <td></td>
                    </tr>
                    <tr class="warning">
                        <td></td>
                        <td></td>
                        <td>Резка лазером</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="warning">
                        <td></td>
                        <td></td>
                        <td>Толстостеная резка</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                    $totalMaterialWeight = 0;
                    $totalMaterialPrice = 0;
                    $totalDelivery = 0;
                    ?>
                    <?php foreach($uniqueParts as $part): ?>
                        <?php
                        $partMaterial = MaterialParams::findOne($part->cut_thickness_metal);
                        if($partMaterial == null){
                            continue;
                        }
                        $oneMaterialWeight = $part->material_length * $part->material_width / 1000000 * 7.85 * $partMaterial->thickness;
                        $oneMaterialWeightStr = explode('.', strval($oneMaterialWeight));
//                            if(count($oneMaterialWeightStr) == 2){
//                                $oneMaterialWeight = floatval($oneMaterialWeightStr[0].'.'.($oneMaterialWeightStr[1]+1));
//                            }
                        $oneMaterialWeight = NumberHelper::roundUp($oneMaterialWeight, 1);
                        $totalMaterialWeight = $totalMaterialWeight + $oneMaterialWeight;
                        $totalDelivery += $oneMaterialWeight * 2;


                        $part->material_price = $part->material_price + $oneMaterialWeight * 2; // TODO: Вывести в настройки 2

                        $totalWeight = 0;
                        foreach ($uniqueParts as $uPart)
                        {
                            $uPartMaterial = MaterialParams::findOne($uPart->cut_thickness_metal);
                            if($uPartMaterial == null){
                                continue;
                            }
                            $totalWeight = $totalWeight + ($uPart->material_length * $uPart->material_width / 1000000 * 7.85 * $uPartMaterial->thickness);
                        }

                        $materialParam = MaterialParams::findOne($part->cut_thickness_metal);
                        $materialRatioNoRound = 0;
                        if($materialParam && (floatval($part->material_length) * floatval($part->material_width)) > 0){
                            $materialRatioNoRound = ($materialParam->length * $materialParam->width) / ($part->material_length * $part->material_width);
                        }


                        if($totalWeight == 0 || $materialRatioNoRound == 0){
                            $materialUniquePrice = 0;
                        } else {
                            $materialUniquePrice = $part->material_sheet_price / $materialRatioNoRound + $part->order->price_delivery_metal / $totalWeight * $oneMaterialWeight;

                            $materialUniquePrice = ceil($materialUniquePrice / 100);
                            $materialUniquePrice = $materialUniquePrice * 100;
                        }

                        $totalMaterialPrice = $totalMaterialPrice + $materialUniquePrice;

                        ?>
                        <tr data-unique-row="<?=$part->id?>" class="warning">
                            <td></td>
                            <td data-unique="material-<?=$part->id?>"><?=ArrayHelper::getValue($partMaterial, 'materials.name')?></td>
                            <td data-unique="name-<?=$part->id?>">1 лист <?=$part->material_length?>x<?=$part->material_width?> (<?=$oneMaterialWeight?> кг)</td>
                            <td data-unique="price-<?=$part->id?>"><?=$materialUniquePrice?></td>
                            <td data-unique="delivery-<?=$part->id?>">Дост <?= $oneMaterialWeight * 2 ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="warning">
                        <td></td>
                        <td></td>
                        <td data-unique-total="material"><b>За материал (<?=$totalMaterialWeight?> кг.): </b></td>
                        <td data-unique-total="price"><b><?=$totalMaterialPrice?></b></td>
                        <td data-unique-total="delivery"><b><?php
//                                echo $totalDelivery;
                                echo $model->order->price_delivery_metal;

                                ?></b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?php Pjax::end() ?>
        </div>
    </div>
<?php

$script = <<< JS

Array.prototype.inArray = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++)	{
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}


$('[data-entity-id]').change(change);


function change(){
    var id = $(this).data('entity-id');
    var attr = $(this).attr('id').split('-')[1];
    var value = $(this).val();
    
    var fld = this;
    
    $.get('/order/change-part-ajax?id='+id+'&attribute='+attr+'&value='+value, function(response){
        $.each(response.data, function(attr, value){
            if($('[data-calc="'+attr+'-'+id+'"]').length != 0){
                $('[data-calc="'+attr+'-'+id+'"]').text(value);
            } else {
                $('#orderpart-'+attr+'-'+id).val(value);            
            }
        });
        $.each(response.depend, function(attr, value){
            if(attr == 'cut_thickness_metal'){
                var currentValue = $('#orderpart-'+attr+'-'+id).val();
                $('#orderpart-'+attr+'-'+id).html(value);            
                $('#orderpart-'+attr+'-'+id).val(currentValue);
            }
        });
        $.each(response.total, function(attr, value){
            $('[data-total="'+attr+'"]').text(value);
        });
        var uniquePartsPks = [];
        $.each(response.uniqueParts.data, function(i, part){
            var id = part.id;
            uniquePartsPks.push(id);
            console.log('[data-unique-row="'+id+'"]');
            if($('[data-unique-row="'+id+'"]').length > 0){
                response.uniqueParts.data[i].edited = true;
            }
            for (var attr in part){
                $('[data-unique="'+attr+'-'+id+'"]').text(part[attr]);
            }
        });
        $("[data-unique-row]").each(function(){ // Удаляем
            var mustDelete = true;
            var id = $(this).attr("data-unique-row");
            $.each(uniquePartsPks, function(i, value){
                if(value == id){
                    mustDelete = false;
                }
            });
            if(mustDelete){
                $(this).remove();
            }
        });
        $.each(response.uniqueParts.data, function(i, part){ // добавляем новые
            var id = part.id;
            if(part.edited !== true){
                var row = $('[data-unique-row]').first().clone();
                var rowId = row.attr('data-unique-row');
                for (var attr in part){
                    $(row).find('[data-unique="'+attr+'-'+rowId+'"]').text(part[attr]);
                }
                var rowHTML = row[0].outerHTML;
                // rowHTML.replace(new RegExp(rowId+'"', 'g'), id+'"');
                console.log(rowId+'"');
                console.log(id+'"');
                var output = rowHTML.split(rowId+'"').join(id+'"');
                $('[data-unique-row]').last().after(output);
            }
        });
        $.each(response.uniqueParts.total, function(attr, value){
            if(attr == 'delivery'){
                $('#order-price_delivery_metal').val(value.replace(/(<([^>]+)>)/gi, ""));
            }
            $('[data-unique-total="'+attr+'"]').html(value);
        });
    });
}

$('[name="OrderPart[material_length]"]').change(function(){
    var value = $(this).val();
    var material = $(this).parent().parent().find('[name="OrderPart[cut_material_id]"]').val();
    var thickness = $(this).parent().parent().find('[name="OrderPart[cut_thickness_metal]"]').val();
    var self = this;
    var selfId = $(self).attr('id').split('-')[2];
    $('[name="OrderPart[cut_material_id]"]').each(function(){
        var rowMaterial = $(this).parent().parent().find('[name="OrderPart[cut_material_id]"]').val();
        var rowThickness = $(this).parent().parent().find('[name="OrderPart[cut_thickness_metal]"]').val();
        if(material === rowMaterial && rowThickness === thickness){
            $(this).parent().parent().find('[name="OrderPart[material_length]"]').val(value);
            var id = $(this).attr('id').split('-')[2];
           if(selfId != id){
                change.call($(this).parent().parent().find('[name="OrderPart[material_length]"]'));
           }
        }
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPart */
?>
<div class="order-part-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'path_file:ntext',
            'count',
            'cut_price_without_material',
            'cut_price_for_one_detail',
            'cut_thickness_metal',
            'cut_discount:ntext',
            'cut_material_id',
            'cut_num',
            'cut_length_each',
            'cut_time',
            'cut_incut',
            'incut_price_per_meter',
            'incut_total_price',
            'welding_price',
            'bend_num',
            'detail_area',
            'digitization_price',
            'material_length',
            'material_width',
            'material_owner',
            'material_area',
            'material_area_sum',
            'material_ratio',
            'material_sheet_price',
            'material_sheet_size:ntext',
            'material_weight',
            'material_price',
            'total_price',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>

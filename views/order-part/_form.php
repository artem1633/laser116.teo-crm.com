<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPart */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-part-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'path_file')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'count')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cut_price_without_material')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cut_price_for_one_detail')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cut_thickness_metal')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cut_discount')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cut_material_id')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cut_num')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cut_length_each')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cut_time')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cut_incut')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'incut_price_per_meter')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'incut_total_price')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'welding_price')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'bend_num')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'detail_area')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'digitization_price')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'material_length')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'material_width')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'material_owner')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'material_area')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'material_area_sum')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'material_ratio')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'material_sheet_price')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'material_weight')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'material_price')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'total_price')->textInput() ?>
        </div>
    </div>
    

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

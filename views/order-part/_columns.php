<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'path_file',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cut_price_without_material',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cut_price_for_one_detail',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cut_thickness_metal',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cut_discount',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cut_material_id',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cut_num',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cut_length_each',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cut_time',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cut_incut',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'incut_price_per_meter',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'incut_total_price',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'welding_price',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'bend_num',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'detail_area',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'digitization_price',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_length',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_width',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_owner',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_area',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_area_sum',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_ratio',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_sheet_price',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_sheet_size',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_weight',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'material_price',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'total_price',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'updated_at',
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(['order-part/'.$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   
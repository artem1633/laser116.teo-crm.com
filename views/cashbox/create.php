<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cashbox */

?>
<div class="cashbox-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Materials;

/* @var $this yii\web\View */
/* @var $model app\models\BendPrice */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->materials = ArrayHelper::getColumn(\app\models\BendPriceMaterials::find()->where(['bend_price_id' => $model->id])->all(), 'materials_id');
}

?>

<div class="bend-price-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'materials')->widget(\kartik\select2\Select2::class, [
            'data' => ArrayHelper::map(Materials::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'multiple' => true,
            ],
    ]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BendPrice */
?>
<div class="bend-price-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'count',
            'price',
        ],
    ]) ?>

</div>

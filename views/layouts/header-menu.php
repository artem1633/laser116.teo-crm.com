<?php

use app\admintheme\widgets\TopMenu;


$dataSpecial = [
    \app\models\User::ROLE_CUTTING,
    \app\models\User::ROLE_BEND,
    \app\models\User::ROLE_PAINTING,
    \app\models\User::ROLE_WELDING,
];




?>

<div id="top-menu" class="top-menu" style="margin-top: -18px;">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        /** @var \app\models\User $identity */
        $identity = Yii::$app->user->identity;
        try {
            echo TopMenu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        //                    ['label' => 'Контракты', 'icon' => 'fa  fa-th', 'url' => ['/contract'],],
                        //                    ['label' => 'Кандидаты', 'icon' => 'fa  fa-users', 'url' => ['/candidate'],],

                        //                    ['label' => 'Проекты', 'icon' => 'fa fa-briefcase', 'url' => ['/contract'],],
                        //                    ['label' => 'Спринты', 'icon' => 'fa  fa-flag', 'url' => ['/order'],],
                        ['label' => 'Рабочий стол', 'icon' => 'fa fa-bar-chart', 'url' => ['/dashboard'], 'visible' => Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN || Yii::$app->user->identity->role == \app\models\User::ROLE_MANAGER],
                        ['label' => 'Заказы', 'icon' => 'fa fa-shopping-cart', 'url' => ['/order'],],
                        ['label' => 'Задачи', 'icon' => 'fa fa-sitemap', 'url' => ['/task'],],
                        ['label' => 'Касса', 'icon' => 'fa fa-area-chart', 'url' => ['/payments'], 'visible' => !in_array(Yii::$app->user->identity->role, $dataSpecial),],
                        ['label' => 'Остатки', 'icon' => 'fa fa-area-chart', 'url' => ['/available'], 'visible' => !in_array(Yii::$app->user->identity->role, $dataSpecial),],
                        ['label' => 'Контрагенты', 'icon' => 'fa fa-users', 'url' => ['/customer'], 'visible' => !in_array(Yii::$app->user->identity->role, $dataSpecial),],

                        //
                        [
                            'label' => 'Справочники',
                            'icon' => 'fa fa-book',
                            'visible' => !in_array(Yii::$app->user->identity->role, $dataSpecial),
                            'url' => '/user',
                            'options' => ['class' => 'has-sub'],
//                            'visible' => $identity->isSuperAdmin(),
                            'items' => [
                                [
                                    'label' => 'Пользователи',
                                    'icon' => 'fa  fa-user-o',
                                    'url' => ['/user'],
                                    'visible' => $identity->isSuperAdmin()
                                ],
                                ['label' => 'Материалы', 'icon' => 'fa  fa-user-o', 'url' => ['/materials']],
                                //                            ['label' => 'Резка', 'icon' => 'fa  fa-user-o', 'url' => ['/cutting']],
                                ['label' => 'Цена гибки', 'icon' => 'fa  fa-user-o', 'url' => ['/bend-price']],
                                ['label' => 'Цена покраски', 'icon' => 'fa  fa-user-o', 'url' => ['/bend-price-paint']],
                                ['label' => 'Шаблоны смс', 'icon' => 'fa  fa-user-o', 'url' => ['/pattern-sms']],
                                ['label' => 'Шаблоны email', 'icon' => 'fa  fa-user-o', 'url' => ['/pattern-email']],
                                [
                                    'label' => 'Шаблоны Документов',
                                    'icon' => 'fa  fa-user-o',
                                    'url' => ['/pattern-document']
                                ],
//                                ['label' => 'Контрагенты', 'icon' => 'fa  fa-user-o', 'url' => ['/customer']],
                                [
                                    'label' => 'Юридические лица',
                                    'icon' => 'fa  fa-user-o',
                                    'url' => ['/legal-entities']
                                ],
                                ['label' => 'Поставщики', 'icon' => 'fa  fa-user-o', 'url' => ['/provider']],
                                ['label' => 'Кассы', 'icon' => 'fa  fa-user-o', 'url' => ['/cashbox']],
                                ['label' => 'Настройки', 'icon' => 'fa  fa-user-o', 'url' => ['/settings']],
                                [
                                    'label' => 'Статусы',
                                    'url' => '#',
                                    'options' => ['class' => 'has-sub'],
                                    'items' => [
                                        [
                                            'label' => 'Статус гибки',
                                            'icon' => 'fa  fa-user-o',
                                            'url' => ['/bend-status']
                                        ],
                                        [
                                            'label' => 'Статус резки',
                                            'icon' => 'fa  fa-user-o',
                                            'url' => ['/cutting-status']
                                        ],
                                        [
                                            'label' => 'Статус покраски',
                                            'icon' => 'fa  fa-user-o',
                                            'url' => ['/paint-status']
                                        ],
                                        [
                                            'label' => 'Статус сварки',
                                            'icon' => 'fa  fa-user-o',
                                            'url' => ['/welding-status']
                                        ],
                                        [
                                            'label' => 'Статус заказа',
                                            'icon' => 'fa  fa-user-o',
                                            'url' => ['/order-status']
                                        ],
                                        [
                                            'label' => 'Статус задач',
                                            'icon' => 'fa  fa-user-o',
                                            'url' => ['/task-status']
                                        ],
                                    ]
                                ],
                            ]
                        ],

                    ],

                ]
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>

<?php

use yii\helpers\Html;
use app\models\Users;
use app\models\Task;
use app\models\MaterialParams;
use app\models\Available;
use yii\helpers\Url;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                <span class="navbar-logo"></span>
                LASER 116
            </a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <?php if(Yii::$app->user->isGuest == false): ?>
            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown navbar-user">
                    <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/<?= Yii::$app->user->identity->realAvatar ?>" style="object-fit: cover;" data-role="avatar-view" alt="">
                        <span class="hidden-xs"><?=Yii::$app->user->identity->login?></span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft">
                        <li class="arrow"></li>
                        <li> <?= Html::a('Настройки пользователи', ['user/profile']) ?> </li>
                        <li class="divider"></li>
                        <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <?php

                $newTasks = Task::find()->andWhere(['executor_id' => Yii::$app->user->getId()])->joinWith(['taskStatus'])->andWhere(['task_status.is_new' => true])->all();
                $closeTasks = [];
                //                        $closeTasks = Task::find()->andWhere(['executor_id' => Yii::$app->user->getId()])->joinWith(['taskStatus'])->where(['task_status.is_close' => true])->all();

                $materialParams = MaterialParams::find()->all();
                $availables = Available::find()->all();

                $availableRemainders = [];

                foreach ($materialParams as $materialParam)
                {
                    foreach ($availables as $available)
                    {
                        if($available->category_id == $materialParam->id){
                            if($materialParam->remainder > $available->count){
                                $availableRemainders[] = $materialParam;
                            }
                        }
                    }
                }

                ?>

                <li class="navbar-user" style="position: relative;">
                    <?= Html::a('<i class="fa fa-bell" style="font-size: 20px;"></i>', '#', ['title' => 'Уведомления', 'style' => 'cursor: pointer;', 'onclick' => "event.preventDefault(); $('.header-messages').toggle();"]) ?>
                    <?php if(count($newTasks)): ?>
                        <span style="position: absolute; display: inline-block; background: #cc2b1d; color: #fff; width: 18px; height: 18px; text-align: center; border-radius: 100%; top: 21px;"><?= (count($newTasks)) ?></span>
                    <?php endif; ?>
                </li>
                <li class="dropdown navbar-user">
                    <div class="header-messages" style="display: none;">
                        <?php foreach ($newTasks as $task): ?>
                            <a href="<?= Url::toRoute(['task/view', 'id' => $task->id]) ?>" class="header-message">
                                <div class="header-message-title">Новая задача</div>
                                <p>Появилась новая задача №<?=$task->id?></p>
                            </a>
                        <?php endforeach; ?>
                        <?php foreach ($closeTasks as $task): ?>
                            <a href="<?= Url::toRoute(['task/view', 'id' => $task->id]) ?>" class="header-message">
                                <div class="header-message-title">Задача завершена</div>
                                <p>Завершилась задача №<?=$task->id?></p>
                            </a>
                        <?php endforeach; ?>
                        <?php if(Yii::$app->user->identity->role == \app\models\User::ROLE_ADMIN || Yii::$app->user->identity->role == \app\models\User::ROLE_DIRECTOR): ?>
                            <?php foreach ($availableRemainders as $availableRemainder): ?>
                                <a class="header-message">
                                    <div class="header-message-title">Остаток</div>
                                    <p>Остаток менее минимальной отметки <?=$availableRemainder->materials->name?> с толщиной <?=$availableRemainder->thickness?></p>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>


                    </div>
                </li>
            </ul>
            <!-- end header navigation right -->
        <?php endif; ?>
    </div>
    <!-- end container-fluid -->
</div>
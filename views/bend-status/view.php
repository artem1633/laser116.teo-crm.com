<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BendStatus */
?>
<div class="bend-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
        ],
    ]) ?>

</div>

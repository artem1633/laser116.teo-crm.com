<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BendStatus */
?>
<div class="bend-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

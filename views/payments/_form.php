<?php

use app\models\Cashbox;
use app\models\Customer;
use app\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $model->created_at = date('Y-m-d H:i');
}

?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'created_at')->widget(\kartik\datetime\DateTimePicker::class, [

    ]) ?>

    <?= $form->field($model, 'customer_id')->widget(\kartik\select2\Select2::class, [
        'data' => ArrayHelper::map(Customer::find()->all(), 'id', 'name'),
        'pluginEvents' => [
            'change' => 'function(){
                var value = $(this).val();
                
                if(value){
                    $.get("/order/get-orders-by-customer?customerId="+value, function(response){
                        $("#payments-order_id").html(response);
                        $("#payments-order_id").trigger("click");
                    });
                }
            }',
        ],
    ]) ?>

    <?= $form->field($model, 'order_id')->widget(\kartik\select2\Select2::class, [
        'data' => $model->customer_id ? ArrayHelper::map(Order::find()->where(['customer_id' => $model->customer_id])->all(), 'id', 'name') :
            [],
    ]) ?>

    <?= $form->field($model, 'cashbox_id')->dropDownList(ArrayHelper::map(Cashbox::find()->orderBy('name asc')->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([
            1 => 'Поступление',
            2 => 'Списание'
    ]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

use app\models\Cashbox;
use app\models\User;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginEvents' => [
                'cancel.daterangepicker'=>'function(ev, picker) {$("#ordersearch-order_date").val(""); $("#ordersearch-order_date").trigger("change"); }'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'convertFormat'=>true,
                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'YYYY-MM-DD'
                ]
            ],
        ],
        'format' => ['date', 'php:d.m.Y'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_id',
        'value' => function($model){
            $order = \app\models\Order::findOne($model->order_id);
            if($order){
                return $order->name;
            }
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cashbox_id',
        'content' => function($data){
            $status = ArrayHelper::getColumn(Cashbox::find()->where(['id' => $data->cashbox_id])->all(), 'name');
            return implode('',$status);

        },
        'filter' => ArrayHelper::map(Cashbox::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true, 'multiple' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer_id',
        'value' => 'customer.name'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' =>function($data){
            if ($data->type == '1') {
                return 'Поступление';
            }
            if ($data->type == '2') {
                return 'Списание';
            }
            return 'не заданно';
        },

        'filter'=>array(
            '1' => 'Поступление',
            '2' => 'Списание'
        ),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_by',
         'content' => function($data){
             $status = ArrayHelper::getColumn(User::find()->where(['id' => $data->created_by])->all(), 'name');
             return implode('',$status);

         },
         'filter' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'name'),
         'filterType' => GridView::FILTER_SELECT2,
         'filterWidgetOptions' => [
             'options' => ['prompt' => ''],
             'pluginOptions' => ['allowClear' => true, 'multiple' => true],
     ],
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Удаление',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];   
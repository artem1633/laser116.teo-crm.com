<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
?>
<div class="payments-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'name',
            'sum',
            'type',
            'comment:ntext',
            'created_at',
            'created_by',
        ],
    ]) ?>

</div>

<?php

use app\models\Customer;
use app\models\User;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'official_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'official_type')->dropDownList(Customer::officialTypeLabels()) ?>
        </div>
    </div>
    <div class="row">
        <div class="<?=($model->official_type==Customer::OFFICIAL_TYPE_PERSON ? ' col-md-6' : 'col-md-3')?>" data-type="all">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="<?=($model->official_type==Customer::OFFICIAL_TYPE_PERSON ? ' col-md-6' : 'col-md-3')?>" data-type="all">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3<?=($model->official_type==Customer::OFFICIAL_TYPE_PERSON ? ' hidden' : '')?>" data-type="company">
            <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3<?=($model->official_type==Customer::OFFICIAL_TYPE_PERSON ? ' hidden' : '')?>" data-type="company">
            <?= $form->field($model, 'post_address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row<?=($model->official_type==Customer::OFFICIAL_TYPE_PERSON ? ' hidden' : '')?>" data-type="company">
        <div class="col-md-3">
            <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'r_s')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'k_s')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'bic')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'manager_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(User::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
    </div>


    <?= $form->field($model, 'contacts')->widget(MultipleInput::className(), [

        'id' => 'my_id',
        'min' => 0,
        'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden'
                ]
            ],
            [
                'name' => 'fio',
                'enableError' => true,
                'title' => 'ФИО',
            ],
            [
                'name' => 'position',
                'title' => 'Должность',
            ],
            [
                'name' => 'mobile',
                'title' => 'Мобильный телефон',
            ],
            [
                'name' => 'phone',
                'title' => 'Служебный телефон',
            ],
            [
                'name' => 'email',
                'title' => 'email',
            ],

        ],
    ]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS
    $('#customer-official_type').change(function(){
        if($(this).val() == 0){
            $('[data-type="all"]').removeClass('col-md-3');
            $('[data-type="all"]').addClass('col-md-6');
            $('[data-type="company"]').addClass('hidden');
        } else if($(this).val() == 1){
            $('[data-type="all"]').removeClass('col-md-6');
            $('[data-type="all"]').addClass('col-md-3');
            $('[data-type="company"]').removeClass('hidden');
        }
    });
JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>
<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
?>
<div class="customer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'address',
            'director',
            'post_address',
            'inn',
            'kpp',
            'r_s',
            'k_s',
            'bic',
        ],
    ]) ?>

</div>

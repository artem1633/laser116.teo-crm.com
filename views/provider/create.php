<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Provider */

?>
<div class="provider-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Provider */
?>
<div class="provider-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

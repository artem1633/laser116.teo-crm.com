<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Provider */
?>
<div class="provider-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'email:email',
            'director',
        ],
    ]) ?>

</div>

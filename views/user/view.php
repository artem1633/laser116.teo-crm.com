<?php


use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

?>
<div class="user-view">

    <div class="panel panel-inverse">
        <?php Pjax::begin(['id' => 'pjax-container-info-container', 'enablePushState' => false]); ?>
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
            <div class="panel-heading-btn" style="margin-top: -20px;">
                <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['user/update', 'id' => $model->id, 'containerPjaxReload' => '#pjax-user-info-container'], ['role' => 'modal-remote']) ?>
            </div>
        </div>
        <div class="panel-body">
 

            <div class="row">
                <div class="col-md-2">
                    <img src="<?=Url::toRoute(['/'.$model->realAvatar])?>" style="width: 100%; height: 100%; object-fit: cover;">
                </div>
                <div class="col-md-10">
                    <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'login:email',
                        'birth_date',
                        'phone',
                        'info'
                    ],
                    ]) ?>
                </div>



            </div>



        </div>




    </div>

</div>
<?php Pjax::end(); ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php

use app\models\City;
use app\models\Role;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],


    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($model){
            return Html::a($model->name, ['view', 'id' => $model->id], ['data-pjax' => '0']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'birth_date',
     ],

     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'info',
     ],
    [
        'attribute' => 'color',
        'value' => function ($model, $key, $index, $widget) {
            return "<span class='badge'  style='background-color: {$model->color}'> </span>  ";
        },
        'width' => '',
        'filterType' => GridView::FILTER_COLOR,
        'filterWidgetOptions' => [
            'showDefaultPalette' => true,
        ],
        'vAlign' => 'middle',
        'format' => 'raw',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'role',
         'content' =>function($data){

             if ($data->role == '1') {
                 return 'Администратор CRM';
             }
             if ($data->role == '2') {
                 return 'Менеджер';
             }
             if ($data->role == '3') {
                 return 'Резка';
             }
             if ($data->role == '4') {
                 return 'Гибка';
             }
             if ($data->role == '5') {
                 return 'Покраска';
             }
             if ($data->role == '6') {
                 return 'Сварка';
             }
             if ($data->role == '7') {
                 return 'Начальник цеха';
             }
             return 'не заданно';
         },

         'filter'=>array(
             '1' => 'Администратор CRM',
             '2' =>  'Менеджер',
             '3' => 'Резка',
             '4' => 'Гибка',
             '5' => 'Покраска',
             '6' => 'Сварка',
             '7' => 'Начальник цеха'
         ),

     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'is_deletable',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{view}{update}{delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },

//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];
<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Materials */
?>
<div class="materials-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'desc:ntext',
            'name',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Materials */
?>
<div class="materials-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Materials */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materials-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'density')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'desc')->textarea(['rows' => 4]) ?>
    <?= $form->field($model, 'additional_coefficient')->checkbox() ?>
    <?= $form->field($model, 'params')->widget(MultipleInput::className(), [

        'id' => 'my_id',
        'min' => 0,
        'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden'
                ]
            ],
            [
                'name' => 'thickness',
                'title' => 'Толщина',
            ],
            [
                'name' => 'width',
                'title' => 'Ширина',
            ],
            [
                'name' => 'length',
                'title' => 'Длина',
            ],
            [
                'name' => 'remainder',
                'title' => 'Минимальный остаток',
            ],
            [
                'name' => 'price',
                'enableError' => true,
                'title' => 'Цена',
            ],
            [
                'name' => 'inset_price',
                'title' => 'Цена Врезки',
            ],
            [
                'name' => 'cut_price',
                'title' => 'Цена Резки',
            ],
            [
                'name' => 'inset_time',
                'title' => 'Время Врезки',
            ],
            [
                'name' => 'cut_speed',
                'title' => 'Скорость Реза',
            ],

        ],
    ]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

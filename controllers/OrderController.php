<?php

namespace app\controllers;

use app\components\helpers\SmsSender;
use app\components\OrderPartFieldCalc;
use app\components\TagHelper;
use app\models\BendPrice;
use app\models\BendPriceMaterials;
use app\models\CommercialOfferSearch;
use app\models\Customer;
use app\models\CustomerContact;
use app\models\forms\DocumentForm;
use app\models\OrderPart;
use app\models\OrderPartSearch;
use app\models\OrderStatus;
use app\models\PatternSms;
use app\models\PaymentsSearch;
use app\models\ScoreSearch;
use app\models\TaskSearch;
use app\models\User;
use Mpdf\Mpdf;
use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->joinWith(['status', 'bendStatus', 'cuttingStatus', 'paintStatus', 'weldingStatus']);

        if(Yii::$app->user->identity->role == User::ROLE_CUTTING){
            $dataProvider->query->andWhere(['order_status.in_work' => true]);
            $dataProvider->query->andWhere(['cutting_status.in_work' => true]);
        }

        if(Yii::$app->user->identity->role == User::ROLE_BEND){
            $dataProvider->query->andWhere(['order_status.in_work' => true]);
            $dataProvider->query->andWhere(['bend_status.in_work' => true]);
        }

        if(Yii::$app->user->identity->role == User::ROLE_PAINTING){
            $dataProvider->query->andWhere(['order_status.in_work' => true]);
            $dataProvider->query->andWhere(['paint_status.in_work' => true]);
        }

        if(Yii::$app->user->identity->role == User::ROLE_WELDING){
            $dataProvider->query->andWhere(['order_status.in_work' => true]);
            $dataProvider->query->andWhere(['welding_status.in_work' => true]);
        }


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetWord($id)
    {
        $model = \app\models\Order::findOne($id);
        $parts = \app\models\OrderPart::find()->where(['order_id' => $id])->all();


        // Creating the new document...
        $phpWord = new \PhpOffice\PhpWord\PhpWord();


        $cellHCentered = ['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER];


        $section = $phpWord->addSection();

        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell(2000, $cellHCentered);

        $cell->addText('Р.Ф. 420095, г. Казань, ул. В. Кулагина, д.9.', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('ИНН 1659217014', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('КПП 165901001', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('+7 (937) 600 30 30', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('+7 (843) 245 41 41', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('zakaz@tatlazer.ru', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);


        $cell = $table->addCell(5000, $cellHCentered);
        $cell->addImage(\yii\helpers\Url::toRoute('/logo.jpg', true), ['height' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
        $cell->addText('', ['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
        $cell->addText('         ТАТЛАЗЕР', ['bold' => true, 'size' => 24, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
        $cell->addText('        ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ', ['size' => 8, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);

        $cell = $table->addCell(2000, $cellHCentered);

        $cell->addText('RUSSIA 420095, Kazan city, str. V. Kulagina 9', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('TIN 1659217014', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('CAT 165901001', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('+7 (937) 600 30 30', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('+7 (843) 245 41 41', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('zakaz@tatlazer.ru', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);


        $section->addText('');
        $section->addText('');
        $section->addText('Исполнитель: ООО «ТАТЛАЗЕР»', ['bold' => true]);
        $section->addText('Заказчик: ООО «Заказчик»', ['bold' => true]);




        $rows = 10;
        $cols = 6;

        $tableStyle = [
            'borderColor' => '000000',
            'borderSize'  => 6,
            'cellMargin'  => 50,
            'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
            'valign' => 'center'
        ];


        $headersLabels = [
            1 => [
                'label' => '№',
                'width' => 700,
            ],
            2 => [
                'label' => 'Наименование',
                'width' => 3500,
            ],
            3 => [
                'label' => 'Ед.Изм.',
                'width' => 1000,
            ],
            4 => [
                'label' => 'Кол-во',
                'width' => 1000,
            ],
            5 => [
                'label' => 'Цена с НДС',
                'width' => 1300,
            ],
            6 => [
                'label' => 'Сумма с НДС',
                'width' => 1500,
            ],
        ];

        $table = $section->addTable($tableStyle);
        for ($r = 1; $r <= (count($parts)+1); $r++) {
            $table->addRow();

            if($r == 1){
                $part = null;
            } else {
                $part = $parts[($r - 2)];
            }

            for ($c = 1; $c <= $cols; $c++) {

                if($r == 1){
                    $table->addCell($headersLabels[$c]['width'])->addText($headersLabels[$c]['label']);
                } else {
                    // $table->addCell($headersLabels[$c]['width'])->addText("Row {$r}, Cell {$c}");


                    if($c == 1){
                        $table->addCell($headersLabels[$c]['width'])->addText(($r - 1));
                    } elseif($c == 2){
                        $table->addCell($headersLabels[$c]['width'])->addText("Деталь {$part->path_file}");
                    } elseif($c == 3){
                        $table->addCell($headersLabels[$c]['width'])->addText("шт");
                    } elseif($c == 4){
                        $table->addCell($headersLabels[$c]['width'])->addText($part->cut_num);
                    } elseif($c == 5){
                        $table->addCell($headersLabels[$c]['width'])->addText(Yii::$app->formatter->asDecimal($part->material_sheet_price).' ₽');
                    } elseif($c == 6){
                        $table->addCell($headersLabels[$c]['width'])->addText(Yii::$app->formatter->asDecimal(0).' ₽');
                    }
                }
            }
        }
        

        $table->addRow();

        $table->addCell(750, ['gridSpan' => 5])->addTextRun($cellHCentered)->addText('ИТОГО:');

        $table->addCell(750, ['gridSpan' => 1])->addText(Yii::$app->formatter->asDecimal(0).' ₽');


        $phpWord->addNumberingStyle(
            'multilevel',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                )
            )
        );

        $section->addListItem('Общая стоимость Изделий настоящей Спецификации составляет ___ (___) руб. __ копеек, в том числе НДС 20% ___ (____) руб. ___ копеек.', 0, null, 'multilevel');
        $section->addListItem('Изделия изготавливаются из материала Исполнителя, на основании предоставленных чертежей Заказчик.', 0, null, 'multilevel');
        $section->addListItem('Заказчик производит предоплату 100% в сумме ______ (___) рублей __ копеек путем перечисления денежных средств на расчетный счет Исполнителя.', 0, null, 'multilevel');
        $section->addListItem('Изготовление и поставка Изделий составляет __ (___) рабочих дней со дня поступления платежа на расчетный счет Исполнителя.', 0, null, 'multilevel');
        $section->addListItem('Базис поставки: Заказчик обязуется вывезти Изделия по адресу: РТ, г. Казань, ул. В. Кулагина д.9. ', 0, null, 'multilevel');
        $section->addListItem('Исполнитель уведомляет Заказчика к отгрузке Изделий путем направления в его адрес уведомления.', 0, null, 'multilevel');



        



        $section->addText('');
        $section->addText('');
        $section->addText('');
        $section->addText('');

        $section->addText('Надеемся на долгосрочное сотрудничество.', ['size' => 13, 'name' => 'Calibri', '']);
        $section->addText("С уважением", ['size' => 13, 'name' => 'Calibri', '']);
        $section->addText("директор ООО “ТАТЛАЗЕР”                           М.П.                       Юрков Э.К.", ['size' => 13, 'name' => 'Calibri', '']);




        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('helloWorld.docx');

        Yii::$app->response->sendFile('helloWorld.docx', 'кп.docx');
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $partSearchModel = new OrderPartSearch();
        $partDataProvider = $partSearchModel->search(Yii::$app->request->queryParams);
        $partDataProvider->query->andWhere(['order_id' => $id]);

        $taskSearchModel = new TaskSearch();
        $taskDataProvider = $taskSearchModel->search(Yii::$app->request->queryParams);
        $taskDataProvider->query->andWhere(['order_id' => $id]);
        $taskDataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        $paymentsSearchModel = new PaymentsSearch();
        $paymentsDataProvider = $paymentsSearchModel->search(Yii::$app->request->queryParams);
        $paymentsDataProvider->query->andWhere(['order_id' => $id]);
        $paymentsDataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        $scoreSearchModel = new ScoreSearch();
        $scoreDataProvider = $scoreSearchModel->search(Yii::$app->request->queryParams);
        $scoreDataProvider->query->andWhere(['order_id' => $id]);
        $scoreDataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        $offerSearchModel = new CommercialOfferSearch();
        $offerDataProvider = $offerSearchModel->search(Yii::$app->request->queryParams);
        $offerDataProvider->sort->defaultOrder = ['id' => SORT_DESC];


        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Заказ #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $model,
                    'partSearchModel' => $partSearchModel,
                    'partDataProvider' => $partDataProvider,
                    'taskSearchModel' => $taskSearchModel,
                    'taskDataProvider' => $taskDataProvider,
                    'paymentsSearchModel' => $paymentsSearchModel,
                    'paymentsDataProvider' => $paymentsDataProvider,
                    'scoreSearchModel' => $scoreSearchModel,
                    'scoreDataProvider' => $scoreDataProvider,
                    'offerSearchModel' => $offerSearchModel,
                    'offerDataProvider' => $offerDataProvider,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $model,
                'partSearchModel' => $partSearchModel,
                'partDataProvider' => $partDataProvider,
                'taskSearchModel' => $taskSearchModel,
                'taskDataProvider' => $taskDataProvider,
                'paymentsSearchModel' => $paymentsSearchModel,
                'paymentsDataProvider' => $paymentsDataProvider,
                'scoreSearchModel' => $scoreSearchModel,
                'scoreDataProvider' => $scoreDataProvider,
                'offerSearchModel' => $offerSearchModel,
                'offerDataProvider' => $offerDataProvider,
            ]);
        }
    }

    /**
     * Creates a new Order model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Order();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Order",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                foreach (explode("\n", $model->files) as $file){
                    (new OrderPart([
                        'path_file' => $file,
                        'order_id' => $model->id,
                        'mine' => 1,
                    ]))->save(false);
                }

                return $this->redirect(['order/view', 'id' => $model->id]);

//                return [
//                    'forceReload'=>'#crud-datatable-pjax',
//                    'title'=> "Create new Order",
//                    'content'=>'<span class="text-success">Create Order success</span>',
//                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                        Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
//
//                ];
            }else{
                return [
                    'title'=> "Create new Order",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionCustomers($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($q){
            $data = ArrayHelper::map(Customer::find()->where(['like', 'name', $q])->all(), 'id', 'name');

            $output = [];

            foreach ($data as $id => $name)
            {
                $output[] = ['id' => $id, 'text' => $name];
            }

            return ['results' => $output];
        } else {
            return [];
        }
    }

    /**
     * @param int $customerId
     * @return string
     */
    public function actionGetOrdersByCustomer($customerId)
    {
        $output = "";
        $orders = Order::find()->where(['customer_id' => $customerId])->all();

        foreach ($orders as $order)
        {
            $output .= "<option value='".$order->id."'>".$order->name."</option>";
        }

        return $output;
    }

    /**
     * Updates an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Order #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                foreach (explode("\n", $model->files) as $file){
                    (new OrderPart([
                        'path_file' => $file,
                        'order_id' => $model->id,
                    ]))->save(false);
                }
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }else{
                return [
                    'title'=> "Update Order #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdateCount($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load($request->post()) && $model->save(false)) {

            foreach (OrderPart::find()->where(['order_id' => $id])->all() as $part){
                $calculator = new OrderPartFieldCalc([
                    'model' => $part
                ]);
                $calculator->calc(false);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionDocument($id)
    {
        $request = Yii::$app->request;
        $model = new DocumentForm();
        $order = $this->findModel($id);

        if($model->load($request->post()) && $model->generate()) {

            $content = TagHelper::handleModel($model->content, $order);

            if($model->type == DocumentForm::TYPE_PRINT){
                $session = \Yii::$app->session;
                $session->set('doc-content', $content);
                return $this->redirect(['order/document-print']);
            } else if($model->type == DocumentForm::TYPE_MAIL){
                Yii::$app->response->format = Response::FORMAT_JSON;

                $mpdf = new Mpdf();
                $mpdf->WriteHTML($content);
                $mpdf->Output('mail.pdf', \Mpdf\Output\Destination::FILE);

                try {
                    $customerContract = CustomerContact::findOne($order->customer_contact_id);
                    \Yii::warning($customerContract);
                    if($customerContract){
                        $sent = Yii::$app->mailer->compose()
                            ->setSubject("Документ | {$model->name}")
                            ->setHtmlBody("Документ")
                            ->setFrom('laser.notify@mail.ru')
                            ->setTo($customerContract->email)
                            ->attach('mail.pdf')
                            ->send();
                    } else {
                        $sent = false;
                    }
                } catch (\Exception $e){
                    \Yii::warning($e, 'Mail sending error');
                    $sent = false;
                }

                return [
                    'title'=> "Формирование документа",
                    'content'=> ($sent ? '<span class="text-success">Письмо успешно отправлено</span>' : '<span class="text-danger">При отправке письма произошла ошибка</span>'),
                    'footer'=> Html::button('Готово',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"]),
                ];
            }

        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Формирование документа",
                'content'=>$this->renderAjax('document', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Печать',['id' => 'submit-btn', 'class'=>'btn btn-primary','type'=>"submit", 'onclick' => '
                        $("#documentform-type").val("0");
                    ']).
                    Html::button('Отправить на почту',['class'=>'btn btn-warning', 'onclick' => '
                        $("#documentform-type").val("1");
                        $("#submit-btn").trigger("click");
                    '])
            ];
        }
    }

    public function actionDocumentPrint()
    {
        if(\Yii::$app->session->has('doc-content')){
            $content = \Yii::$app->session->get('doc-content');
            \Yii::$app->session->remove('doc-content');
            return $this->renderPartial('document-print', [
                'content' => $content,
            ]);
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function actionUpdateAjax($id)
    {
        $model = $this->findModel($id);
        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $model->save()];
        }
    }

    public function actionEditableChange($id, $output)
    {
        $model = $this->findModel($id);
        $oldStatus = $model->status_id;

        // Check if there is an Editable ajax request
        if (isset($_POST['hasEditable'])) {
            // use Yii's response format to encode output as JSON
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


//            $output = '';
            foreach ($_POST as $name => $value){
                if($name != 'hasEditable' && $name != '_csrf'){
                    $model->$name = $value;

                    // Статус изменен. Отправка SMS
                    if($name == 'status_id' && $oldStatus != $value){
                        \Yii::warning('in status');
//                        $editStatus = true;
                        $orderStatus = OrderStatus::findOne($value);
                        if($orderStatus){
                            \Yii::warning('orderStatus');
                            if($orderStatus->pattern_sms_id){
                                \Yii::warning('in pattern_sms_id');
                                $smsTemplate = PatternSms::findOne($orderStatus->pattern_sms_id);
                                if($smsTemplate){
                                    \Yii::warning('sms template');
                                    $content = TagHelper::handleModel($smsTemplate->text, $model);
                                    $phone = null;
                                    $contact = CustomerContact::findOne($model->customer_contact_id);
                                    if($contact){
                                        \Yii::warning('contact');
                                        $phone = $contact->mobile;
                                        if($phone){
                                            \Yii::warning('phone');
                                            SmsSender::send($phone, $content);
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }


            // read your posted model attributes
            if ($model->save(false)) {

                foreach (OrderPart::find()->where(['order_id' => $model->id])->all() as $part){
                    $calculator = new OrderPartFieldCalc([
                        'model' => $part
                    ]);
                    $calculator->calc(false);
                }

                // return JSON encoded output in the below format
                return ['output'=>ArrayHelper::getValue($model, $output), 'message'=>''];

                // alternatively you can return a validation error
                // return ['output'=>'', 'message'=>'Validation error'];
            }
            // else if nothing to do always return an empty JSON encoded output
            else {
                return ['output'=>'', 'message'=>''];
            }
        }

    }

//    public function actionTest()
//    {
//        $model = Order::find()->one();
//        $content = TagHelper::handleModel('{order.id}', $model);
//
//        echo $content;
//    }

    /**
     * @param string $id
     * @param string $attribute
     * @param string $value
     * @return mixed
     */
    public function actionChangePartAjax($id, $attribute, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = OrderPart::findOne($id);

        $attrArr = explode('[', $attribute);

        if(count($attrArr) == 2){
            $realAttrName = $attrArr[0];

            $userId = substr($attrArr[1], 0, strlen($attrArr[1])-1);

            $userData = json_decode($model->$realAttrName, true);

            $userData[$userId] = $value;

            $model->$realAttrName = json_encode($userData);
        } else {
            $model->$attribute = $value;
            \Yii::warning($model->$attribute, 'attr s');
        }



//        $model->save(false);
        $calculator = new OrderPartFieldCalc([
            'model' => $model
        ]);
        $result = $calculator->calc();
//        $model->attributes = $result['data'];
//        $model->save();

        $order = Order::findOne($model->order_id);

        $totalCutTime = 0;

        foreach (OrderPart::find()->where(['order_id' => $model->order_id])->all() as $part)
        {
            $totalCutTime = $totalCutTime + $part->cut_time;
        }


        if($attribute == 'cut_material_id'){
            $bendPrice = BendPriceMaterials::find()->where(['materials_id' => $value])->one();
            if($bendPrice){
                $bendPrice = BendPrice::findOne($bendPrice->bend_price_id);
                if($bendPrice){
                    $order->price_bend = $bendPrice->price;
                    $order->save(false);
                }
            }
        }

        if($order){
            $timesCount = round($totalCutTime / 8);
//            cutting_discount

            if($timesCount == 1){
                $order->cutting_discount = 1;
            } else if($timesCount == 2){
                $order->cutting_discount = 10;
            } else if($timesCount == 3 || $timesCount == 4){
                $order->cutting_discount = 20;
            } else if($timesCount == 5 || $timesCount == 6){
                $order->cutting_discount = 25;
            } else if($timesCount == 7 || $timesCount == 8 || $timesCount == 9 || $timesCount == 10){
                $order->cutting_discount = 30;
            } else if($timesCount > 10){
                $order->cutting_discount = 35;
            }
            $order->save();
        }

        return $result;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionCopy($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $newModel = new Order();
        $newModel->attributes = $model->attributes;
        if($newModel->save(false)){
            $modelParts = OrderPart::find()->where(['order_id' => $model->id])->all();
            foreach ($modelParts as $modelPart){
                $newModelPart = new OrderPart();
                $newModelPart->attributes = $modelPart->attributes;
                $newModelPart->order_id = $newModel->id;
                $newModelPart->save(false);
            }
        }

        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }

    /**
     * Delete an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

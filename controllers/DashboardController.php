<?php

namespace app\controllers;

use app\components\helpers\DateHelper;
use app\models\Cashbox;
use app\models\CuttingParams;
use app\models\Order;
use app\models\OrderPartSearch;
use app\models\Payments;
use app\models\Score;
use app\models\User;
use Yii;
use app\models\Cutting;
use app\models\CuttingSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * DashboardController implements the CRUD actions for Cutting model.
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cutting models.
     * @return mixed
     */
    public function actionIndex($period = null)
    {
        $dateStart = null;
        $dateEnd = null;


//        if($period == 'today') {
//            $startDate = date('Y-m-d 00:00:00');
//            $endDate = date('Y-m-d 23:59:59');
//        } elseif($period == 'yesterday') {
//            $startDate = date('Y-m-d 00:00:00', strtotime('- 1 day'));
//            $endDate = date('Y-m-d 23:59:59', strtotime('- 1 day'));
//        } elseif($period == 'week') {
//            $day = date('w');
//            $startDate = date('Y-m-d 00:00:00', strtotime('-'.$day.' days'));
//            $endDate = date('Y-m-d 23:59:59', strtotime('+'.(6-$day).' days'));
//        } elseif($period == 'last_week') {
//            $startDate = date('Y-m-d 00:00:00');
//            $endDate = date('Y-m-d 23:59:59');
//        } elseif($period == 'month') {
//            $startDate = date('Y-m-d 00:00:00');
//            $endDate = date('Y-m-d 23:59:59');
//        } elseif($period == 'last_month') {
//            $startDate = date('Y-m-d 00:00:00');
//            $endDate = date('Y-m-d 23:59:59');
//        }

        if($period == 'today'){
            $dateStart = date('Y-m-d 00:00:01');
            $dateEnd = date('Y-m-d 23:59:59');
        }else if($period == 'yesterday'){
            $dateStart = date('Y-m-d 00:00:00', strtotime('- 1 day'));
            $dateEnd = date('Y-m-d 23:59:59', strtotime('- 1 day'));
        } else if($period == 'week'){
            $dateStart = DateHelper::getStartOfWeekDate()->format('Y-m-d 00:00:01');
            $dateEnd = DateHelper::getEndOfWeekDate()->format('Y-m-d 23:59:59');
        } else if($period == 'last_week'){
            $dateTime = new \DateTime("Monday last week ".date('Y-m-d'));
            $dateStart = $dateTime->format('Y-m-d 00:00:01');
            $dateEnd = $dateTime->modify('this week +6 days')->format('Y-m-d 23:59:59');
        } else if($period == 'last_month'){
            $dateStart = date("Y-m-01 00:00:01", strtotime("-1 month"));
            $dateEnd = date("Y-m-t 23:59:59", strtotime(date("Y-m-01", strtotime("-1 month"))));
        } else if($period == 'month'){
            $dateStart = date("Y-m-01 00:00:01");
            $dateEnd = date("Y-m-t 23:59:59");
        } else if($period != null){
            $dates = explode(' - ', $period);
            if(count($dates) == 2){
                $dateStart = $dates[0];
                $dateEnd = $dates[1];
            }
        }



        $cashiers = Cashbox::find()->asArray()->all();

        array_walk($cashiers, function(&$cashier){
            $cashier['debit'] = Payments::find()->where(['cashbox_id' => $cashier['id'], 'type' => Payments::TYPE_DEBIT])->sum('sum');
            $cashier['credit'] = Payments::find()->where(['cashbox_id' => $cashier['id'], 'type' => Payments::TYPE_CREDIT])->sum('sum');

            $cashier['balance'] = Payments::find()->where(['cashbox_id' => $cashier['id'], 'type' => Payments::TYPE_DEBIT])->sum('sum') - Payments::find()->where(['cashbox_id' => $cashier['id'], 'type' => Payments::TYPE_CREDIT])->sum('sum');
        });


        $rgbaColors = [
            'rgba(23, 191, 205,0.6)',
            'rgba(205, 163, 23,0.6)',
            'rgba(20, 201, 20,0.6)',
            'rgba(199, 20, 121,0.6)',
            'rgba(194, 37, 19,0.6)',
        ];

        // ------------------------
        // Показатели по менеджерам
        // ------------------------
        $ordersDates = ArrayHelper::getColumn(Order::find()->andFilterWhere(['between', 'created_at', $dateStart, $dateEnd])->all(), 'created_at');

        array_walk($ordersDates, function(&$date){
            $date = date('Y-m-d', strtotime($date));
        });
        
        $ordersDates = array_unique($ordersDates);

        // Делаем читаемые даты для графика
        $ordersDatesLabels = [];
        foreach ($ordersDates as $date)
        {
            $ordersDatesLabels[] = Yii::$app->formatter->asDate($date, 'php:d M');
        }
        $ordersLabels = $ordersDatesLabels;


        $ordersDataset = [];

        $users = User::find()->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();

        // Собираем данные по пользователям за конкретные даты ($ordersDates)
        foreach ($users as $user){
            $ordersCount = Order::find()->where(['manager_id' => $user->id]);
            $orCondition = ['or'];
            $data = [];
            foreach ($ordersDates as $date)
            {
                $orCondition = ArrayHelper::merge($orCondition, [['like', 'created_at', $date]]);
                $data[] = intval(Order::find()->where(['manager_id' => $user->id])->andWhere(['like', 'created_at', $date])->count());
            }
            $ordersCount->andWhere($orCondition);

            $ordersDataset[] = [
                'label' => $user->name,
                'total' => intval($ordersCount->count()),
                'data' => $data,
            ];
        }

        // Сортируем по общему кол-ву
        usort($ordersDataset, function($a, $b){
            return $a['total'] < $b['total'];
        });

        // Убираем пользователей у которых вообще нет заказов
        $ordersDataset = array_filter($ordersDataset, function($model){
            return $model['total'] > 0;
        });

        // Оставляем только 4 пользователей
        if(count($ordersDataset) > 4){
            $ordersDataset = array_slice($ordersDataset, 1, 4);
        }

        // Присваиваем цвета
        for ($i = 0; $i < count($ordersDataset); $i++)
        {
            $ordersDataset[$i]['backgroundColor'] = isset($rgbaColors[$i]) ? $rgbaColors[$i] : null;
        }

        // Добавляем админа к данным
        $admin = User::find()->where(['role' => User::ROLE_ADMIN])->one();
        if($admin){
            $data = [];
            foreach ($ordersDates as $date)
            {
                $data[] = intval(Order::find()->where(['manager_id' => $admin->id])->andWhere(['like', 'created_at', $date])->count());
            }

            $ordersDataset[] = [
                'label' => $admin->name,
                'total' => null,
                'data' => $data,
                'backgroundColor' => $rgbaColors[4],
            ];
        }

        // Убираем из конечного массива лишнее вспомогательные данные
        array_walk($ordersDataset, function(&$model){
            unset($model['total']);
        });


        // ------------
        // Задолженость
        // ------------
        /** @var Order[] $orders */
        $orders = Order::find()
            ->joinWith(['status'])
            ->andFilterWhere(['between', 'created_at', $dateStart, $dateEnd])
            ->andWhere(['order_status.name' => ['Отгружен', 'Упакован']])
            ->all();

        $creditors = [];
        foreach ($orders as $order)
        {
            $scores = Score::find()->where(['type' => Score::TYPE_SCORE, 'order_id' => $order->id, 'status' => 0])->all();
//            VarDumper::dump(count($scores), 10, true);
            if(count($scores)){
                $customer = $order->customer;
                if($customer){
                    if(isset($creditors[$customer->id])){
                        $creditors[$customer->id] = $creditors[$customer->id] + array_sum(ArrayHelper::getColumn($scores, 'amount'));
                    } else {
                        $creditors[$customer->id] = array_sum(ArrayHelper::getColumn($scores, 'amount'));
                    }
                }
            }
        }



        // ------------------------
        // Диаграмма по менеджерам
        // ------------------------
        $scoreDates = ArrayHelper::getColumn(Score::find()->andFilterWhere(['between', 'created_at', $dateStart, $dateEnd])->andWhere(['type' => Score::TYPE_SCORE, 'status' => 1])->all(), 'created_at');


        array_walk($scoreDates, function(&$date){
            $date = date('Y-m-d', strtotime($date));
        });

        $scoreDates = array_unique($scoreDates);

        // Делаем читаемые даты для графика
        $scoreDatesLabels = [];
        foreach ($scoreDates as $date)
        {
            $scoreDatesLabels [] = Yii::$app->formatter->asDate($date, 'php:d M');
        }
        $scoreLabels = $scoreDatesLabels;


        $scoreDataset = [];

        $users = User::find()->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();

        // Собираем данные по пользователям за конкретные даты ($ordersDates)
        foreach ($users as $user){
            $scoreCount = Score::find()->where(['created_by' => $user->id]);
            $orCondition = ['or'];
            $data = [];
            foreach ($scoreDates as $date)
            {
                $orCondition = ArrayHelper::merge($orCondition, [['like', 'created_at', $date]]);
                $data[] = intval(Score::find()->where(['created_by' => $user->id])->andWhere(['like', 'created_at', $date])->sum('amount'));
            }
            $scoreCount->andWhere($orCondition);

            $scoreDataset[] = [
                'label' => $user->name,
                'total' => intval($scoreCount->sum('amount')),
                'data' => $data,
            ];
        }

        // Сортируем по общему кол-ву
        usort($scoreDataset, function($a, $b){
            return $a['total'] < $b['total'];
        });

        // Убираем пользователей у которых вообще нет заказов
        $scoreDataset = array_filter($scoreDataset, function($model){
            return $model['total'] > 0;
        });

        // Оставляем только 4 пользователей
        if(count($scoreDataset) > 4){
            $scoreDataset = array_slice($scoreDataset, 1, 4);
        }

        // Присваиваем цвета
        for ($i = 0; $i < count($scoreDataset); $i++)
        {
            $scoreDataset[$i]['backgroundColor'] = isset($rgbaColors[$i]) ? $rgbaColors[$i] : null;
        }

        // Добавляем админа к данным
        $admin = User::find()->where(['role' => User::ROLE_ADMIN])->one();
        if($admin){
            $data = [];
            foreach ($scoreDates as $date)
            {
                $data[] = intval(Score::find()->where(['created_by' => $admin->id])->andWhere(['like', 'created_at', $date])->sum('amount'));
            }

            $scoreDataset[] = [
                'label' => $admin->name,
                'total' => null,
                'data' => $data,
                'backgroundColor' => $rgbaColors[4],
            ];
        }

        // Убираем из конечного массива лишнее вспомогательные данные
        array_walk($scoreDataset, function(&$model){
            unset($model['total']);
        });



        // ------------------------
        // Показатели компании
        // ------------------------
        $payedDates = ArrayHelper::getColumn(Score::find()->andFilterWhere(['between', 'created_at', $dateStart, $dateEnd])->andWhere(['type' => Score::TYPE_SCORE, 'status' => 1])->all(), 'created_at');

        array_walk($payedDates, function(&$date){
            $date = date('Y-m-d', strtotime($date));
        });

        $payedDates = array_unique($payedDates);

        // Делаем читаемые даты для графика
        $payedDatesLabels = [];
        foreach ($scoreDates as $date)
        {
            $payedDatesLabels [] = Yii::$app->formatter->asDate($date, 'php:d M');
        }
        $payedLabels = $payedDatesLabels;


        $payedDataset = [];

        $payedData = [];
        $payedOrderData = [];

        foreach ($payedDates as $date)
        {
            $payedData[] = intval(Score::find()->andWhere(['like', 'created_at', $date])->sum('amount'));

            $orders = Order::find()->all();
            $payedSum = 0;
            foreach ($orders as $order){
                $amount = intval(Score::find()->andWhere(['like', 'created_at', $date])->andWhere(['order_id' => $order->id])->sum('amount'));

                $partSearchModel = new OrderPartSearch();
                $partDataProvider = $partSearchModel->search(Yii::$app->request->queryParams);
                $partDataProvider->query->andWhere(['order_id' => $order->id]);
                $partDataProvider->pagination = false;

                $totalMaterialPrice = 0;
                $paintPriceTotal = 0;
                foreach ($partDataProvider->models as $model)
                {
                    $totalMaterialPrice += $model->material_price;
                    $paintPriceTotal += $model->paint_price;
                }

                $amount = $amount - $totalMaterialPrice - $paintPriceTotal;

                $payedSum = $payedSum + $amount;
            }

            $payedOrderData[] = $payedSum;
        }


        // ------------------------
        // Показатели по оцифровке
        // ------------------------
        $numberingDates = ArrayHelper::getColumn(Order::find()->andFilterWhere(['between', 'created_at', $dateStart, $dateEnd])->all(), 'created_at');

        array_walk($numberingDates, function(&$date){
            $date = date('Y-m-d', strtotime($date));
        });

        $numberingDates = array_unique($numberingDates);

        // Делаем читаемые даты для графика
        $numberingDatesLabels = [];
        foreach ($numberingDates as $date)
        {
            $numberingDatesLabels[] = Yii::$app->formatter->asDate($date, 'php:d M');
        }
        $numberingLabels = $numberingDatesLabels;


        $numberingDataset = [];

        $users = User::find()->andWhere(['!=', 'role', User::ROLE_ADMIN])->all();

        // Собираем данные по пользователям за конкретные даты ($ordersDates)
        foreach ($users as $user){
            $ordersCount = Order::find()->where(['manager_id' => $user->id]);
            $orCondition = ['or'];
            $data = [];
            foreach ($numberingDates as $date)
            {
                $orCondition = ArrayHelper::merge($orCondition, [['like', 'created_at', $date]]);
                $data[] = intval(Order::find()->where(['manager_id' => $user->id])->andWhere(['like', 'created_at', $date])->sum('numbering'));
            }
            $ordersCount->andWhere($orCondition);

            $numberingDataset[] = [
                'label' => $user->name,
                'total' => intval($ordersCount->sum('numbering')),
                'data' => $data,
            ];
        }

        // Сортируем по общему кол-ву
        usort($numberingDataset, function($a, $b){
            return $a['total'] < $b['total'];
        });

        // Убираем пользователей у которых вообще нет заказов
        $numberingDataset = array_filter($numberingDataset, function($model){
            return $model['total'] > 0;
        });

        // Оставляем только 4 пользователей
        if(count($numberingDataset) > 4){
            $numberingDataset = array_slice($numberingDataset, 1, 4);
        }

        // Присваиваем цвета
        for ($i = 0; $i < count($numberingDataset); $i++)
        {
            $numberingDataset[$i]['backgroundColor'] = isset($rgbaColors[$i]) ? $rgbaColors[$i] : null;
        }

        // Добавляем админа к данным
        $admin = User::find()->where(['role' => User::ROLE_ADMIN])->one();
        if($admin){
            $data = [];
            foreach ($numberingDates as $date)
            {
                $data[] = intval(Order::find()->where(['manager_id' => $admin->id])->andWhere(['like', 'created_at', $date])->count());
            }

            $numberingDataset[] = [
                'label' => $admin->name,
                'total' => null,
                'data' => $data,
                'backgroundColor' => $rgbaColors[4],
            ];
        }

        // Убираем из конечного массива лишнее вспомогательные данные
        array_walk($numberingDataset, function(&$model){
            unset($model['total']);
        });

        return $this->render('index', [
            'cashiers' => $cashiers,
            'ordersDataset' => $ordersDataset,
            'ordersLabels' => $ordersLabels,
            'creditors' => $creditors,
            'scoreDataset' => $scoreDataset,
            'scoreLabels' => $scoreLabels,
            'payedLabels' => $payedLabels,
            'payedData' => $payedData,
            'payedOrderData' => $payedOrderData,
            'numberingDataset' => $numberingDataset,
            'numberingLabels' => $numberingLabels,
        ]);
    }
}

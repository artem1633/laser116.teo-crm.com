<?php

namespace app\controllers;

use Yii;
use app\models\Available;
use app\models\AvailableSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\manual\Product;
use app\models\Resource;
use app\models\ResourceSearch;
use app\models\Storage;
use app\models\Move;
use app\models\Users;
use yii\data\ActiveDataProvider;

/**
 * AvailableController implements the CRUD actions for Available model.
 */
class AvailableController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Available models.
     * @return mixed
     */
    public function actionIndex()
    {
        $post = Yii::$app->request->post();

        // Остатки
        $searchModel = new ResourceSearch();
        $remainResourceProvider = $searchModel->searchRemainResource(Yii::$app->request->queryParams/*, $post*/);
        $remainResource = $this->renderPartial('remain', [
            'dataProvider' => $remainResourceProvider,
            'searchModel' => $searchModel,
            //'post' => $post,
        ]);

        //Ожидает
        $waitingResourceProvider = $searchModel->searchWaitingResource(Yii::$app->request->queryParams, $post);
        $waitingResourceProvider->query->andWhere(['type' => Available::TYPE_INCOME]);
        $waitingResource = $this->renderPartial('waiting', [
            'dataProvider' => $waitingResourceProvider,
            'searchModel' => $searchModel,
            'post' => $post,
        ]);

        //Оприходано
        $availableModel = new AvailableSearch();
        $accruedProvider = $availableModel->searchAccrued(Yii::$app->request->queryParams, $post);
        $accruedProvider->query->andWhere(['type' => Available::TYPE_INCOME]);
        $contentInResourceWaid = $this->renderPartial('inresource', [
            'dataProvider' => $accruedProvider,
            'searchModel' => $searchModel,
            'post' => $post,
        ]);

        //Списано
        $writeoffModel = new ResourceSearch();
        $writeoffProvider = $writeoffModel->searchWaitingResource(Yii::$app->request->queryParams, $post);
        $writeoffProvider->query->andWhere(['type' => Available::TYPE_WRITE_OFF]);
        $writeoffContent = $this->renderPartial('writeoff', [
            'dataProvider' => $writeoffProvider,
            'searchModel' => $writeoffModel,
            'post' => $post,
        ]);


        $result = Move::find()->orderBy(['id'=>SORT_DESC])->all();

        return $this->render('index', [
            'contentInResourceWaid' => $contentInResourceWaid,
            'remainResource' => $remainResource,
            'waitingResource' => $waitingResource,
            'writeoffContent' => $writeoffContent,
            'result' => $result,
            'post' => $post,
        ]);
    }

    public function actionViewAvailable($id)
    {
        $availables = Available::find()->where(['number' => $this->findModel($id)->number ])->all();
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Товары",
                'size' => 'large',
                'content'=>$this->renderAjax('view-available', [
                    'result' => $availables,
                ]),
            ];
        }else{
            return $this->render('view-available', [
                'result' => $availables,
            ]);
        }
    }
    public function actionView($id)
    {
        $availables = Available::find()->where(['number' => $this->findModel($id)->number ])->all();
        return $this->render('view', [
            'result' => $availables,
        ]);
    }

    /**
     * Creates a new Available model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null, $status = null, $storage_id = null, $postavshik_id = null, $type = 1)
    {
        $request = Yii::$app->request;
        $model = new Available();
        $model->status_id = $status;
        $model->storage_id = $storage_id;
        $model->type = $type;
        $model->postavshik_id = $postavshik_id;

        /*if(Yii::$app->user->identity->role_id != Users::USER_ROLE_ADMIN) $model->storage_id = Yii::$app->user->identity->atelier_id;*/
        if($model->storage_id == null) $model->storage_id = Storage::find()->where(['is_main' => 1])->one()->id;
        $model->storage_name = $model->storage->name;

        if ($id > 0)
        {
            $query = Available::find()->where(['number'=>$id]);
            $model->number = $id;
        }
        else $query = Available::find()->where(['id'=>'null']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 10,
                'validatePage' => false,
            ],
        ]);

        if ($model->load($request->post()) && $model->save())
        {
            if($model->number == null) { $model->number = $model->id; $model->save();}

            if($model->status_id == 1)
            {
                $resource = Resource::find()->where([
//                    'provider_id' => $model->provider_id,
//                    ''
                    'product_id' => $model->product_id,
                    'category_id' => $model->category_id,
                    'storage_id' => $model->storage_id,
                    'status_id' => 1,
                ])->one();
            }

            if($model->status_id == 3)
            {
                $resource = Resource::find()->where([
//                    'provider_id' => $model->provider_id,
                    'product_id' => $model->product_id,
                    'category_id' => $model->category_id,
                    'storage_id' => $model->storage_id,
                    'status_id' => 3,
                    'type' => $type,
                    'number' => $model->number,
                ])->one();
            }

            if($resource != null)
            {
                $resource->price = $model->price;
                $resource->price_shop = $model->price_shop;
                $resource->count += $model->count;
                $resource->save(false);
            }
            else
            {
                $resource = new Resource();
                $resource->attributes = $model->attributes;
                $resource->save(false);
            }


            return $this->redirect(['create', 'id' => $model->number, 'storage_id' => $model->storage_id, 'status' => $model->status_id, 'postavshik_id' => $model->postavshik_id]);
        }
        else
        {
            \Yii::warning($model->errors, 'Available model errors');
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Available model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $oldCount = $model->count;



        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $resource = Resource::find()->where([
//                    'provider_id' => $model->provider_id,
                    'product_id' => $model->product_id,
                    'category_id' => $model->category_id,
                    'storage_id' => $model->storage_id,
                    'status_id' => 1,
                    'received' => 1,
                    // 'number' => $model->number,
                ])->one();

                if($resource){
                    $resource->count = $resource->count - $oldCount;
                    $resource->count = $resource->count + $model->count;
                    $resource->save(false);
                }


                return [
                    'forceReload'=>'#available-pjax',
                    'forceClose'=>true,
                ];
            }else{
                return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionSetPrice($id)
    {
        $product = Product::findOne($id);
        return $product->cost;
    }

    public function actionMoveStorage($id, $type, $forceReload)
    {
        $request = Yii::$app->request;
        $model = new Storage();
        if($type == 'available') $tovar = Available::findOne($id);
        else $tovar = Resource::findOne($id);
        $tovar->move_count = 1;//$tovar->count;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){

                $post = $request->post();
                if($type == 'available'){
                    $count = $post['Available']['move_count'];
                    $storage_id = $post['Available']['storage_id'];
                }
                else {
                    $count = $post['Resource']['move_count'];
                    $storage_id = $post['Resource']['storage_id'];
                }

//                if(Yii::$app->user->identity->access_for_moving != 1)
//                    return [
//                        'title'=> "".'<b>Ошибка</b>',
//                        'size' => 'normal',
//                        'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете переместить товаров. Потому что для перемещения вам не хватает прав</b></span></center>',
//                    ];

                if((int)($tovar->count) < $count) {
                    $defis = (int)$count - $tovar->count;
                    return [
                        'title'=> "".'<b>Ошибка</b>',
                        'size' => 'normal',
                        'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете переместить '. $count.' товаров. Потому что для перемещения вам не хватает '.$defis.' товаров </b></span></center>',
                    ];
                }


                if($type == 'available'){
                    $new_available = Available::find()->where(['product_id' => $tovar->product_id, 'storage_id' => $storage_id])->one();
                    if($new_available == null) {
                        $new_available = new Available();
                        $new_available->attributes = $tovar->attributes;
                        if($tovar->type == Available::TYPE_INCOME){ // Пополнение
                            $new_available->count = $count;
                        } elseif($tovar->type == Available::TYPE_WRITE_OFF){ // Списание
                            $new_available->count = 0 - $count;
                        }
                        $new_available->storage_id = $storage_id;
                        $new_available->save();
                    }
                    else{
                        if($tovar->type == Available::TYPE_INCOME){ // Пополнение
                            $new_available->count = $new_available->count + $count;
                        } elseif($tovar->type == Available::TYPE_WRITE_OFF){ // Списание
                            $new_available->count = $new_available->count - $count;
                        }
                        $new_available->save();
                    }
                }
                else {
                    $resource = Resource::find()->where(['good_id' => $tovar->good_id, 'product_id' => $tovar->product_id, 'storage_id' => $storage_id, 'status_id' => $tovar->status_id])->one();
                    if($resource == null) {
                        $resource = new Resource();
                        $resource->attributes = $tovar->attributes;
                        if($tovar->type == Available::TYPE_INCOME){ // Пополнение
                            $resource->count = $count;
                        } elseif($tovar->type == Available::TYPE_WRITE_OFF){ // Списание
                            $resource->count = 0 - $count;
                        }
                        $resource->count = $count;
                        $resource->storage_id = $storage_id;
                        $resource->save();
                    }
                    else{
                        if($tovar->type == Available::TYPE_INCOME){ // Пополнение
                            $resource->count = $resource->count + $count;
                        } elseif($tovar->type == Available::TYPE_WRITE_OFF){ // Списание
                            $resource->count = $resource->count - $count;
                        }
                        $resource->save();
                    }
                }


                $tovar->count = $tovar->count - $count;
                $tovar->save();

                $move = new Move();
                $move->table = $type;
                $move->storage_form = $tovar->storage_id;
                $move->storage_to = $storage_id;
                $move->old_count = $tovar->count;
                $move->sending_count = $count;
                $move->part_id = $tovar->product_id;
                $move->data = date('Y-m-d H:i:s');
                $move->save(false);

                return ['forceClose'=>true, 'forceReload'=> $forceReload];
            }else{
                return [
                    'title'=> "Перемещение",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('move-available', [
                        'model' => $model,
                        'tovar' => $tovar,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Переместить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->render('move-available', [
                'model' => $model,
                'tovar' => $tovar,
            ]);
        }
    }

    public function actionMoveStorageWaiting($id, $type, $forceReload)
    {
        $request = Yii::$app->request;
        $tovar = Resource::findOne($id);
        Yii::$app->response->format = Response::FORMAT_JSON;

//        if(Yii::$app->user->identity->access_for_moving != 1){
//            return [
//                'title'=> "".'<b>Ошибка</b>',
//                'size' => 'normal',
//                'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете переместить товаров. Потому что для перемещения вам не хватает прав</b></span></center>',
//            ];
//        }

        $resources = Resource::find()->where(['number' => $tovar->number, 'status_id' => 3])->all();


//        $availables = Available::find()->where(['number' => $tovar->number])->all();
        $availables = Available::updateAll(['received' => 1], ['number' => $tovar->number]);


        foreach ($resources as $value)
        {
            $resource = Resource::find()->where([
                'product_id' => $value->product_id,
//                'postavshik_id' => $value->postavshik_id,
//                'good_id' => $value->good_id,
                'category_id' => $value->category_id,
                'storage_id' => $value->storage_id,
                'status_id' => 1,
                'received' => 1,
            ])->one();

            if($resource == null) {
                $resource = new Resource();
                $resource->attributes = $value->attributes;
                if($tovar->type == Available::TYPE_INCOME){ // Пополнение
                    $resource->count = $value->count;
                } elseif($tovar->type == Available::TYPE_WRITE_OFF){ // Списание
                    $resource->count = 0 - $value->count;
                }
                $resource->status_id = 1;
                $resource->received = 1;
                $resource->save();
            }
            else{
                if($tovar->type == Available::TYPE_INCOME){ // Пополнение
                    $resource->count = $resource->count + $value->count;
                } elseif($tovar->type == Available::TYPE_WRITE_OFF){ // Списание
                    $resource->count = $resource->count - $value->count;
                }
                $resource->save();
            }
            $value->received = 1;
            $value->save();

            $move = new Move();
            $move->table = $type;
            $move->storage_form = $value->storage_id;
            $move->storage_to = $value->storage_id;
            $move->old_count = $value->count;
            $move->sending_count = $resource->count;
            $move->part_id = $value->product_id;
            $move->data = date('Y-m-d H:i:s');
            $move->save();
        }
        return ['forceClose'=>true, 'forceReload'=> $forceReload];
    }

    /**
     * Delete an existing Available model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();


        $resource = Resource::find()->where([
//                    'provider_id' => $model->provider_id,
            'product_id' => $model->product_id,
            'category_id' => $model->category_id,
            'storage_id' => $model->storage_id,
            'status_id' => 1,
            'number' => $model->number,
        ])->one();

        if($resource){
            $resource->count = $resource->count - $model->count;
            $resource->save(false);
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#available-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }
    public function actionDeleteAvailable($id)
    {
        $request = Yii::$app->request;
        $available = Available::find()->where(['number' => $this->findModel($id)->number])->all();
        foreach ($available as $value) {
            $value->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=> '#available-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Available model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Available model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Available the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Available::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use app\behaviors\RoleBehavior;
use app\components\helpers\SmsSender;
use app\components\TagHelper;
use app\models\forms\ResetPasswordForm;
use app\models\Order;
use app\models\ReportSettingColumn;
use app\models\Task;
use app\models\User;
use rmrevin\yii\module\Comments\models\Comment;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest($id)
    {
        $model = \app\models\Order::findOne($id);
        $parts = \app\models\OrderPart::find()->where(['order_id' => $id])->all();


        // Creating the new document...
        $phpWord = new \PhpOffice\PhpWord\PhpWord();


        $cellHCentered = ['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER];


        $section = $phpWord->addSection();

        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell(2000, $cellHCentered);

        $cell->addText('Р.Ф. 420095, г. Казань, ул. В. Кулагина, д.9.', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('ИНН 1659217014', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('КПП 165901001', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('+7 (937) 600 30 30', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('+7 (843) 245 41 41', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);
        $cell->addText('zakaz@tatlazer.ru', ['name' => 'Calibri', 'size' => 10, 'italic' => true, 'alignment' => 'center']);


        $cell = $table->addCell(5000, $cellHCentered);
        $cell->addImage(\yii\helpers\Url::toRoute('/logo.jpg', true), ['height' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
        $cell->addText('', ['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
        $cell->addText('         ТАТЛАЗЕР', ['bold' => true, 'size' => 24, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
        $cell->addText('        ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ', ['size' => 8, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);

        $cell = $table->addCell(2000, $cellHCentered);

        $cell->addText('RUSSIA 420095, Kazan city, str. V. Kulagina 9', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('TIN 1659217014', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('CAT 165901001', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('+7 (937) 600 30 30', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('+7 (843) 245 41 41', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);
        $cell->addText('zakaz@tatlazer.ru', ['name' => 'Calibri', 'size' => 10, 'italic' => true]);


        $section->addText('');
        $section->addText('');
        $section->addText('Исполнитель: ООО «ТАТЛАЗЕР»', ['bold' => true]);
        $section->addText('Заказчик: ООО «Заказчик»', ['bold' => true]);




        $rows = 10;
        $cols = 6;

        $tableStyle = [
            'borderColor' => '000000',
            'borderSize'  => 6,
            'cellMargin'  => 50,
            'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER,
            'valign' => 'center'
        ];


        $headersLabels = [
            1 => [
                'label' => '№',
                'width' => 700,
            ],
            2 => [
                'label' => 'Наименование',
                'width' => 3500,
            ],
            3 => [
                'label' => 'Ед.Изм.',
                'width' => 1000,
            ],
            4 => [
                'label' => 'Кол-во',
                'width' => 1000,
            ],
            5 => [
                'label' => 'Цена с НДС',
                'width' => 1300,
            ],
            6 => [
                'label' => 'Сумма с НДС',
                'width' => 1500,
            ],
        ];

        $table = $section->addTable($tableStyle);
        for ($r = 1; $r <= (count($parts)+1); $r++) {
            $table->addRow();

            if($r == 1){
                $part = null;
            } else {
                $part = $parts[($r - 2)];
            }

            for ($c = 1; $c <= $cols; $c++) {

                if($r == 1){
                    $table->addCell($headersLabels[$c]['width'])->addText($headersLabels[$c]['label']);
                } else {
                    // $table->addCell($headersLabels[$c]['width'])->addText("Row {$r}, Cell {$c}");


                    if($c == 1){
                        $table->addCell($headersLabels[$c]['width'])->addText(($r - 1));
                    } elseif($c == 2){
                        $table->addCell($headersLabels[$c]['width'])->addText("Деталь {$part->path_file}");
                    } elseif($c == 3){
                        $table->addCell($headersLabels[$c]['width'])->addText("шт");
                    } elseif($c == 4){
                        $table->addCell($headersLabels[$c]['width'])->addText($part->cut_num);
                    } elseif($c == 5){
                        $table->addCell($headersLabels[$c]['width'])->addText(Yii::$app->formatter->asDecimal($part->material_sheet_price).' ₽');
                    } elseif($c == 6){
                        $table->addCell($headersLabels[$c]['width'])->addText(Yii::$app->formatter->asDecimal(0).' ₽');
                    }
                }
            }
        }
        

        $table->addRow();

        $table->addCell(750, ['gridSpan' => 5])->addTextRun($cellHCentered)->addText('ИТОГО:');

        $table->addCell(750, ['gridSpan' => 1])->addText(Yii::$app->formatter->asDecimal(0).' ₽');


        $phpWord->addNumberingStyle(
            'multilevel',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                )
            )
        );

        $section->addListItem('Общая стоимость Изделий настоящей Спецификации составляет ___ (___) руб. __ копеек, в том числе НДС 20% ___ (____) руб. ___ копеек.', 0, null, 'multilevel');
        $section->addListItem('Изделия изготавливаются из материала Исполнителя, на основании предоставленных чертежей Заказчик.', 0, null, 'multilevel');
        $section->addListItem('Заказчик производит предоплату 100% в сумме ______ (___) рублей __ копеек путем перечисления денежных средств на расчетный счет Исполнителя.', 0, null, 'multilevel');
        $section->addListItem('Изготовление и поставка Изделий составляет __ (___) рабочих дней со дня поступления платежа на расчетный счет Исполнителя.', 0, null, 'multilevel');
        $section->addListItem('Базис поставки: Заказчик обязуется вывезти Изделия по адресу: РТ, г. Казань, ул. В. Кулагина д.9. ', 0, null, 'multilevel');
        $section->addListItem('Исполнитель уведомляет Заказчика к отгрузке Изделий путем направления в его адрес уведомления.', 0, null, 'multilevel');



        



        $section->addText('');
        $section->addText('');
        $section->addText('');
        $section->addText('');

        $section->addText('Надеемся на долгосрочное сотрудничество.', ['size' => 13, 'name' => 'Calibri', '']);
        $section->addText("С уважением", ['size' => 13, 'name' => 'Calibri', '']);
        $section->addText("директор ООО “ТАТЛАЗЕР”                           М.П.                       Юрков Э.К.", ['size' => 13, 'name' => 'Calibri', '']);




        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('helloWorld.docx');

        Yii::$app->response->sendFile('helloWorld.docx');
    }

    public function actionDashboard()
    {

        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);

        $user_id = Yii::$app->user->identity->id;
        $tasks = Task::find()->where(['responsible_id' => $user_id])->all();
        foreach ($tasks as $value) {
            $array [] = $value->name;
        }

        $comments = Comment::find()->orderBy(['created_at' => SORT_DESC])->all();

        $comment = [];
        foreach ($comments as $value) {
            $user = User::findOne($value->created_by);
            $str = $value->entity;
            $task_id = preg_replace("/[^0-9]/", '', $str);
            $task = Task::findOne($task_id);


            $name = 'Unknown';
            $task_title = 'Unknown';

            if($task != null) {
                $task_title = $task->name;
            }
            if ($user != null){
                $name = $user->name;
            }
            $comment [] = [
                'user' => $name,
                'title' => $task_title,
                'text' => $value->text,
                'id' => $user->id,
            ];
        }

        return $this->render('dashboard', [
//            'all_tasks' => $result1,
//            'result' => $result,
//             'total_hour' => $total_hour,
            'comment' => $comment,
            'tasks' => $tasks,
        ]);
    }


    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}

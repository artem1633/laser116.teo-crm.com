<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\models\Settings;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * BalancesController implements the CRUD actions for Balances model.
 */
class SettingsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Список параметров
     * @return mixed
     */
    public function actionIndex()
    {

        $request = Yii::$app->request;

        if($request->isPost)
        {
            $data = $request->post();
            $checked = [];

            foreach ($data['Settings'] as $key => $value) {
                $setting = Settings::findByKey($key);


                if ($setting != null) {
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }


        }

        $settings = Settings::find()->all();

        return $this->render('index', [
            'settings' => $settings,
        ]);
    }



}

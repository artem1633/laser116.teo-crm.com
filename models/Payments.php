<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $customer_id Контрагент
 * @property int $order_id Заказ
 * @property string $name Название
 * @property string $sum Сумма
 * @property int $type Тип
 * @property string $comment Комментарий
 * @property string $created_at Дата создания
 * @property int $created_by Кто создал
 * @property int $cashbox_id Квсса
 *
 *
 * @property User $createdBy
 */
class Payments extends \yii\db\ActiveRecord
{
    const TYPE_DEBIT = 1;
    const TYPE_CREDIT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'order_id', 'type', 'created_by','cashbox_id', 'score_id'], 'integer'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
            [['name', 'sum'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'created_by',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Контрагент',
            'score_id' => 'Счет',
            'order_id' => 'Заказ',
            'name' => 'Название',
            'sum' => 'Сумма',
            'type' => 'Тип',
            'comment' => 'Комментарий',
            'cashbox_id' => 'Касса',
            'created_at' => 'Дата и время создания',
            'created_by' => 'Кто создал',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_DEBIT => 'Поступление',
            self::TYPE_CREDIT => 'Списание'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * {@inheritdoc}
     * @return PaymentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentsQuery(get_called_class());
    }
}

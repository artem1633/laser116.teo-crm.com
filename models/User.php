<?php

namespace app\models;

use rmrevin\yii\module\Comments\interfaces\CommentatorInterface;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $user_type тип
 * @property string $login Логин
 * @property string $name ФИО
 * @property string $email email
 * @property string $email_password Email пароль
 * @property string $phone телефон
 * @property string $birth_date дата рождения
 * @property string $avatar Аватар
 * @property string $password_hash Зашифрованный пароль
 * @property int $is_deletable Можно удалить или нельзя
 * @property string $created_at
 * @property string $info
 * @property string $color
 * @property integer $role
 * 'color','role'
 *
 * @property UploadedFile $file
 *

 */
class User extends ActiveRecord implements IdentityInterface, CommentatorInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_CUTTING = 3;
    const ROLE_BEND = 4;
    const ROLE_PAINTING = 5;
    const ROLE_WELDING = 6;
    const ROLE_DIRECTOR = 7;


    public $password;

    public $user_type;


    public $file;

    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'name', 'login', 'email_password', 'is_deletable',
                'info', 'password', 'password_hash',
                'phone', 'address', 'birth_date', 'avatar','color','role'],
            self::SCENARIO_EDIT => [
                'name', 'login', 'email_password', 'type', 'is_deletable', 'password',
                'info', 'password_hash', 'role', 'city_id',
                'phone', 'birth_date', 'avatar','color'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'name'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
            ['birth_date', function () {
                if ($this->birth_date > date('Y-m-d') or strlen($this->birth_date) > 10) {
                    $this->addError('birth_date', 'Некорректная дата');
                    return false;
                }
                return true;
            }],
            ['login', 'email'],
            [['login'], 'unique'],
            ['file', 'file'],
            [['is_deletable','role'], 'integer'],
            [['login', 'password_hash', 'password', 'name', 'avatar','color', 'email_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        $uid = $identity->id;

        if ($uid == $this->id) {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if ($this->is_deletable == false) {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }
    }

    public function getCommentatorAvatar()
    {
        return '/' . $this->getRealAvatar();
    }

    public function getCommentatorName()
    {
        return $this->name;
    }

    public function getCommentatorUrl()
    {
        return ['user/view', 'id' => $this->id]; // or false, if user does not have a public page
    }

    /**
     * @return string
     */
    public function getRealAvatar()
    {
        return $this->avatar != null ? $this->avatar : 'img/nouser.png';
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->file != null) {
                $fileName = Yii::$app->security->generateRandomString();
                if (is_dir('upload') == false) {
                    mkdir('upload');
                }
                $path = "uploads/{$fileName}.{$this->file->extension}";
                $this->file->saveAs($path);
                if ($this->file != null && file_exists($this->file)) {
                    unlink($this->file);
                }
                $this->avatar = $path;
            }

            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s');
            }
            if ($this->password != null) {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }


            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Email',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Номер телефона ',
            'type' => 'Тип пользователя',
            'birth_date' => 'Дата рождения',
            'file' => 'Фотография',
            'avatar' => 'Фотография',
            'password_hash' => 'Password Hash',
            'is_deletable' => 'Можно ли удалять',
            'created_at' => 'Дата создания',
            'info' => 'Информация',
            'email_password' => 'Пароль от Email',
            'color' => 'Цвет',
            'role' => 'Роль'
        ];
    }

    /**
     * @return array
     */
    public static function roleLabels()
    {
        return [
            self::ROLE_ADMIN => 'Администратор CRM',
            self::ROLE_MANAGER =>  'Менеджер',
            self::ROLE_CUTTING => 'Резка',
            self::ROLE_BEND => 'Гибка',
            self::ROLE_PAINTING => 'Покраска',
            self::ROLE_WELDING => 'Сварка',
            self::ROLE_DIRECTOR => 'Начальник цеха'
        ];
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }



}

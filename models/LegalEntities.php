<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "legal_entities".
 *
 * @property int $id
 * @property string $name Название
 * @property string $address Адрес
 * @property string $director Директор
 * @property string $post_address Почтовый адрес
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property string $r_s Р/С
 * @property string $k_s К/С
 * @property string $bic БИК
 */
class LegalEntities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'legal_entities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address', 'director', 'post_address', 'inn', 'kpp', 'r_s', 'k_s', 'bic'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'address' => 'Адрес',
            'director' => 'Директор',
            'post_address' => 'Почтовый адрес',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'r_s' => 'Р/С',
            'k_s' => 'К/С',
            'bic' => 'БИК',
        ];
    }

    /**
     * {@inheritdoc}
     * @return LegalEntitiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LegalEntitiesQuery(get_called_class());
    }
}

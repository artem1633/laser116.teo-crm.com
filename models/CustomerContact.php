<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_contact".
 *
 * @property int $id
 * @property string $fio ФИО контактного лица
 * @property string $position Должность
 * @property string $mobile Мобильный телефон
 * @property string $phone Служебный телефон
 * @property string $email email
 * @property int $customer_id Контрагент
 *
 * @property Customer $customer
 */
class CustomerContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['fio', 'position', 'mobile', 'phone', 'email'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО контактного лица',
            'position' => 'Должность',
            'mobile' => 'Мобильный телефон',
            'phone' => 'Служебный телефон',
            'email' => 'email',
            'customer_id' => 'Контрагент',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * {@inheritdoc}
     * @return CustomerContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerContactQuery(get_called_class());
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pattern_document".
 *
 * @property int $id
 * @property string $name Название
 * @property string $text Текст
 */
class PatternDocument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pattern_document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Текст',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PatternDocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatternDocumentQuery(get_called_class());
    }
}

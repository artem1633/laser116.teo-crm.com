<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[WeldingStatus]].
 *
 * @see WeldingStatus
 */
class WeldingStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return WeldingStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return WeldingStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
/**
 * This is the model class for table "resource".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $creator_id
 * @property integer $storage_id
 * @property integer $status_id
 * @property integer $count
 * @property double $price
 * @property double $price_shop
 * @property integer $good_id
 * @property string $comment
 * @property int $provider_id Поставщик
 * @property int $category_id Категория
 *
 * @property MaterialParams $category
 * @property Product $available
 * @property Users $creator
 * @property Goods $good
 * @property Materials $product
 * @property Provider $provider
 * @property ProductStatus $status
 * @property Storage $storage
 */
class Resource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $move_count;
    public $tovar;
    public $storages;
    public $active_window;


    const TYPE_INCOME = 1;
    const TYPE_WRITE_OFF = 2;

    public static function tableName()
    {
        return 'resource';
    }


    /**
     * @inheritdoc
     */
    /*public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
    }*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'creator_id', 'storage_id', 'status_id', 'count', 'good_id', 'move_count', 'postavshik_id', 'active_window', 'number', 'received', 'provider_id', 'category_id', 'type'], 'integer'],
            [['price', 'price_shop'], 'number'],
            [['comment'], 'string'],
            [['date_cr'], 'safe'],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\Goods::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Materials::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\ProductStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['postavshik_id'], 'exist', 'skipOnError' => true, 'targetClass' => manual\Suppliers::className(), 'targetAttribute' => ['postavshik_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Материал',
            'creator_id' => 'Создатель',
            'storage_id' => 'Склад',
            'status_id' => 'Статус',
            'count' => 'Количество',
            'price' => 'Цена',
            'price_shop' => 'Цена закупа',
            'good_id' => 'Категория товар',
            'comment' => 'Комментария',
            'move_count' => 'Количество',
            'tovar' => 'Товар',
            'storages' => 'Склады',
            'postavshik_id' => 'Поставщик',
            'number' => 'Номер документа',
            'date_cr' => 'Дата создания',
            'received' => 'Поступило',
            'provider_id' => 'Поставщик',
            'category_id' => 'Толщина',
            'type' => 'Тип',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->creator_id = Yii::$app->user->identity->id;
            $this->date_cr = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(MaterialParams::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(manual\Goods::className(), ['id' => 'good_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Materials::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(manual\ProductStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostavshik()
    {
        return $this->hasOne(manual\Suppliers::className(), ['id' => 'postavshik_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    public function getProductList()
    {
        $product = manual\Product::find()->all();
        return ArrayHelper::map($product, 'id', 'name');
    }

    public function getStorageList()
    {
        $storage = Storage::find()->all();
        return ArrayHelper::map($storage, 'id', 'name');
    }

    public function getStatusList()
    {
        $product_status = manual\ProductStatus::find()->all();
        return ArrayHelper::map($product_status, 'id', 'name');
    }

    public function getGoodList()
    {
        $goods = manual\Goods::find()->all();
        return ArrayHelper::map($goods, 'id', 'name');
    }

    public function getOtherStorageList()
    {
        $result = [];
        $storages = Storage::find()->all();
        foreach ($storages as $storage) {
            
            if($this->storage_id != $storage->id){
                if($storage->is_main == 1)
                    $result [] = [
                        'id' => $storage->id,
                        'name' => 'Пользователь : Администратор / ' .$storage->user->name . $storage->name,
                    ];
                else $result [] = [
                    'id' => $storage->id,
                    'name' => 'Пользователь :' .$storage->user->name  . ' / '. $storage->name,
                ];
            }
        }
        return ArrayHelper::map($result,'id', 'name');
    }
}

<?php

namespace app\models;

use app\components\TagHelper;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $name Имя
 * @property string $number Номер
 * @property int $customer_id Заказчик
 * @property int $manager_id Менеджер
 * @property int $status_id Статус заказа
 * @property int $cutting_status_id Статус резки
 * @property int $bend_status_id Статус гибки
 * @property int $welding_status_id Статус сварки
 * @property int $paint_status_id Статус покраски
 * @property int $num_set Кол-во комплектов
 * @property int $all_count Общее кол-во
 * @property int $customer_contact_id Контактное лицо
 * @property double $price Сумма заказа
 * @property double $total_time Кол-во времени, час
 * @property double $total_shift Кол-во смен
 * @property int $pay_status Статус оплаты
 * @property int $pay_no_tax Оплата без НДС
 * @property string $planned_date Планируемая дата
 * @property int $show_discount Показывать скидку
 * @property double $price_painting Цена покраски
 * @property double $price_bend Цена гиба
 * @property double $cutting_discount Скидка на резку
 * @property int $numbering Оцифровка
 * @property double $price_delivery_metal Цена доставки металла
 * @property double $round_to Точность расчета(округление до)
 * @property string $comment Комментарий
 * @property string $planing_date Планируемая дата
 * @property string $created_at
 * @property string $updated_at
 * @property integer $duration Срок изготовления, дни
 * @property string $payment_date Дата оплаты
 * @property integer $bend_num Число гибов
 * @property double $material_price Цена материала
 * @property double $cut_price Цена за резку
 * @property double $total_bend_price Сумма по гибке
 * @property double $total_welding_price Сумма по сварке
 * @property double $total_paint_price Сумма по покраске
 * @property double $min_cut_price Минимальная цена резки
 * @property double $min_bend_price Минимальная цена гибки
 * @property double $min_welding_price Минимальная цена сварки
 * @property double $min_painting_price Минимальная цена покраски
 * @property int $is_broken Брак
 * @property string $date_broken Дата брака
 * @property int $broken_user_id Менеджер, который допустил брак
 *
 * @property BendStatus $bendStatus
 * @property User $brokenUser
 * @property CustomerContact $customerContact
 * @property Customer $customer
 * @property CuttingStatus $cuttingStatus
 * @property User $manager
 * @property PaintStatus $paintStatus
 * @property OrderStatus $status
 * @property WeldingStatus $weldingStatus
 * @property OrderPart[] $orderParts
 */
class Order extends \yii\db\ActiveRecord
{
    /** @var string */
    public $files;

    const PAYMENT_STATUS_NOT_PAYED = 1;
    const PAYMENT_STATUS_PART_PAYED = 2;
    const PAYMENT_STATUS_PAYED = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'manager_id', 'status_id', 'cutting_status_id', 'bend_status_id', 'welding_status_id', 'paint_status_id', 'num_set', 'all_count', 'customer_contact_id', 'pay_status', 'pay_no_tax', 'show_discount', 'numbering', 'bend_num', 'broken_user_id', 'is_broken'], 'integer'],
            [['price', 'total_time', 'total_shift', 'price_painting', 'price_bend', 'cutting_discount', 'price_delivery_metal', 'round_to', 'material_price', 'cut_price', 'total_bend_price', 'total_welding_price', 'total_paint_price', 'min_cut_price', 'min_bend_price', 'min_welding_price', 'min_painting_price'], 'number'],
            [['planned_date', 'planing_date', 'created_at', 'updated_at', 'payment_date', 'date_broken'], 'safe'],
            [['comment', 'files'], 'string'],
            [['number'], 'string', 'max' => 255],
            [['bend_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BendStatus::className(), 'targetAttribute' => ['bend_status_id' => 'id']],
            [['customer_contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerContact::className(), 'targetAttribute' => ['customer_contact_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['cutting_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => CuttingStatus::className(), 'targetAttribute' => ['cutting_status_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_id' => 'id']],
            [['paint_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaintStatus::className(), 'targetAttribute' => ['paint_status_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['welding_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => WeldingStatus::className(), 'targetAttribute' => ['welding_status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя заказа',
            'files' => 'Детали',
            'number' => 'Номер',
            'customer_id' => 'Заказчик',
            'manager_id' => 'Менеджер',
            'status_id' => 'Статус заказа',
            'cutting_status_id' => 'Статус резки',
            'bend_status_id' => 'Статус гибки',
            'welding_status_id' => 'Статус сварки',
            'paint_status_id' => 'Статус покраски',
            'num_set' => 'Кол-во комплектов',
            'all_count' => 'Общее кол-во',
            'customer_contact_id' => 'Контактное лицо',
            'price' => 'Сумма заказа',
            'total_time' => 'Кол-во времени, час',
            'total_shift' => 'Кол-во смен',
            'pay_status' => 'Статус оплаты',
            'pay_no_tax' => 'Оплата без НДС',
            'planned_date' => 'Планируемая дата',
            'show_discount' => 'Показывать скидку',
            'bend_num' => 'Число гибов',
            'price_painting' => 'Цена покраски',
            'price_bend' => 'Цена гиба',
            'cutting_discount' => 'Скидка на резку',
            'numbering' => 'Оцифровка',
            'price_delivery_metal' => 'Цена доставки металла',
            'round_to' => 'Точность расчета(округление до)',
            'comment' => 'Комментарий',
            'planing_date' => 'Планируемая дата',
            'created_at' => 'Дата заказа',
            'updated_at' => 'Updated At',
            'duration' => 'Срок изготовления, дни',
            'payment_date' => 'Дата оплаты',
            'material_price' => 'Сумма за материал',
            'cut_price' => 'Сумма по резке',
            'total_bend_price' => 'Сумма по гибке',
            'total_welding_price' => 'Сумма по сварке',
            'min_cut_price' => 'Минимальная цена резки',
            'min_bend_price' => 'Минимальная цена гибки',
            'min_welding_price' => 'Минимальная цена сварки',
            'min_painting_price' => 'Минимальная цена покраски',
            'total_paint_price' => 'Сумма по покраске',
            'is_broken' => 'Брак',
            'date_broken' => 'Дата брака',
            'broken_user_id' => 'Менеджер, который допустил брак',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->number = self::getNewNumber();
            $this->name = $this->getNewName();
            $this->pay_status = self::PAYMENT_STATUS_NOT_PAYED;
            $this->num_set = 1;

            // Значения по умолчанию
            $this->numbering = 200;
            $this->cutting_discount = 0;
            $this->round_to = 4;
            $this->pay_no_tax = 0;
            $this->price_bend = 20;
            $this->price_delivery_metal = 8;

            $this->min_cut_price = 300;
            $this->min_bend_price = 300;
            $this->min_welding_price = 300;
            $this->min_painting_price = 300;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public static function paymentStatusLabels()
    {
        return [
            self::PAYMENT_STATUS_NOT_PAYED => 'Не оплачен',
            self::PAYMENT_STATUS_PART_PAYED => 'Оплачен частично',
            self::PAYMENT_STATUS_PAYED => 'Оплачен',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Yii::warning($changedAttributes);

        if(in_array('status_id', array_keys($changedAttributes))){
            $status = OrderStatus::findOne($this->status_id);
            if($status->pattern_email_id != null){
                $emailTemplate = PatternEmail::findOne($status->pattern_email_id);
                if($emailTemplate){
                    $content = $emailTemplate->text;
                    $content = TagHelper::handleModel($content, $this);

                    $after = "С ув. ".\Yii::$app->user->identity->name.' '.\Yii::$app->user->identity->phone.' '.\Yii::$app->user->identity->login;

                    $content = $content.' '.$after;

                    $customerContract = CustomerContact::findOne($this->customer_contact_id);

                    if($customerContract){
                        try {
//                            Yii::$app->mailer->compose()
//                                ->setSubject($emailTemplate->name)
//                                ->setHtmlBody($content)
//                                ->setFrom('laser.notify@mail.ru')
//                                ->setTo($customerContract->email)
//                                ->send();


                            $login = Yii::$app->user->identity->login;
                            $password = Yii::$app->user->identity->email_password;

                            \Yii::warning('before email sending');

                            if($login && $password){
                                $transport = (new Swift_SmtpTransport('smtp.mail.ru', 465, 'ssl'))
                                    ->setUsername($login)
                                    ->setPassword($password);

                                $mailer = new Swift_Mailer($transport);

                                $message = (new Swift_Message($emailTemplate->name))
                                    ->setFrom([$login => $login])
                                    ->setTo([$customerContract->email])
                                    ->setBody($content, 'text/html');

                                $result = $mailer->send($message);

                                \Yii::warning($result, 'result of mailing');
                            }

                        } catch (\Exception $e){
                            \Yii::warning($e->getMessage());
                        }
                    }
                }
            }
            if($status->pattern_sms_id != null){

                $smsTemplate = PatternSms::findOne($status->pattern_sms_id);
                if($smsTemplate){
                    $content = $smsTemplate->text;
                    $content = TagHelper::handleModel($content, $this);

                    $customerContract = CustomerContact::findOne($this->customer_contact_id);

                    if($customerContract){
                        $dataQuery = http_build_query([
                            'api_id' => 'F40B352B-CA02-5CDD-10D0-9B09FEC96FEA',
                            'to' => $customerContract->phone,
                            'msg' => $content,
                            'json' => '1',
                        ]);
                        $smsResponse = json_decode(file_get_contents("https://sms.ru/sms/send?{$dataQuery}"), true);
//                        VarDumper::dump($smsResponse, 'Response of sms sending service');
                    }
                }
            }
        }
    }

    /**
     * @return string
     */
    public static function getNewNumber()
    {
        $ordersCount = Order::find()->where(['>', 'created_at', date('Y-01-01 00:00:00')])->count();

        $count = $ordersCount + 1;

        $number = date('y');

        $zeroCount = 4 - strlen(strval($count));

        for ($i = 1; $i < $zeroCount; $i++)
        {
            $number .= '0';
        }

        $number .= $count;

        return $number;
    }

    /**
     * @return string
     */
    public function getNewName()
    {
        $name = "#{$this->number}_";

        if($this->manager_id){
            $name .= $this->manager->name;
        }
        if($this->customer_id){
            $name .= $this->customer->name;
        }
        if($this->customer_id){

        }



        return $name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBendStatus()
    {
        return $this->hasOne(BendStatus::className(), ['id' => 'bend_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerContact()
    {
        return $this->hasOne(CustomerContact::className(), ['id' => 'customer_contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuttingStatus()
    {
        return $this->hasOne(CuttingStatus::className(), ['id' => 'cutting_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaintStatus()
    {
        return $this->hasOne(PaintStatus::className(), ['id' => 'paint_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWeldingStatus()
    {
        return $this->hasOne(WeldingStatus::className(), ['id' => 'welding_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderParts()
    {
        return $this->hasMany(OrderPart::className(), ['order_id' => 'id']);
    }
}

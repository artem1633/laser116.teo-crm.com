<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CuttingParams]].
 *
 * @see CuttingParams
 */
class CuttingParamsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CuttingParams[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CuttingParams|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cutting_params".
 *
 * @property int $id
 * @property int $cutting_id Резка
 * @property int $thickness Толщина
 * @property int $price Цена
 *
 * @property Cutting $cutting
 */
class CuttingParams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cutting_params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cutting_id', 'thickness', 'price'], 'integer'],
            [['cutting_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cutting::className(), 'targetAttribute' => ['cutting_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cutting_id' => 'Резка',
            'thickness' => 'Толщина',
            'price' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCutting()
    {
        return $this->hasOne(Cutting::className(), ['id' => 'cutting_id']);
    }

    /**
     * {@inheritdoc}
     * @return CuttingParamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CuttingParamsQuery(get_called_class());
    }
}

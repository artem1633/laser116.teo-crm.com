<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Cutting]].
 *
 * @see Cutting
 */
class CuttingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Cutting[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Cutting|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provider".
 *
 * @property int $id
 * @property string $name Название
 * @property string $official_name Оф. наименование
 * @property string $email email
 * @property string $director Директор
 * @property string $post_address Почтовый адрес
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property string $r_s Р/С
 * @property string $k_s К/С
 * @property string $bic БИК
 *
 * @property ProviderContact[] $providerContacts
 */
class Provider extends \yii\db\ActiveRecord
{
    public $contacts;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'director', 'post_address', 'inn', 'kpp', 'r_s', 'k_s', 'bic', 'official_name'], 'string', 'max' => 255],
            [['contacts',], 'validateСontacts',],
        ];
    }
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        $contactAll = ProviderContact::find()->where(['provider_id' => $this->id,])->all();
        foreach ($contactAll as $item1) {
            $a = false;
            if ($this->contacts == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->contacts as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if ($this->contacts != null) {
            foreach ($this->contacts as $item) {
                $contact = ProviderContact::find()->where(['id' => $item['id']])->one();

                if (!$contact) {
                    (new ProviderContact([
                        'fio' => $item['fio'],
                        'position' => $item['position'],
                        'phone' => $item['phone'],
                        'mobile' => $item['mobile'],
                        'email' => $item['email'],
                        'provider_id' => $this->id,
                    ]))->save(false);
                }else{
                    $contact->fio = $item['fio'];
                    $contact->position = $item['position'];
                    $contact->phone = $item['phone'];
                    $contact->email = $item['email'];
                    $contact->mobile = $item['mobile'];
                    $contact->save(false);
                }
            }

        }
    }
    /**
     * @param $attribute
     */
    public function validateСontacts($attribute)
    {
        foreach($this->$attribute as $index => $row) {
            if ($row['fio'] == null) {
                $key = $attribute . '[' . $index . '][fio]';
                $this->addError($key, 'Обязательное для заполненния');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'email' => 'email',
            'official_name' => 'Оф. наименование',
            'address' => 'Адрес',
            'director' => 'Директор',
            'post_address' => 'Почтовый адрес',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'r_s' => 'Р/С',
            'k_s' => 'К/С',
            'bic' => 'БИК',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderContacts()
    {
        return $this->hasMany(ProviderContact::className(), ['provider_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ProviderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProviderQuery(get_called_class());
    }
}

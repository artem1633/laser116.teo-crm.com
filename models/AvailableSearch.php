<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Available;
use yii\helpers\ArrayHelper;

/**
 * AvailableSearch represents the model behind the search form about `app\models\Available`.
 */
class AvailableSearch extends Available
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'creator_id', 'storage_id', 'status_id', 'count', 'good_id', 'postavshik_id', 'number', 'received', 'provider_id', 'category_id'], 'integer'],
            [['price', 'price_shop'], 'number'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Available::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'creator_id' => $this->creator_id,
            'storage_id' => $this->storage_id,
            'status_id' => $this->status_id,
            'count' => $this->count,
            'price' => $this->price,
            'provider_id' => $this->provider_id,
            'price_shop' => $this->price_shop,
            'good_id' => $this->good_id,
            'category_id' => $this->category_id,
            'postavshik_id' => $this->postavshik_id,
            'received' => $this->received,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

    public function searchAccrued($params, $post)
    {
        $query = Available::find()->where([ 'status_id' => 3 ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'product_id' => $this->product_id,
            'creator_id' => $this->creator_id,
            //'storage_id' => $this->storage_id,
            'status_id' => $this->status_id,
            'count' => $this->count,
            'price' => $this->price,
            'provider_id' => $this->provider_id,
            'price_shop' => $this->price_shop,
            'good_id' => $this->good_id,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->andFilterWhere(['like', 'product_id', ArrayHelper::getValue($post, 'Available.tovar')]);
        $query->andFilterWhere(['like', 'storage_id', ArrayHelper::getValue($post, 'Available.storages')]);

        $query->andWhere(['received' => 1]);

        return $dataProvider;
    }

    public static function searchById($id)
    {
        $query = Available::find()->where(['id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);
        return $dataProvider;
    }

    public static function searchByNumber($number)
    {
        $query = Available::find()->where(['number' => $number]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);
        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_params".
 *
 * @property int $id
 * @property int $thickness Толщина
 * @property int $width Ширина
 * @property int $length Длина
 * @property int $price Цена
 * @property int $inset_price цена врезки
 * @property int $cut_price Цена резки
 * @property int $remainder Минимальный остаток
 * @property int $materials_id Материал
 *
 * @property Materials $materials
 */
class MaterialParams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['thickness', 'width', 'length', 'price', 'remainder','cut_price','inset_price','cut_speed',
                'inset_time', 'materials_id'], 'integer'],
            [['materials_id'], 'exist', 'skipOnError' => true, 'targetClass' => Materials::className(), 'targetAttribute' => ['materials_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'thickness' => 'Толщина',
            'width' => 'Ширина',
            'length' => 'Длина',
            'price' => 'Цена',
            'remainder' => 'Минимальный остаток',
            'materials_id' => 'Материал',
            'cut_price' => 'Цена резки',
            'cut_speed' => 'Скорость реза',
            'inset_time' => 'Время врезки',
            'inset_price' => 'Цена врезки'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasOne(Materials::className(), ['id' => 'materials_id']);
    }

    /**
     * {@inheritdoc}
     * @return MaterialParamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MaterialParamsQuery(get_called_class());
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Resource;
use yii\helpers\ArrayHelper;

/**
 * ResourceSearch represents the model behind the search form about `app\models\Resource`.
 */
class ResourceSearch extends Resource
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'creator_id', 'storage_id', 'status_id', 'count', 'good_id', 'postavshik_id', 'number'], 'integer'],
            [['price', 'price_shop'], 'number'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Resource::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'creator_id' => $this->creator_id,
            'storage_id' => $this->storage_id,
            'status_id' => $this->status_id,
            'count' => $this->count,
            'price' => $this->price,
            'price_shop' => $this->price_shop,
            'good_id' => $this->good_id,
            'postavshik_id' => $this->postavshik_id,
            'number' => $this->number,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

    public function searchRemainResource($params/*, $post*/)
    {
        $query = Resource::find()->where([ 'status_id' => 1, 'received' => true]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'creator_id' => $this->creator_id,
            'status_id' => $this->status_id,
            'count' => $this->count,
            'price' => $this->price,
            'price_shop' => $this->price_shop,
            'good_id' => $this->good_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        //$query->andFilterWhere(['like', 'product_id', $post['tovars'] ]);
        //$query->andFilterWhere(['like', 'storage_id', $post['storages'] ]);


        return $dataProvider;
    }

    public function searchWaitingResource($params, $post)
    {
        $query = Resource::find()->where(['status_id' => 3]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'product_id' => $this->product_id,
            'creator_id' => $this->creator_id,
            //'storage_id' => $this->storage_id,
            'status_id' => $this->status_id,
            'count' => $this->count,
            'price' => $this->price,
            'price_shop' => $this->price_shop,
            'good_id' => $this->good_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->andFilterWhere(['like', 'product_id', ArrayHelper::getValue($post, 'Resource.tovar')]);
        $query->andFilterWhere(['like', 'storage_id', ArrayHelper::getValue($post, 'Resource.storages')]);

        $query->andWhere(['is', 'received', null]);

        return $dataProvider;
    }

    public static function searchById($id)
    {
        $query = Resource::find()->where(['id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
        ]);
        return $dataProvider;
    }
}

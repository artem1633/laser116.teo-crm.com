<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cashbox".
 *
 * @property int $id
 * @property string $name Название
 */
class Cashbox extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cashbox';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }


    /**
     * {@inheritdoc}
     * @return CashboxQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CashboxQuery(get_called_class());
    }
}

<?php

namespace app\models\manual;

use Yii;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
use app\models\Available;
use app\models\Resource;

/**
 * This is the model class for table "suppliers".
 *
 * @property int $id
 * @property string $name
 */
class Suppliers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suppliers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['company_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'company_id' => 'Company ID',
        ];
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            $available = Available::find()->where(['postavshik_id' => $this->id])->all();
            foreach ($available as $value) {
               $value->delete();
            }

            $resource = Resource::find()->where(['postavshik_id' => $this->id])->all();
            foreach ($resource as $value) {
               $value->delete();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailables()
    {
        return $this->hasMany(Available::className(), ['postavshik_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['postavshik_id' => 'id']);
    }
}

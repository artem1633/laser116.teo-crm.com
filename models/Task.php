<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property string $name Описание задачи
 * @property int $executor_id Исполнитель
 * @property string $deadline Срок выполнения
 * @property int $priority Приоритет
 * @property string $comment Комментарии
 * @property string $created_at Дата создания
 * @property string $updated_at Дата изменения
 * @property int $is_question Вопрос
 * @property int $created_by Кто создал
 *
 * @property Order $order
 * @property User $createdBy
 * @property User $executor
 * @property TaskStatus $taskStatus
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'executor_id', 'priority','task_status_id', 'created_by', 'is_question'], 'integer'],
            [['deadline', 'created_at', 'updated_at'], 'safe'],
            [['comment'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['executor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['executor_id' => 'id']],
            [['task_status_id'], 'exist', 'skipOnEmpty' => true, 'targetClass' => TaskStatus::class, 'targetAttribute' => ['task_status_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'name' => 'Задача',
            'executor_id' => 'Исполнитель',
            'is_question' => 'Вопрос',
            'deadline' => 'Срок выполнения',
            'priority' => 'Приоритет',
            'comment' => 'Описание задачи',
            'task_status_id'=> 'Статус задачи',
            'created_at' => 'Дата и время создания',
            'updated_at' => 'Дата и время изменения',
            'created_by' => 'Кто создал',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'created_by',
                'value' => Yii::$app->user->id
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(User::class, ['id' => 'executor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskStatus()
    {
        return $this->hasOne(TaskStatus::class, ['id' => 'task_status_id']);
    }

    /**
     * {@inheritdoc}
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }
}

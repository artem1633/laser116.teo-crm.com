<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProviderContact]].
 *
 * @see ProviderContact
 */
class ProviderContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ProviderContact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ProviderContact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

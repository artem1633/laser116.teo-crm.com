<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "score".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property int $type Тип
 * @property double $amount Сумма
 * @property int $status Оплачен/не оплачен
 * @property string $inner_score Внутренней счет
 * @property int $customer_id Контрагент
 * @property string $one_s Связь с 1С
 * @property string $file Файл
 * @property string $created_at Дата и время
 * @property int $created_by Кто создал
 *
 * @property Payments[] $payments
 * @property User $createdBy
 * @property Customer $customer
 * @property Order $order
 */
class Score extends \yii\db\ActiveRecord
{
    const TYPE_SCORE = 1;
    const TYPE_REALIZATION = 2;

    /** @var UploadedFile */
    public $fileload;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'score';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'status', 'customer_id', 'type'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
            [['inner_score', 'one_s', 'file'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['fileload'], 'file'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $file = UploadedFile::getInstance($this, 'fileload');
        \Yii::warning($file);
        if($file != null){
            $this->file = UploadedFile::getInstance($this, 'fileload');

            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $name = Yii::$app->security->generateRandomString();
            $path = "uploads/{$name}.{$this->file->extension}";
            $this->file->saveAs($path);
            $this->file = '/'.$path;
        }

        if($this->isNewRecord){
            $this->created_by = Yii::$app->user->getId();
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'amount' => 'Сумма',
            'status' => 'Оплачен',
            'inner_score' => 'Внутренней счет',
            'customer_id' => 'Контрагент',
            'one_s' => 'Связь с 1С',
            'type' => 'Тип',
            'file' => 'Файл',
            'fileload' => 'Файл',
            'created_at' => 'Дата и время',
            'created_by' => 'Кто создал',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_SCORE => 'Счет',
            self::TYPE_REALIZATION => 'Реализация',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['score_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_part".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property string $path_file Путь к файлу дизайна
 * @property int $count Количество
 * @property double $cut_price Цена за резку
 * @property double $cut_price_without_material Цена резки без учета материала. Расчет резки
 * @property double $cut_price_for_one_detail Цена резки за одну деталь. Расчет резки
 * @property double $cut_thickness_metal Толщина материала. Расчет резки
 * @property string $cut_discount Выбор скидки. Расчет резки
 * @property int $cut_material_id Материал. Расчет резки
 * @property int $cut_num Количество. Расчет резки
 * @property double $cut_length_each Длинна каждого. Расчет резки
 * @property double $cut_time Время резки. Расчет резки
 * @property int $cut_incut Кол-во врезок. Расчет резки
 * @property double $incut_price_per_meter Цена за метр врезки
 * @property double $incut_total_price Общая цена врезки
 * @property double $welding_price Цена сварки
 * @property int $bend_num Кол-во гибов
 * @property double $detail_area Площадь детали
 * @property double $digitization_price Цена оцифровки
 * @property double $material_length Длина материала
 * @property double $material_width Ширина материала
 * @property int $material_owner Владелец материала
 * @property double $material_area Площадь материала
 * @property double $material_area_sum Сумма площади материала
 * @property double $material_ratio Коофициент
 * @property double $material_sheet_price Цена одного листа материала
 * @property string $material_sheet_size Размеры листа материла
 * @property double $material_weight Вес листа материала
 * @property double $material_price Цена материала
 * @property double $paint_price Цена покраски
 * @property double $additional_word_price1 Стоимость дополнительных работ 1
 * @property double $additional_word_price2 Стоимость дополнительных работ 2
 * @property double $total_price Общая сумма по позиции
 * @property double $welding_sum Сумма сварки
 * @property double $material_ratio_percent Коэф (%)
 * @property string $created_at
 * @property string $updated_at
 * @property int $mine Чей материал
 *
 * @property Materials $cutMaterial
 * @property Order $order
 */
class OrderPart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_part';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'count', 'cut_material_id', 'cut_num', 'cut_incut', 'bend_num', 'material_owner', 'mine'], 'integer'],
            [['path_file', 'cut_discount', 'material_sheet_size'], 'string'],
            [['cut_price_without_material', 'paint_price', 'additional_word_price1', 'additional_word_price2', 'welding_sum', 'cut_price_for_one_detail', 'cut_thickness_metal', 'cut_length_each', 'cut_time', 'incut_price_per_meter', 'incut_total_price', 'welding_price', 'detail_area', 'digitization_price', 'material_length', 'material_width', 'material_area', 'material_area_sum', 'material_ratio', 'material_sheet_price', 'material_weight', 'material_price', 'total_price', 'cut_price', 'bend_price', 'material_ratio_percent', 'material_weight_painted', 'total_painted', 'material_weight_cutted', 'total_cutted', 'material_weight_bended', 'total_bended', 'material_weight_welded', 'total_welded', 'users_cutted', 'users_bended'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['cut_material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Materials::className(), 'targetAttribute' => ['cut_material_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'path_file' => 'Путь к файлу дизайна',
            'count' => 'Количество',
            'cut_price' => 'Цена за резку',
            'cut_price_without_material' => 'Цена резки без учета материала. Расчет резки',
            'cut_price_for_one_detail' => 'Цена резки за одну деталь. Расчет резки',
            'cut_thickness_metal' => 'Толщина материала. Расчет резки',
            'cut_discount' => 'Выбор скидки. Расчет резки',
            'cut_material_id' => 'Материал. Расчет резки',
            'cut_num' => 'Количество. Расчет резки',
            'cut_length_each' => 'Длинна каждого. Расчет резки',
            'cut_time' => 'Время резки. Расчет резки',
            'cut_incut' => 'Кол-во врезок. Расчет резки',
            'incut_price_per_meter' => 'Цена за метр врезки',
            'incut_total_price' => 'Общая цена врезки',
            'welding_price' => 'Цена сварки',
            'bend_num' => 'Кол-во гибов',
            'bend_price' => 'Цена гибки',
            'detail_area' => 'Площадь детали',
            'digitization_price' => 'Цена оцифровки',
            'material_length' => 'Длина материала',
            'material_width' => 'Ширина материала',
            'material_owner' => 'Владелец материала',
            'material_area' => 'Площадь материала',
            'material_area_sum' => 'Сумма площади материала',
            'material_ratio' => 'Коофициент',
            'material_sheet_price' => 'Цена одного листа материала',
            'material_sheet_size' => 'Размеры листа материла',
            'material_weight' => 'Вес листа материала',
            'material_price' => 'Цена материала',
            'total_price' => 'Общая сумма по позиции',
            'material_ratio_percent' => 'Коэф. (%)',
            'paint_price' => 'Цена кораски',
            'additional_word_price1' => 'Цена дополнительных работ 1',
            'additional_word_price2' => 'Цена дополнительных работ 2',
            'welding_sum' => 'Сумма сварки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'mine' => 'Чей материал',
            'material_weight_painted' => 'Вес покрашенных деталей',
            'total_painted' => 'Всего покрашенных деталей',
            'material_cutted_cutted' => 'Вес нарезанных деталей',
            'total_cutted' => 'Всего нарезано деталей',
            'users_cutted' => 'Пользователи нарезали',
            'material_bended_bended' => 'Вес согнутых деталей',
            'total_bended' => 'Всего согнутых деталей',
            'users_bended' => 'Пользователи согнули',
            'material_welded_welded' => 'Вес сваренных деталей',
            'total_welded' => 'Всего сваренных деталей',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCutMaterial()
    {
        return $this->hasOne(Materials::className(), ['id' => 'cut_material_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pattern_email".
 *
 * @property int $id
 * @property string $name Название
 * @property string $text Текст
 *
 * @property OrderStatus[] $orderStatuses
 */
class PatternEmail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pattern_email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatuses()
    {
        return $this->hasMany(OrderStatus::className(), ['pattern_email_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PatternEmailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatternEmailQuery(get_called_class());
    }
}

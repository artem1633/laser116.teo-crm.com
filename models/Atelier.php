<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;
use app\models\manual\Cashbox;

/**
 * This is the model class for table "atelier".
 *
 * @property integer $id
 * @property string $name
 * @property string $director
 * @property string $inn
 * @property string $kpp
 * @property string $ogrn
 * @property string $bank
 * @property string $kor_schot
 * @property string $checking_accaunt
 * @property string $okpo
 * @property string $bik
 * @property string $address
 * @property string $telephone
 * @property string $email
 *
 * @property Users[] $users
 */
class Atelier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'atelier';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['company_id'], 'integer'],
            //[['name'], 'unique'],
            [['email'], 'email'],
            [['name', 'director', 'inn', 'kpp', 'ogrn', 'bank', 'kor_schot', 'checking_accaunt', 'okpo', 'bik', 'address', 'telephone', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название юр. лица',
            'director' => 'Директор',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'ogrn' => 'ОГРН',
            'bank' => 'Название банка',
            'kor_schot' => 'Корр. счет',
            'checking_accaunt' => 'Расчетный счет',
            'okpo' => 'ОКПО',
            'bik' => 'БИК',
            'address' => 'Адрес',
            'telephone' => 'Телефон',
            'email' => 'Email',
        ];
    }

    public function beforeDelete()
    {
//        $cashbox = Cashbox::find()->where(['atelier_id' => $this->id])->all();
//        foreach ($cashbox as $cash)
//        {
//            $cash->delete();
//        }

//        $clients = Clients::find()->where(['atelier_id' => $this->id])->all();
//        foreach ($clients as $client)
//        {
//            $client->delete();
//        }

//        $orders = Orders::find()->where(['atelier_id' => $this->id])->all();
//        foreach ($orders as $order)
//        {
//            $order->delete();
//        }

        $storages = Storage::find()->where(['atelier_id' => $this->id])->all();
        foreach ($storages as $storage)
        {
            $storage->delete();
        }

//        $users = Users::find()->where(['atelier_id' => $this->id])->all();
//        foreach ($users as $user)
//        {
//            $user->delete();
//        }

        return parent::beforeDelete();
    }

//   /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getCompany()
//    {
//        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
//    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getCashboxes()
//    {
//        return $this->hasMany(Cashbox::className(), ['atelier_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getClients()
//    {
//        return $this->hasMany(Clients::className(), ['atelier_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getOrders()
//    {
//        return $this->hasMany(Orders::className(), ['atelier_id' => 'id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany(Storage::className(), ['atelier_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['atelier_id' => 'id']);
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PatternEmail]].
 *
 * @see PatternEmail
 */
class PatternEmailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PatternEmail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PatternEmail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PatternSms]].
 *
 * @see PatternSms
 */
class PatternSmsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PatternSms[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PatternSms|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

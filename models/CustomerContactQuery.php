<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CustomerContact]].
 *
 * @see CustomerContact
 */
class CustomerContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CustomerContact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CustomerContact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

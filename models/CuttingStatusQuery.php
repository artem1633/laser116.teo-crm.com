<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CuttingStatus]].
 *
 * @see CuttingStatus
 */
class CuttingStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CuttingStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CuttingStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

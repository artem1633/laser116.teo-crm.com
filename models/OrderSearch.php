<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'num_set', 'all_count', 'customer_contact_id', 'pay_status', 'pay_no_tax', 'show_discount', 'numbering', 'duration', 'bend_num'], 'integer'],
            [['name', 'number', 'planned_date', 'comment', 'planing_date', 'customer_id', 'manager_id', 'created_at', 'updated_at', 'payment_date', 'status_id', 'cutting_status_id', 'bend_status_id', 'welding_status_id', 'paint_status_id'], 'safe'],
            [['price', 'total_time', 'total_shift', 'price_painting', 'price_bend', 'cutting_discount', 'price_delivery_metal', 'round_to', 'material_price', 'cut_price', 'total_bend_price', 'total_welding_price', 'total_paint_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->joinWith(['cuttingStatus', 'status', 'bendStatus', 'paintStatus', 'weldingStatus']);

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'manager_id' => $this->manager_id,
            'status_id' => $this->status_id,
            'cutting_status_id' => $this->cutting_status_id,
            'bend_status_id' => $this->bend_status_id,
            'welding_status_id' => $this->welding_status_id,
            'paint_status_id' => $this->paint_status_id,
            'num_set' => $this->num_set,
            'all_count' => $this->all_count,
            'customer_contact_id' => $this->customer_contact_id,
            'price' => $this->price,
            'total_time' => $this->total_time,
            'total_shift' => $this->total_shift,
            'pay_status' => $this->pay_status,
            'pay_no_tax' => $this->pay_no_tax,
            'planned_date' => $this->planned_date,
            'show_discount' => $this->show_discount,
            'price_painting' => $this->price_painting,
            'price_bend' => $this->price_bend,
            'total_bend_price' => $this->total_bend_price,
            'total_welding_price' => $this->total_welding_price,
            'total_paint_price' => $this->total_paint_price,
            'cutting_discount' => $this->cutting_discount,
            'numbering' => $this->numbering,
            'price_delivery_metal' => $this->price_delivery_metal,
            'round_to' => $this->round_to,
            'material_price' => $this->material_price,
            'cut_price' => $this->cut_price,
            'planing_date' => $this->planing_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'bend_num', $this->bend_num])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LegalEntities]].
 *
 * @see LegalEntities
 */
class LegalEntitiesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LegalEntities[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LegalEntities|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

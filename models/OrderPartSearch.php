<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrderPart;

/**
 * OrderPartSearch represents the model behind the search form about `app\models\OrderPart`.
 */
class OrderPartSearch extends OrderPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'count', 'cut_material_id', 'cut_num', 'cut_incut', 'bend_num', 'material_owner'], 'integer'],
            [['path_file', 'cut_discount', 'material_sheet_size', 'created_at', 'updated_at'], 'safe'],
            [['cut_price_without_material', 'cut_price_for_one_detail', 'cut_thickness_metal', 'cut_length_each', 'cut_time', 'incut_price_per_meter', 'incut_total_price', 'welding_price', 'detail_area', 'digitization_price', 'material_length', 'material_width', 'material_area', 'material_area_sum', 'material_ratio', 'material_sheet_price', 'material_weight', 'material_price', 'total_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderPart::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'count' => $this->count,
            'cut_price_without_material' => $this->cut_price_without_material,
            'cut_price_for_one_detail' => $this->cut_price_for_one_detail,
            'cut_thickness_metal' => $this->cut_thickness_metal,
            'cut_material_id' => $this->cut_material_id,
            'cut_num' => $this->cut_num,
            'cut_length_each' => $this->cut_length_each,
            'cut_time' => $this->cut_time,
            'cut_incut' => $this->cut_incut,
            'incut_price_per_meter' => $this->incut_price_per_meter,
            'incut_total_price' => $this->incut_total_price,
            'welding_price' => $this->welding_price,
            'bend_num' => $this->bend_num,
            'detail_area' => $this->detail_area,
            'digitization_price' => $this->digitization_price,
            'material_length' => $this->material_length,
            'material_width' => $this->material_width,
            'material_owner' => $this->material_owner,
            'material_area' => $this->material_area,
            'material_area_sum' => $this->material_area_sum,
            'material_ratio' => $this->material_ratio,
            'material_sheet_price' => $this->material_sheet_price,
            'material_weight' => $this->material_weight,
            'material_price' => $this->material_price,
            'total_price' => $this->total_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'path_file', $this->path_file])
            ->andFilterWhere(['like', 'cut_discount', $this->cut_discount])
            ->andFilterWhere(['like', 'material_sheet_size', $this->material_sheet_size]);

        return $dataProvider;
    }
}

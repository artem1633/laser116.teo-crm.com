<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "bend_price".
 *
 * @property int $id
 * @property int $count Количество
 * @property int $price Цена
 * @property int $material_id Материал
 */
class BendPrice extends \yii\db\ActiveRecord
{
    public $materials;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bend_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count', 'price'], 'integer'],
            [['materials'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'count' => 'Количество до',
            'price' => 'Цена',
            'materials' => 'Материалы'
//            'material_id' => 'Материал'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->materials){
            $bankAll = BendPriceMaterials::find()->where(['bend_price_id' => $this->id,])->all();
            foreach ($bankAll as $item1) {
                $delete = true;

                foreach ($this->materials as $item2){
                    if($item2 == $item1->materials_id){
                        $delete = false;
                    }
                }

                if($delete){
                    $item1->delete();
                }
            }

            if($this->materials != null){
                foreach ($this->materials as $item) {
                    $bank = BendPriceMaterials::find()->where(['id' => $item])->one();
                    if (!$bank) {
                        (new BendPriceMaterials([
                            'bend_price_id' => $this->id,
                            'materials_id' => $item,
                        ]))->save(false);
                    } else {
                        $bank->materials_id = $item;
                        $bank->save(false);
                    }
                }
            }
        } else {
            BendPriceMaterials::deleteAll(['bend_price_id' => $this->id]);
        }
    }

    /**
     * {@inheritdoc}
     * @return BendPriceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BendPriceQuery(get_called_class());
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getMaterial()
//    {
//        return $this->hasOne(Materials::className(), ['id' => 'material_id']);
//    }
}

<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PatternDocument]].
 *
 * @see PatternDocument
 */
class PatternDocumentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PatternDocument[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PatternDocument|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

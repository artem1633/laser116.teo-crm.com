<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Cashbox]].
 *
 * @see Cashbox
 */
class CashboxQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Cashbox[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Cashbox|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

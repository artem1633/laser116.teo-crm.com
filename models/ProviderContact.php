<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provider_contact".
 *
 * @property int $id
 * @property string $fio ФИО контактного лица
 * @property string $position Должность
 * @property string $mobile Мобильный телефон
 * @property string $phone Служебный телефон
 * @property string $email email
 * @property int $provider_id Контрагент
 *
 * @property Provider $provider
 */
class ProviderContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provider_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provider_id'], 'integer'],
            [['fio', 'position', 'mobile', 'phone', 'email'], 'string', 'max' => 255],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provider::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО контактного лица',
            'position' => 'Должность',
            'mobile' => 'Мобильный телефон',
            'phone' => 'Служебный телефон',
            'email' => 'email',
            'provider_id' => 'Контрагент',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
    }

    /**
     * {@inheritdoc}
     * @return ProviderContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProviderContactQuery(get_called_class());
    }
}

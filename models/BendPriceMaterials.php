<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bend_price_materials".
 *
 * @property int $id
 * @property int $bend_price_id
 * @property int $materials_id
 *
 * @property BendPrice $bendPrice
 * @property Materials $materials
 */
class BendPriceMaterials extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bend_price_materials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bend_price_id', 'materials_id'], 'integer'],
            [['bend_price_id'], 'exist', 'skipOnError' => true, 'targetClass' => BendPrice::className(), 'targetAttribute' => ['bend_price_id' => 'id']],
            [['materials_id'], 'exist', 'skipOnError' => true, 'targetClass' => Materials::className(), 'targetAttribute' => ['materials_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bend_price_id' => 'Bend Price ID',
            'materials_id' => 'Materials ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBendPrice()
    {
        return $this->hasOne(BendPrice::className(), ['id' => 'bend_price_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasOne(Materials::className(), ['id' => 'materials_id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_status".
 *
 * @property int $id
 * @property string $name Название
 * @property string $color Цвет
 * @property int $pattern_sms_id Шаблон СМС
 * @property int $pattern_email_id Шаблон email
 * @property int $in_work В работе
 *
 * @property PatternEmail $patternEmail
 * @property PatternSms $patternSms
 */
class OrderStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pattern_sms_id', 'pattern_email_id', 'in_work'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
            [['pattern_email_id'], 'exist', 'skipOnError' => true, 'targetClass' => PatternEmail::className(), 'targetAttribute' => ['pattern_email_id' => 'id']],
            [['pattern_sms_id'], 'exist', 'skipOnError' => true, 'targetClass' => PatternSms::className(), 'targetAttribute' => ['pattern_sms_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'color' => 'Цвет',
            'pattern_sms_id' => 'Шаблон СМС',
            'pattern_email_id' => 'Шаблон email',
            'in_work' => 'В работе',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatternEmail()
    {
        return $this->hasOne(PatternEmail::className(), ['id' => 'pattern_email_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatternSms()
    {
        return $this->hasOne(PatternSms::className(), ['id' => 'pattern_sms_id']);
    }

    /**
     * {@inheritdoc}
     * @return OrderStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderStatusQuery(get_called_class());
    }
}

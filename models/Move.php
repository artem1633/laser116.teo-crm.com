<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use app\base\AppActiveQuery;
use app\models\Companies;

/**
 * This is the model class for table "move".
 *
 * @property int $id
 * @property string $table
 * @property int $storage_form
 * @property int $storage_to
 * @property int $old_count
 * @property int $sending_count
 * @property string $data
 */
class Move extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'move';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_form', 'storage_to', 'old_count', 'sending_count', 'part_id'], 'integer'],
            [['data'], 'safe'],
            [['table'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table' => 'Table',
            'storage_form' => 'Storage Form',
            'storage_to' => 'Storage To',
            'old_count' => 'Old Count',
            'sending_count' => 'Sending Count',
            'data' => 'Data',
            'part_id' => 'Товар',
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "commercial_offer".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property int $customer_id Контрагент
 * @property string $path Файл
 * @property double $sum Сумма
 * @property string $created_at
 *
 * @property Customer $customer
 * @property Order $order
 */
class CommercialOffer extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commercial_offer';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'sum'], 'required'],
            [['order_id', 'customer_id'], 'integer'],
            [['sum'], 'number'],
            [['created_at'], 'safe'],
            [['path'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['file'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'customer_id' => 'Контрагент',
            'path' => 'Файл',
            'sum' => 'Сумма',
            'created_at' => 'Дата и время',
            'file' => 'Файл',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->file){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = 'uploads/'.Yii::$app->security->generateRandomString().'.'.$this->file->extension;
            $this->path = $path;

            $this->file->saveAs($path);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}

<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Class DocumentForm
 * @package app\models\forms
 */
class DocumentForm extends Model
{
    const TYPE_PRINT = 0;
    const TYPE_MAIL = 1;

    /** @var int */
    public $templateId;

    /** @var string */
    public $name;

    /** @var string */
    public $content;

    /** @var int */
    public $type;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['templateId', 'name', 'content', 'type'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'templateId' => 'Шаблон',
            'name' => 'Наименование',
            'content' => 'Контент',
            'type' => 'Тип'
        ];
    }

    /**
     * @return bool
     */
    public function generate()
    {
        if($this->validate()){
            return true;
        }

        return false;
    }
}
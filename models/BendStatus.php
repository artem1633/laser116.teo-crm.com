<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bend_status".
 *
 * @property int $id
 * @property string $name Название
 * @property string $color Цвет
 * @property int $in_work В работе
 */
class BendStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bend_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'string', 'max' => 255],
            [['in_work'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'color' => 'Цвет',
            'in_work' => 'В работе',
        ];
    }

    /**
     * {@inheritdoc}
     * @return BendStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BendStatusQuery(get_called_class());
    }
}

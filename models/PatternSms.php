<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pattern_sms".
 *
 * @property int $id
 * @property string $name Название
 * @property string $text Текст
 *
 * @property OrderStatus[] $orderStatuses
 */
class PatternSms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pattern_sms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatuses()
    {
        return $this->hasMany(OrderStatus::className(), ['pattern_sms_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PatternSmsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatternSmsQuery(get_called_class());
    }
}

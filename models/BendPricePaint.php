<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bend_price_paint".
 *
 * @property int $id
 * @property int $count Количество
 * @property int $price Цена
 */
class BendPricePaint extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bend_price_paint';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count', 'price'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'count' => 'Площадь до',
            'price' => 'Цена',
        ];
    }
}

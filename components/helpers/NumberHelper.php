<?php

namespace app\components\helpers;

/**
 * Class NumberHelper
 * @package app\components\helpers
 */
class NumberHelper
{
    public static function roundUp( $value, $precision ){
        $pow = pow ( 10, $precision );
        return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow;
    }
}
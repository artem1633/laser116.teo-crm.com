<?php

namespace app\components\helpers;

/**
 * Class DateHelper
 * @package app\helpers
 */
class DateHelper
{
    public static function getStartOfWeekDate($date = null)
    {
        if ($date instanceof \DateTime) {
            $date = clone $date;
        } else if (!$date) {
            $date = new \DateTime();
        } else {
            $date = new \DateTime($date);
        }

        $date->setTime(0, 0, 0);

        if ($date->format('N') == 1) {
            return $date;
        } else {
            return $date->modify('last monday');
        }
    }

    public static function getEndOfWeekDate($date = null)
    {
        if ($date instanceof \DateTime) {
            $date = clone $date;
        } else if (!$date) {
            $date = self::getStartOfWeekDate();
        } else {
            $date = self::getStartOfWeekDate($date);
        }

        $date->setTime(0, 0, 0);



        return $date->modify('this week +6 days');
    }
}
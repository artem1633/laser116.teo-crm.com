<?php

namespace app\components;


use app\components\helpers\NumberHelper;
use app\controllers\BendPricePaintController;
use app\models\BendPricePaint;
use app\models\MaterialParams;
use app\models\Materials;
use app\models\Order;
use app\models\OrderPart;
use app\models\Settings;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class OrderPartFieldCalc
 * @package app\components
 */
class OrderPartFieldCalc extends Component
{
    /** @var OrderPart */
    public $model;

    /** @var array */
    public $calcAttributes = [
        'material_sheet_size',
        'material_sheet_price',
        'material_width',
        'material_weight',
        'material_area_sum',
        'incut_price_per_meter',
        'incut_total_price',
        'material_ratio',
        'material_price',
        'cut_price',
        'cut_time',
        'bend_price',
        'total_price',
        'welding_sum',
        'paint_price',
        'material_ratio_percent',
        'cut_price_for_one_detail',
        // 'total_painted',
        'material_weight_painted',
        'material_weight_cutted',
        'material_weight_bended',
        'material_weight_welded',
        // 'total_cutted',
    ];

    public $arrayAttributes = [
        'users_cutted',
        'users_bended',
    ];

    /** @var array  */
    public $dependAttributes = [
        'cut_thickness_metal'
    ];

    /** @var array */
    public $totalValues = [
        'total_cut_num',
        'total_cut_length_each',
        'total_cut_incut',
        'total_welding_price',
        'total_bend_num',
        'total_detail_area',
        'total_digitization_price',
        'total_total_price',
    ];


    /**
     * @return array
     */
    public function calc($calcOrder = true)
    {
        $resultAttr = ['data' => [], 'depend' => [], 'total' => []];

        /** @var MaterialParams $materialParam */
        $materialParam = null;
        if($this->model->cut_thickness_metal){
            $materialParam = MaterialParams::findOne($this->model->cut_thickness_metal);
        }

        /** @var Materials $material */
        $material = null;
        if($this->model->cut_material_id){
            $material = Materials::findOne($this->model->cut_material_id);
        }

        $parts = OrderPart::find()->where(['order_id' => $this->model->order_id])->groupBy(['cut_material_id', 'cut_thickness_metal'])->all();
        $totalMaterialLength = 0;
        $totalMaterialWidth = 0;
        $totalPartMaterialLength = 0;
        $totalPartMaterialWidth = 0;
//        $total =

        $allMaterialWeight = 0;

        foreach ($parts as $part)
        {
            $orderPartsTwo = OrderPart::find()->where(['order_id' => $this->model->order_id, 'cut_material_id' => $part->cut_material_id, 'cut_thickness_metal' => $part->cut_thickness_metal])->all();
            $partMaterial = MaterialParams::findOne($part->cut_thickness_metal);
            if($partMaterial == null){
                continue;
            }
            foreach ($orderPartsTwo as $orderpart)
            {
                $totalMaterialLength += $part->material_length;
                $totalMaterialWidth += $part->material_width;
                $totalPartMaterialLength += $partMaterial->length;
                $totalPartMaterialWidth += $partMaterial->width;
            }
            $allMaterialWeight = round($part->material_length * $part->material_width / 1000000 * $partMaterial->materials->density, 1);
        }

//        echo $allMaterialWeight;
//        exit;

//        $this->model->order->numbering = $this->getNumbering();

//        \Yii::warning($this->model->order->numbering, "Numbering");

        foreach ($this->calcAttributes as $attr)
        {
            try {
                $value = '';

                if($attr == 'material_sheet_size'){
                    if($materialParam){
                        $value = "{$materialParam->length}x{$materialParam->width}";
                    }
                } else if($attr == 'material_sheet_price'){
                    if($materialParam){
                        $value = $materialParam->price;
                    }
                } else if($attr == 'material_width'){
                    if($materialParam){
                        $value = $materialParam->width;
                    }
                } else if($attr == 'material_weight'){
                    if($material && $materialParam){
                        $value = round($material->density * $materialParam->thickness * $this->model->material_area / 1000000, 1);
                    }
                } else if($attr == 'material_area_sum'){
                    $value = $this->model->material_area * $this->model->cut_num * 1000;
                } else if($attr == 'incut_price_per_meter'){
                    if($materialParam){
                        $value = $materialParam->cut_price;
                    }
                } else if($attr == 'incut_total_price'){
                    if($materialParam){
                        $value = $materialParam->inset_price;
                    }
                } else if ($attr == 'material_ratio') {

//                $value = ($totalMaterialLength * $totalMaterialWidth) / ($totalPartMaterialLength * $totalPartMaterialWidth);
                    if($materialParam && (floatval($this->model->material_length) * floatval($this->model->material_width)) > 0){
                        $value = round(($materialParam->length * $materialParam->width) / ($this->model->material_length * $this->model->material_width), 1);
                    }
                }else if($attr == 'material_price'){
                    if($material && $materialParam){
//                    $parts = OrderPart::find()->where(['order_id' => $this->model->order_id])->groupBy(['cut_material_id', 'cut_thickness_metal'])->all();
//                    array_walk($parts, function($model) use($partsData){
//                        foreach($partsData as $data){
//                            if($data->cut_material_id == $model->cut_material_id && $data->cut_thickness_metal == $model->cut_thickness_metal){
//                                continue;
//                            }
//                        }
//                        $partsData[] = $model;
//                    });

                        $partMaterial = MaterialParams::findOne($this->model->cut_thickness_metal);

                        $oneMaterialWeight = round(floatval($this->model->material_length) * floatval($this->model->material_width) / 1000000000 * $partMaterial->materials->density, 1);


//                    $oneMaterialWeight = 0;
//
//                    if($material && $materialParam){
//                        $oneMaterialWeight = round($material->density * $materialParam->thickness * $this->model->material_area / 1000000, 1);
//                    }


                        if($this->model->material_ratio > 0){
                            $value = round($this->model->material_sheet_price / $this->model->material_ratio);
                        }

//                    \Yii::warning(ArrayHelper::map($parts, 'cut_material_id', 'cut_thickness_metal'));
//                    $value =
                    }
                }  else if($attr == 'cut_time'){

                    $partMaterial = MaterialParams::findOne($this->model->cut_thickness_metal);

                    $value = round((($this->model->cut_length_each * $this->model->cut_num / $partMaterial->cut_speed / 60) + ($this->model->cut_incut * $this->model->cut_num * $partMaterial->inset_time / 60  / 60 )) * $this->model->order->num_set, 2);
                } else if($attr == 'cut_price'){ // Цена резки

                    $commonCutNumFirst = OrderPart::find()->where(['order_id' => $this->model->order_id])->sum('cut_num') * $this->model->order->num_set;



//                $value =
//                    (408.5 * 1 +
//                    64.51612903) +
//                    0.1 * 2400 / (45 / 20);
//
//                $value =
//                    $this->model->cut_length_each * $this->model->incut_price_per_meter * $this->model->cut_num * $this->model->order->num_set +
//                    $this->model->cut_incut * $this->model->incut_total_price * $this->model->cut_num * $this->model->order->num_set * 1 +
//                    $this->model->order->numbering / $commonCutNum * $this->model->cut_num * $this->model->order->num_set +
//                    0.1 * $this->model->material_price / (45 / 20);

                    $commonCutNum = OrderPart::find()->where(['order_id' => $this->model->order_id,'cut_material_id' => $this->model->cut_material_id, 'cut_thickness_metal' => $this->model->cut_thickness_metal])->sum('cut_num');




//                if($this->model->material_area != 0){
                    if($this->model->cut_num > 0){
                        if($commonCutNumFirst > 0 && ($commonCutNum / $this->model->cut_num) > 0){

                            $materialParam = MaterialParams::findOne($this->model->cut_thickness_metal);
                            if($materialParam) {
                                $material = Materials::findOne($materialParam->materials_id);

                                if ($material) {
                                    if ($material->additional_coefficient == 1) {

                                        $materialPrices = $this->getMaterialPrice();

//                                        $partsPrices["{$part->cut_material_id}_{$part->cut_thickness_metal}"] = [
//                                            'material_price' => $materialUniquePrice,
//                                        ];

                                        $materialPrice = $this->model->material_price;

                                        if(isset($materialPrices["{$this->model->cut_material_id}_{$this->model->cut_thickness_metal}"])){
                                            $materialPrice = $materialPrices["{$this->model->cut_material_id}_{$this->model->cut_thickness_metal}"];
                                            $materialPrice = $materialPrice['material_price'];
                                            \Yii::warning($materialPrice,'MATERIAL_PRICE');
                                        }

                                        $value = round(($this->model->cut_length_each * $this->model->incut_price_per_meter * $this->model->cut_num * $this->model->order->num_set +
                                                $this->model->cut_incut * $this->model->incut_total_price * $this->model->cut_num * $this->model->order->num_set) * (1 - $this->model->order->cutting_discount) +
                                            ($this->model->order->numbering / $commonCutNumFirst) * $this->model->cut_num * $this->model->order->num_set + 0.1 *
                                            (floatval($materialPrice) / ($commonCutNum / $this->model->cut_num)), 2);

                                        \Yii::warning("({$this->model->cut_length_each} * {$this->model->incut_price_per_meter} * {$this->model->cut_num} * {$this->model->order->num_set} +
                                    {$this->model->cut_incut} * {$this->model->incut_total_price} * {$this->model->cut_num} * {$this->model->order->num_set}) * (1 - {$this->model->order->cutting_discount})+
                                    {$this->model->order->numbering} / {$commonCutNumFirst} * {$this->model->cut_num} * {$this->model->order->num_set} + 0.1 *
                                    ( {$materialPrice} / ({$commonCutNum} / {$this->model->cut_num}))", 'cut_price');
                                    } else {

                                        $value =
                                            round(($this->model->cut_length_each * $this->model->incut_price_per_meter * $this->model->cut_num * $this->model->order->num_set +
                                                    $this->model->cut_incut * $this->model->incut_total_price * $this->model->cut_num * $this->model->order->num_set) * (1 - $this->model->order->cutting_discount) +
                                                ($this->model->order->numbering / $commonCutNumFirst) * $this->model->cut_num * $this->model->order->num_set, 2);

                                        \Yii::warning("({$this->model->cut_length_each} * {$this->model->incut_price_per_meter} * {$this->model->cut_num} * {$this->model->order->num_set} +
                                    {$this->model->cut_incut} * {$this->model->incut_total_price} * {$this->model->cut_num} * {$this->model->order->num_set}) * (1 - {$this->model->order->cutting_discount})+
                                    ({$this->model->order->numbering} / {$commonCutNumFirst}) * {$this->model->cut_num} * {$this->model->order->num_set}", 'cut_price');


                                    }
                                }
                            }
                        }

                        if($this->model->digitization_price){
                            $value = $value + $this->model->digitization_price;
                        }
                    }
//                } else {
//                    $value = ($this->model->material_length * $this->model->material_width) / $this->model->order->numbering;
//                    echo $value;
//                    exit;

//                }

//                $value = ($this->model->material_length * $this->model->material_width) / ($this->model->order->numbering * $this->model->material_area * $this->model->cut_num * );
//                echo $value;
//                exit;



//                    if($value != 0){
//                        $value = ceil($value / 100);
//                        $value = $value * 100;
//                    }



                    // Округление
                    if($this->model->order->round_to == 4){ // Округление до 100
                        $value = ceil($value / 100);
                        $value = $value * 100;
                    } elseif($this->model->order->round_to == 3){ // Округление до 10
                        $value = ceil($value / 10);
                        $value = $value * 10;
                    } elseif($this->model->order->round_to == 2){ // Округление до 1
                        $value = round($value);
                    } elseif($this->model->order->round_to == 1){ // Округление до 0.1
                        $value = round($value, 1);
                    }

                } else if($attr == 'cut_price_for_one_detail'){


                    \Yii::warning('123', 'cut_price_for_one_detail');

                    if($this->model->cut_num > 0){
                        $value = $this->model->cut_price / $this->model->cut_num;

                        // Минимальная цена
                        if($value < $this->model->min_cut_price){
                            $value = $this->model->min_cut_price;
                        }

                        \Yii::warning($this->model->min_cut_price, 'CUT MIN PRICE');

                        // Округление
                        if($this->model->order->round_to == 4){ // Округление до 100
                            $value = ceil($value / 100);
                            $value = $value * 100;
                        } elseif($this->model->order->round_to == 3){ // Округление до 10
                            $value = ceil($value / 10);
                            $value = $value * 10;
                        } elseif($this->model->order->round_to == 2){ // Округление до 1
                            $value = round($value);
                        } elseif($this->model->order->round_to == 1){ // Округление до 0.1
                            $value = round($value, 1);
                        }


                    } else {
                        $value = 0;
                    }


                } else if($attr == 'bend_price'){ // Стоимость гибки
                    if($this->model->bend_num > 0){
                        $coef = ($this->model->bend_num > 4 && $this->model->cut_num < 50) ? 2 : 1;
                        $coef2 = 1;


                        $materialWeight = null;
                        if($material && $materialParam){
                            $materialWeight = round($material->density * $materialParam->thickness * ($materialParam->length * $materialParam->width) / 1000000, 1);
                            \Yii::warning("{$material->density} * {$materialParam->thickness} * ($materialParam->length * $materialParam->width) / 1000000 = {$materialWeight}", 'MATERIAL WEIGHT');
                        }
//                        \Yii::warning([$material, $materialParam], 'MATERIAL WEIGHT');
                        if($this->model->material_weight > 7 && $this->model->material_weight < 15){
                            $coef2 = 2;
                        } else if($this->model->material_weight > 15 && $this->model->material_weight < 50){
                            $coef2 = 4;
                        } else if($this->model->material_weight > 50){
                            $coef2 = 20;
                        }

//                        $additionalValue = 1.5;

                        $materialParam = MaterialParams::findOne($this->model->cut_thickness_metal);
                        if($materialParam){
                            $material = Materials::findOne($materialParam->materials_id);
//
//                            if($material){
//                                if($material->additional_coefficient == 1){
//                                    $additionalValue = 1;
//                                }
//                            }
                        }


//                        if($this->model->cut_num < 2){
//                            $additionalValue = 1;
//                        } else {
//                            $additionalValue = 1.5;
//                        }


//                        $value = $this->model->bend_num * $this->model->cut_num * $this->model->order->price_bend * $this->model->order->num_set * $coef * $coef2 * $additionalValue;
                        $value = $this->model->bend_num * $this->model->cut_num * $this->model->order->price_bend * $this->model->order->num_set * $coef * $coef2;
//
//                        $value = ceil($value / 100);
//                        $value = $value * 100;

                        // Округление
                        if($this->model->order->round_to == 4){ // Округление до 100
                            $value = ceil($value / 100);
                            $value = $value * 100;
                        } elseif($this->model->order->round_to == 3){ // Округление до 10
                            $value = ceil($value / 10);
                            $value = $value * 10;
                        } elseif($this->model->order->round_to == 2){ // Округление до 1
                            $value = round($value);
                        } elseif($this->model->order->round_to == 1){ // Округление до 0.1
                            $value = round($value, 1);
                        }


                        \Yii::warning("{$this->model->bend_num} * {$this->model->cut_num} * {$this->model->order->price_bend} * {$this->model->order->num_set} * {$coef} * {$coef2}", 'BEND PRICE');
                    }

//                $resultAttr['bendParts']['data'][] = [
//                    'price' => $price,
//                ];
                } else if($attr == 'welding_sum'){
                    if($this->model->welding_price > 0){
                        $value = $this->model->welding_price * $this->model->cut_num * $this->model->order->num_set;

                        // Округление
                        if($this->model->order->round_to == 4){ // Округление до 100
                            $value = ceil($value / 100);
                            $value = $value * 100;
                        } elseif($this->model->order->round_to == 3){ // Округление до 10
                            $value = ceil($value / 10);
                            $value = $value * 10;
                        } elseif($this->model->order->round_to == 2){ // Округление до 1
                            $value = round($value);
                        } elseif($this->model->order->round_to == 1){ // Округление до 0.1
                            $value = round($value, 1);
                        }
                    }
                } else if($attr == 'paint_price'){ // Цена покраски
                    if($this->model->detail_area > 0){


                        $detailAreaMulti = $this->model->detail_area * 2.05;

                        $a = '';


                        if($detailAreaMulti > 100){
                            $colorPrice = floatval(Settings::findByKey('paint_price')->value);
                            $value = $this->model->detail_area / 1000 * $colorPrice * $this->model->order->num_set * $this->model->cut_num * 2.05;
                        } else {
                            $counter = 0;
                            /** @var BendPricePaint[] $bendPricePaints */
                            $bendPricePaints = BendPricePaint::find()->all();


                            foreach ($bendPricePaints as $bendPricePaint)
                            {
                                if($counter == 0){
                                    if($detailAreaMulti < $bendPricePaint->count){
                                        $value = $bendPricePaint->price * $this->model->cut_num * $this->model->order->num_set;
                                        \Yii::warning("{$bendPricePaint->price} * {$this->model->cut_num} * {$this->model->order->num_set}", 'PAINT PRICE');
                                    }
                                } else if ($counter != 0 && $counter != (count($bendPricePaints))){
                                    \Yii::warning('here');
                                    if($detailAreaMulti > $bendPricePaints[$counter-1]->count && $detailAreaMulti < $bendPricePaint->count){
                                        \Yii::warning($bendPricePaints[$counter]);
                                        $value = $bendPricePaint->price * $this->model->cut_num * $this->model->order->num_set;
                                        \Yii::warning("{$bendPricePaint->price} * {$this->model->cut_num} * {$this->model->order->num_set}", 'PAINT PRICE');
                                    }
                                }else if($counter == (count($bendPricePaints)-1)) {
                                    if($detailAreaMulti > $bendPricePaint->count){
                                        $value = $bendPricePaint->price * $this->model->detail_area * $this->model->order->num_set;
                                        \Yii::warning("{$bendPricePaint->price} * {$this->model->detail_area} * {$this->model->order->num_set}", 'PAINT PRICE');
                                    }
                                }
                                $a .= "{$counter}. {$detailAreaMulti} - {$bendPricePaint->count};<br>";
                                $counter++;
                            }

                        }

                        if(($this->model->detail_area * 2.05) > 100){
                            $value = ceil($value / 100);
                            $value = $value * 100;
                        }

//                        $value = $a;


                        if($value < 300){
                            $value = 300;
                        }

//                    $value = $this->model->detail_area * 2.5 * $this->model->order->num_set * $this->model->cut_num * 15;
                    }
                } else if($attr == 'material_ratio_percent'){

                    if($this->model->material_area > 0){
                        $sumMultiply = 0;

                        $commonParts = OrderPart::find()->where(['order_id' => $this->model->order_id,'cut_material_id' => $this->model->cut_material_id, 'cut_thickness_metal' => $this->model->cut_thickness_metal])->all();

                        foreach ($commonParts as $part){
                            $sumMultiply += $part->cut_num * $part->material_area;
                        }

                        if($sumMultiply > 0){
                            $value = $this->model->material_length * $this->model->material_width / $this->model->order->num_set * 1000 / $sumMultiply;
                            $value = round($value / 10000);
                        }
                    }
                } else if($attr == 'material_weight_painted'){
                    $value = $this->model->material_weight_painted * $this->model->total_painted;
                } else if($attr == 'material_weight_cutted'){
                    $value = $this->model->material_weight_cutted * $this->model->total_cutted;
                } else if($attr == 'material_weight_bended'){
                    $value = $this->model->material_weight_bended * $this->model->total_cutted;
                } else if($attr == 'material_weight_welded'){
                    $value = $this->model->material_weight_welded * $this->model->total_cutted;
                }
            } catch(\Exception $e){

            }

            $this->model->$attr = $value;
            $resultAttr['data'][$attr] = $value;
        }

        foreach($this->arrayAttributes as $attr)
        {
            // $attrArr = explode('[', $attr);


            // if(count($attrArr) == 2){
            //     $realAttrName = $attrArr[0];

            //     $userId = substr($attrArr[1], 0, strlen($attrArr[1])-1);

            //     \Yii::warning($realAttrName, 'realAttrName'); 
            //     \Yii::warning($userId, 'userId'); 
            // }
        }

        foreach ($this->dependAttributes as $attr)
        {
            $value = '';

            if($attr == 'cut_thickness_metal'){
                $value = ArrayHelper::map(MaterialParams::find()->where(['materials_id' => $this->model->cut_material_id])->all(), 'id', 'thickness');
                $str = '';
                foreach ($value as $name => $val){
                    $str .= "<option value='{$name}'>{$val}</option>";
                }
                $value = $str;
            }

            $resultAttr['depend'][$attr] = $value;
        }

        $this->model->save(false);

        $models = OrderPart::find()->where(['order_id' => $this->model->order_id])->all();

        foreach ($this->totalValues as $totalValue)
        {
            $value = '';

            $totalCutNum = OrderPart::find()->where(['order_id' => $this->model->order_id])->sum('cut_num');

            if($totalValue == 'total_cut_num')
            {
                $value = $totalCutNum * $this->model->order->num_set;
            } else if($totalValue == 'total_cut_length_each'){

                $value = 0;

                foreach ($models as $model)
                {
                    $value += $model->cut_num * $model->cut_length_each;

                }

                $value = $value * $this->model->order->num_set;
            } else if($totalValue == 'total_cut_incut'){
                $value = 0;

                foreach ($models as $model)
                {
                    $value += $model->cut_num * $model->cut_incut;

                }

                $value = $value * $this->model->order->num_set;
            } else if($totalValue == 'total_welding_price'){
                $value = 0;

                foreach ($models as $model)
                {
                    $value += $model->cut_num * $model->welding_price;
                }

                $value = $value * $this->model->order->num_set;
            } else if($totalValue == 'total_bend_num'){
                $value = 0;

                foreach ($models as $model)
                {
                    $value += $model->cut_num * $model->bend_num;
                }

                $value = $value * $this->model->order->num_set;
            } else if($totalValue == 'total_detail_area'){
                $value = 0;

                foreach ($models as $model)
                {
                    $value += $model->cut_num * $model->detail_area;
                }

                $value = $value * $this->model->order->num_set * 2.05;
            } else if($totalValue == 'total_digitization_price'){
                $value = 0;

                foreach ($models as $model)
                {
                    $value += $model->digitization_price;
                }

                $value = $value * $this->model->order->num_set;
            } else if($totalValue == 'material_price'){
                $value = 0;

                foreach ($models as $model)
                {
                    $value += $model->material_price;
                }
            }

            $resultAttr['total'][$totalValue] = $value;
        }

        $uniqueParts = OrderPart::find()->where(['order_id' => $model->order_id])->groupBy(['cut_material_id', 'cut_thickness_metal'])->all();

        $resultAttr['uniqueParts'] = [];

        $totalMaterialWeight = 0;
        $totalMaterialPrice = 0;
        $totalDelivery = 0;

        $partsPrices = [];

        foreach ($uniqueParts as $part) // Расчет веса материала и стоимости
        {
            $partMaterial = MaterialParams::findOne($part->cut_thickness_metal);
            if($partMaterial == null){
                continue;
            }
            $oneMaterialWeight = $part->material_length * $part->material_width / 1000000 * 7.85 * $partMaterial->thickness;
            $oneMaterialWeightStr = explode('.', strval($oneMaterialWeight));
//                            if(count($oneMaterialWeightStr) == 1){
//                                $oneMaterialWeight = floatval($oneMaterialWeightStr[0].'.'.($oneMaterialWeightStr[1]+1));
//                            }

//                                        if(count($oneMaterialWeightStr) == 2){
//                                $oneMaterialWeight = floatval($oneMaterialWeightStr[0].'.'.($oneMaterialWeightStr[1]+1));
//                            }

            $part->material_price = ceil($part->material_price / 100);
            $part->material_price = $part->material_price * 100;

//            $totalMaterialPrice += $part->material_price + $oneMaterialWeight * 2;
            $totalDelivery += $oneMaterialWeight * 2;



//            $oneMaterialWeight = ceil($oneMaterialWeight / 0.1);
//            $oneMaterialWeight = $oneMaterialWeight * 0.1;

            $oneMaterialWeight = NumberHelper::roundUp($oneMaterialWeight, 1);

            $totalMaterialWeight += $oneMaterialWeight;

            $part->material_price = $part->material_price + $oneMaterialWeight * 1; // TODO: Вывести в настройки 1

            $part->material_price = ceil($part->material_price / 100);
            $part->material_price = $part->material_price * 100;


            $totalWeight = 0;
            foreach ($uniqueParts as $uPart)
            {
                $partMaterial = MaterialParams::findOne($uPart->cut_thickness_metal);
                if($partMaterial == null){
                    continue;
                }
                $totalWeight = $totalWeight + ($uPart->material_length * $uPart->material_width / 1000000 * 7.85 * $partMaterial->thickness);
            }

            $materialParam = MaterialParams::findOne($part->cut_thickness_metal);
            $materialRatioNoRound = 0;
            if($materialParam && (floatval($part->material_length) * floatval($part->material_width)) > 0){
                $materialRatioNoRound = ($materialParam->length * $materialParam->width) / ($part->material_length * $part->material_width);
            }

//            $materialUniquePrice = 2590 / 0.3125 + 400 / 157.1 * 78.5;
            if($materialRatioNoRound == 0 || $totalWeight == 0){
                $materialUniquePrice = 0;
            } else {
                $materialUniquePrice = $part->material_sheet_price / $materialRatioNoRound + $part->order->price_delivery_metal / $totalWeight * $oneMaterialWeight;

                $materialUniquePrice = ceil($materialUniquePrice / 100);
                $materialUniquePrice = $materialUniquePrice * 100;
            }

            $partsPrices["{$part->cut_material_id}_{$part->cut_thickness_metal}"] = [
                'material_price' => $materialUniquePrice,
            ];

            $totalMaterialPrice = $totalMaterialPrice + $materialUniquePrice;


//            \Yii::warning("{$part->material_sheet_price} / {$materialRatioNoRound} + {$part->order->price_delivery_metal} / {$totalWeight} * {$oneMaterialWeight}", 'Material price calc');

            $resultAttr['uniqueParts']['data'][] = [
                'id' => $part->id,
                'material' => $partMaterial->materials->name,
                'name' => "1 лист {$part->material_length}x{$part->material_width} ({$oneMaterialWeight} кг)",
                'price' => $materialUniquePrice,
                'cut_thickness_metal' => $part->cut_thickness_metal,
                'delivery' => $oneMaterialWeight * 2,
            ];
        }

        // Округление
        if($this->model->order->round_to == 4){ // Округление до 100
            $totalDelivery = ceil($totalDelivery / 100);
            $totalDelivery = $totalDelivery * 100;
        } elseif($this->model->order->round_to == 3){ // Округление до 10
            $totalDelivery = ceil($totalDelivery / 10);
            $totalDelivery = $totalDelivery * 10;
        } elseif($this->model->order->round_to == 2){ // Округление до 1
            $totalDelivery = round($totalDelivery);
        } elseif($this->model->order->round_to == 1){ // Округление до 0.1
            $totalDelivery = round($totalDelivery, 1);
        }


        // Округление
        if($this->model->order->round_to == 4){ // Округление до 100
            $totalMaterialPrice = ceil($totalMaterialPrice / 100);
            $totalMaterialPrice = $totalMaterialPrice * 100;
        } elseif($this->model->order->round_to == 3){ // Округление до 10
            $totalMaterialPrice = ceil($totalMaterialPrice / 10);
            $totalMaterialPrice = $totalMaterialPrice * 10;
        } elseif($this->model->order->round_to == 2){ // Округление до 1
            $totalMaterialPrice = round($totalMaterialPrice);
        } elseif($this->model->order->round_to == 1){ // Округление до 0.1
            $totalMaterialPrice = round($totalMaterialPrice, 1);
        }

        $resultAttr['uniqueParts']['total'] = [
            'material' => "<b>За материал ({$totalMaterialWeight} кг.): </b>",
            'price' => "<b>{$totalMaterialPrice}</b>",
            'delivery' => "<b>{$totalDelivery}</b>",
        ];

        if($calcOrder){
            $model->order->price_delivery_metal = $totalDelivery;
            $model->order->save(false);
        }


        /** @var OrderPart[] $bendParts */
//        $bendParts = OrderPart::find()->where(['order_id' => $model->order_id])->andWhere(['>', 'bend_num', 0])->all();
//        $bendParts = OrderPart::find()->where(['order_id' => $model->order_id])->andWhere(['id' => 70])->all();

//        $resultAttr['bendParts']['data'] = [];
//
//
//        foreach ($bendParts as $part)
//        {
//            $coef = ($part->bend_num > 4 && $part->cut_num < 50) ? 2 : 1;
//            $coef2 = 1;
//
//            if($part->material_weight > 7){
//                $coef2 = 2;
//            } else if($part->material_weight > 15){
//                $coef2 = 4;
//            } else if($part->material_weight > 50){
//                $coef2 = 20;
//            }
//
//            $additionalValue = 1.5;
//
//            $materialParam = MaterialParams::findOne($part->cut_thickness_metal);
//            if($materialParam){
//                $material = Materials::findOne($materialParam->materials_id);
//
//                if($material){
//                    if($material->additional_coefficient == 1){
//                        $additionalValue = 1;
//                    }
//                }
//            }
//
//            $price = $part->bend_num * $part->cut_num * $part->order->price_bend * $part->order->num_set * $coef * $coef2 * $additionalValue;
//
//            $resultAttr['bendParts']['data'][] = [
//                'price' => $price,
//            ];
//
//
////            $resultAttr['uniqueParts']['data'][] = [
////                'id' => $part->id,
////                'material' => $partMaterial->materials->name,
////                'name' => "1 лист {$part->material_length}x{$part->material_width} ({$oneMaterialWeight} кг)",
////                'price' => $part->material_price,
////                'delivery' => $oneMaterialWeight * 2,
////            ];
//        }

        $materialParam = MaterialParams::findOne($this->model->cut_thickness_metal);

        try {
            if($materialParam){
                $materialPrice = 0;
                foreach($resultAttr['uniqueParts']['data'] as $row){
                    if($row['material'] == $materialParam->materials->name && $row['cut_thickness_metal'] == $this->model->cut_thickness_metal){
                        $materialPrice = $row['price'];
                    }
                }

                if($this->model->material_area > 0){ // Если Площадь материала > 0
                    $materialRatioPercent = (floatval($this->model->material_ratio_percent) / 100); // Коэф (%)

                    \Yii::warning("{$this->model->cut_price} + {$this->model->welding_sum} + {$this->model->bend_price} + {$this->model->paint_price} + {$this->model->material_price} /
                    (({$this->model->material_length} * {$this->model->material_width}) /
                        ({$this->model->material_area} * {$this->model->cut_num} * {$this->model->order->numbering} * {$materialRatioPercent} * {$this->model->order->num_set}))");

                    if(($this->model->material_area * $this->model->cut_num * $this->model->order->numbering * ($this->model->material_ratio_percent / 100) * $this->model->order->num_set) > 0){

                        \Yii::warning("{$this->model->cut_price} + {$this->model->welding_sum} + {$this->model->bend_price} + {$this->model->paint_price} + {$this->model->material_price} /
                            (({$this->model->material_length} * {$this->model->material_width}) /
                                ({$this->model->material_area} * {$this->model->cut_num} * {$this->model->order->numbering} * ({$this->model->material_ratio_percent} / 100) * {$this->model->order->num_set}))", 'total price');


                        $materialPrice = 0;

                        if(isset($partsPrices["{$this->model->cut_material_id}_{$this->model->cut_thickness_metal}"])){
                            $materialPrice = $partsPrices["{$this->model->cut_material_id}_{$this->model->cut_thickness_metal}"]['material_price'];
                        }

//                        $resultAttr['data']['total_price'] =
//                            round(floatval($this->model->cut_price) + floatval($this->model->welding_sum) + floatval($this->model->bend_price) + floatval($this->model->paint_price) + floatval($materialPrice), 1);

                        $resultAttr['data']['total_price'] =
                            round(floatval($this->model->cut_price) + floatval($this->model->welding_sum) + floatval($this->model->bend_price) + floatval($this->model->paint_price) + floatval($materialPrice)/
                                (($this->model->material_length * $this->model->material_width) /
                                    ($this->model->material_area_sum * ($this->model->material_ratio_percent / 100) * $this->model->order->num_set)), 2);

                        \Yii::warning("{$this->model->cut_price} + ".floatval($this->model->welding_sum)." + ".floatval($this->model->bend_price)." + ".floatval($this->model->paint_price)." + ".floatval($materialPrice)." /
                                ((".$this->model->material_length." * ".$this->model->material_width.") /
                                    (".$this->model->material_area_sum." * (".$this->model->material_ratio_percent." / 100) * ".$this->model->order->num_set."))", 'TOTAL ROW PRICE CALC');

                    }
                } else {
                    $commonCutNum = OrderPart::find()->where(['order_id' => $this->model->order_id,'cut_material_id' => $this->model->cut_material_id, 'cut_thickness_metal' => $this->model->cut_thickness_metal])->sum('cut_num');

                    $materialPrice = 0;

                    if(isset($partsPrices[$this->model->id])){
                        $materialPrice = $partsPrices[$this->model->id]['material_price'];
                    }

                    $resultAttr['data']['total_price'] =
                        round(floatval($this->model->cut_price) + floatval($this->model->welding_sum) + floatval($this->model->bend_price) + floatval($this->model->paint_price) + floatval($materialPrice) /
                            ($commonCutNum / $this->model->cut_num), 2);

                }


                $resultAttr['data']['total_price'] = $resultAttr['data']['total_price'] + $this->model->additional_word_price1 + $this->model->additional_word_price2;

            }
        } catch (\Exception $e){
            \Yii::warning($e->getMessage(),'ERROR');
        }



//
//        $resultAttr['uniqueParts']['data'][] = [
//            'id' => $part->id,
//            'material' => $partMaterial->materials->name,
//            'name' => "1 лист {$part->material_length}x{$part->material_width} ({$oneMaterialWeight} кг)",
//            'price' => $part->material_price,
//            'delivery' => $oneMaterialWeight * 2,
//        ];
//





        $this->model->attributes = $resultAttr['data'];
        $this->model->save();
        \Yii::warning($this->model->errors, 'Error');




        $models = OrderPart::find()->where(['order_id' => $this->model->order_id])->all();

        $totalTotalPrice = 0;

        foreach ($models as $model)
        {
            $totalTotalPrice += $model->total_price;
        }

        $resultAttr['total']['total_total_price'] = round($totalTotalPrice, 2);

        // Сохранение итоговых данных в заказ
        $order = Order::findOne($this->model->order_id);
        if($order){
            $order->price = round($totalTotalPrice, 2);

            $bendNum = 0;
            $materialPrice = 0;
            $cutPrice = 0;
            $bendPrice = 0;
            $weldingPrice = 0;
            $paintPrice = 0;

            foreach ($models as $model){
                $bendNum = $bendNum + $model->bend_num;
                $cutPrice = $cutPrice + $model->cut_price;
                $bendPrice = $bendPrice + $model->bend_price;
                $weldingPrice = $weldingPrice + $model->welding_sum;
                $paintPrice = $paintPrice + $model->paint_price;
            }

            $order->bend_num = $bendNum;
            $order->material_price = $materialPrice;
            $order->cut_price = $cutPrice;
            $order->total_bend_price = $bendPrice;
            $order->total_welding_price = $weldingPrice;
            $order->total_paint_price = $paintPrice;

            $order->save(false);
        }

        return $resultAttr;
    }

    public function getNumbering()
    {
        return $this->model->digitization_price ? $this->model->digitization_price : $this->model->order->numbering;
    }


    public function getMaterialPrice()
    {
        $uniqueParts = OrderPart::find()->where(['order_id' => $this->model->order_id])->groupBy(['cut_material_id', 'cut_thickness_metal'])->all();

        $resultAttr['uniqueParts'] = [];

        $totalMaterialWeight = 0;
        $totalMaterialPrice = 0;
        $totalDelivery = 0;

        $partsPrices = [];

        foreach ($uniqueParts as $part) // Расчет веса материала и стоимости
        {
            $partMaterial = MaterialParams::findOne($part->cut_thickness_metal);
            if($partMaterial == null){
                continue;
            }
            $oneMaterialWeight = $part->material_length * $part->material_width / 1000000 * 7.85 * $partMaterial->thickness;
            $oneMaterialWeightStr = explode('.', strval($oneMaterialWeight));
//                            if(count($oneMaterialWeightStr) == 2){
//                                $oneMaterialWeight = floatval($oneMaterialWeightStr[0].'.'.($oneMaterialWeightStr[1]+1));
//                            }
            $oneMaterialWeight = NumberHelper::roundUp($oneMaterialWeight, 1);
            $totalMaterialWeight = $totalMaterialWeight + $oneMaterialWeight;
            $totalDelivery += $oneMaterialWeight * 2;


            $part->material_price = $part->material_price + $oneMaterialWeight * 2; // TODO: Вывести в настройки 2

            $totalWeight = 0;
            foreach ($uniqueParts as $uPart)
            {
                $uPartMaterial = MaterialParams::findOne($uPart->cut_thickness_metal);
                if($uPartMaterial == null){
                    continue;
                }
                $totalWeight = $totalWeight + ($uPart->material_length * $uPart->material_width / 1000000 * 7.85 * $uPartMaterial->thickness);
            }

            $materialParam = MaterialParams::findOne($part->cut_thickness_metal);
            $materialRatioNoRound = 0;
            if($materialParam && (floatval($part->material_length) * floatval($part->material_width)) > 0){
                $materialRatioNoRound = ($materialParam->length * $materialParam->width) / ($part->material_length * $part->material_width);
            }


            if($totalWeight == 0 || $materialRatioNoRound == 0){
                $materialUniquePrice = 0;
            } else {
                $materialUniquePrice = $part->material_sheet_price / $materialRatioNoRound + $part->order->price_delivery_metal / $totalWeight * $oneMaterialWeight;

                $materialUniquePrice = ceil($materialUniquePrice / 100);
                $materialUniquePrice = $materialUniquePrice * 100;
            }

            $totalMaterialPrice = $totalMaterialPrice + $materialUniquePrice;

            $partsPrices["{$part->cut_material_id}_{$part->cut_thickness_metal}"] = [
                'material_price' => $materialUniquePrice,
            ];
        }

        return $partsPrices;
    }
}